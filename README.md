pyOTAMIN ![icon](https://bitbucket.org/rmarty/pyotamin/avatar/)
================================================================

[![version](https://img.shields.io/badge/version-2021.08.30-blue)](https://bitbucket.org/rmarty/pyotamin/get/2021.08.30.zip)
[![readthedocs](https://readthedocs.org/projects/pyotamin/badge/?version=latest)](https://pyotamin.readthedocs.io/fr/latest)
[![issues](https://img.shields.io/bitbucket/issues/rmarty/pyotamin)](https://bitbucket.org/rmarty/pyotamin/issues?status=new&status=open)
[![CodeFactor](https://www.codefactor.io/repository/bitbucket/rmarty/pyotamin/badge)](https://www.codefactor.io/repository/bitbucket/rmarty/pyotamin)


Ce projet rassemble les scripts issus de **[pySPC](https://bitbucket.org/rmarty/pyspc)** dédiés au projet Prévision 2015 du Schapi.

[![python 2](https://img.shields.io/badge/python-2.7-blueviolet)](https://www.python.org/)

- GRP2prv : Conversion des sorties de GRP (mode temps-réel ou mode différé) au format PRV Scores, compatible avec EAO/EXPRESSO et OTAMIN

- PLA2prv : Conversion des sorties de PLATHYNES (mode temps-réel ou mode différé) au format PRV Scores, compatible avec EAO/EXPRESSO et OTAMIN


[![python 3](https://img.shields.io/badge/python-3.7-blue)](https://www.python.org/)

- GRP2prv_py3 : Conversion des sorties de GRP (mode temps-réel ou mode différé) au format PRV Scores, compatible avec EAO/EXPRESSO et OTAMIN

- PLA2prv_py3 : Conversion des sorties de PLATHYNES (mode temps-réel ou mode différé) au format PRV Scores, compatible avec EAO/EXPRESSO et OTAMIN

- grp4otamin : Script de préparation *en masse* des données produites par GRP Calage pour OTAMIN, outil d'estimation de l'incertitude prédictive "modèle"

- mohys4otamin : Script de préparation *en masse* des données produites par MOHYS Calage pour OTAMIN, outil d'estimation de l'incertitude prédictive "modèle"

- mohys4otamin_finalfiles : Script créant les fichiers pour OTAMIN à partir des séries produites par [mohys4otamin.py](mohys4otamin.html)

- mohys4otamin_run : Script de lancement de [mohys4otamin.py](mohys4otamin.html)

License
-------------------------------------------------------------------------------
Se reporter au fichier COPYING.txt.

[![License](https://img.shields.io/badge/license-GPLv3-blue)](https://www.gnu.org/licenses/quick-guide-gplv3.fr.html)

Contact
-------------------------------------------------------------------------------
Renaud Marty <renaud.marty@developpement-durable.gouv.fr>

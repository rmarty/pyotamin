#!/usr/bin/python
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyOTAMIN>.
# Copyright (C) 2015-2020  L. Berthet, J. Barat and R. Marty
#   (lionel.berthet@developpement-durable.gouv.fr)
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Conversion des sorties de GRP (mode temps-réel ou mode différé)
au format PRV Scores, compatible avec l'EAO et OTAMIN

:Auteurs:
    - Lionel Berthet (SPC Loire-Cher-Indre)
    - Joris Barat (SPC Loire-Cher-Indre)
    - Renaud Marty (SPC Loire-Allier-Cher-Indre)

Version : 2024.01.24
    - Correction si un même fichier GRP comprend plusieurs pas de temps
    - Cas test : SPC GAD

Version : 2022.05.18
    - Correction liée à la balise EXTRA de GRP v2020

Version : 2022.02.09
    - Ajout GRP version 2020
    - Correction de bogues mineurs

Version : 2021.08.30
    - Ajout options -G et -O pour versions de GRP et OTAMIN

Version : 2019.10.25
    - Ajout option -f pour export dans un répertoire autre qu'OTAMIN
    - Utilisé pour exporter des valeurs avant <DtDerObs>

Version : 2019.04.04
    - ajout arg -1 pour exporter 1 fichier par lieu
    - ajout arg -g pour exporter les simulations
        - sim interne avant DtDerObs
        - prévision sans assimilation après DtDerObs
        - en spécifiant le code modèle correspondant

Version : 2017.08.29
    - ajout arg -u pour exporter toutes series, dont celles qui sont identiques

Version : 2016.10.03
    - ajout arg -d pour exporter les pluies (RR), températures (TA) obs et prv

"""
# *****************************************************************************
# --- IMPORTS ---
# *****************************************************************************
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import argparse
import collections
from datetime import datetime as dt, timedelta as tdelta
import glob
import numpy as np
import os
import os.path
import pandas as pnd
import pytz
import re
import sys


# *****************************************************************************
# --- CONSTANTES ---
# *****************************************************************************
GRP_VERSIONS = ['GRPv2016', 'GRPv2018', 'GRPv2020']
"""Versions de GRP autorisées"""

GRP_FILENAMES = {
    ('obs', True): 'GRP_Obs.txt', ('obs', False): 'GRP_D_Obs.txt',
    ('bv', True): 'BASSIN.DAT', ('bv', False): 'BASSIN.DAT',
    ('sim', True): 'PQE_1A.DAT', ('sim', False): 'PQE_1A_D.DAT',
}
"""Noms des fichiers internes/observation de GRP"""

GRP_PREFIX = {
    ('prv', True): 'GRP_Prev_', ('prv', False): 'GRP_D_Prev_',
    ('sim', True): 'GRP_Simu_', ('sim', False): 'GRP_D_Simu_',
}
"""Préfixes des fichiers prévision de GRP"""

GRP_EXTRAS = ['P0', 'PP']
"""Identifiants des prévisions avec pluie nulle et pluie parfaite"""

GRP_NAVALUES = [-9.9999, -99.9999, '-9.9999', '-99.9999']
"""Valeurs manquantes selon GRP"""

OTAMIN_VERSIONS = ['OTAMINv2016', 'OTAMINv2018']
"""Versions de OTAMIN autorisées"""

OTAMIN_PREFIX = 'GRP_B_'
"""Préfixe de fichier OTAMIN"""

OTAMIN_TAG = {(False, True): '_Obs', (False, False): '_Sim',
              (True, False): '', (True, True): '_DA'}
"""Etiquette de données selon la nature (prévision) et la grandeur"""

OTAMIN_EXT = '.prv'
"""Extension de fichier OTAMIN"""

OTAMIN_RE_JHM = re.compile(r'^\d{2}J\d{2}H\d{2}M$')
"""Regex contrôlant la validité du pas de temps en chaine de caractères"""

MODES = {'TR': 'temps-réel',  'TD': 'temps différé'}
"""Modes de fonctionnement"""

TIMEZONE = pytz.timezone('Europe/Paris').localize(dt.now()).tzinfo
"""Fuseau horaire de Europe/Paris"""

IdSeries = collections.namedtuple(
    'IdSeries', ['loc', 'varname', 'model', 'scen'])
"""Identifiant d'une série de données"""
IdSeries.__new__.__defaults__ = (None,) * len(IdSeries._fields)
#    'IdSeries', ['loc', 'varname', 'model', 'scen'], defaults=[None]*4)


# *****************************************************************************
# --- FONCTIONS ARGS ---
# *****************************************************************************
def check_args(test, name):
    """
    Vérifier les arguments

    Parameters
    ----------
    test : bool
        Test indiquant si l'argument est valide
    name : str
        Nom de l'argument

    Raises
    ------
    ValueError
        Si l'argument est invalid

    """
    if not test:
        raise ValueError("L'arguement '{}' est incorrect".format(name))


def check_mode(mode):
    """
    Vérifier la version de GRP

    Parameters
    ----------
    mode : str
        Mode d'utilisation

    Raises
    ------
    ValueError
        Si le mode est incorrect

    """
    if mode not in MODES.keys():
        raise ValueError("Le mode est incorrect")


def init_args(arg, default=None):
    """
    Définir un argument non-défini (None) avec un objet par défaut

    Parameters
    ----------
    arg : object
        Argument
    default : object
        Objet par défaut

    Returns
    -------
    object
        Objet par défaut si arg est None
        Argument si arg n'est pas None

    """
    if arg is None:
        return default
    return arg


def process_args():
    """
    Récupérer les arguments fournis au script <GRP2prv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Initialisation
    # -----------------------------------------------
    info = {
        "grp_version": {
            "short": "Version de GRP",
            "help": "Version de GRP spécifique à la configuration du SPC",
        },
        "otamin_version": {
            "short": "Version de OTAMIN",
            "help": "Version de OTAMIN spécifique à la configuration du SPC",
        },
        "in_folder": {
            "short": "Dossier des previsions GRP",
            "help": "Dossier des previsions GRP"
        },
        "mode": {
            "short": "Mode de fonctionnement",
            "help": "Mode de fonctionnement :  temps reel (TR)/temps differe"
                    "(TD)"
        },
        "modele": {
            "short": "Code modele (POM)",
            "help": "Code du modele au sens de la POM (ex: 45gGRPd000)"
        },
        "otamin_folder": {
            "short": "Dossier de sortie OTAMIN",
            "help": "Dossier de sortie des fichiers PRV pour OTAMIN"
        },
        "onebyloc": {
            "short": "[optionnel] Exporter 1 fichier par lieu ?",
            "help": "[optionnel] Exporter 1 fichier par lieu ? "
                    "(default=False)"
        },
        "out_folder": {
            "short": "Dossier d'export dans un répertoire autre qu'OTAMIN",
            "help": "Dossier d'export dans un répertoire autre qu'OTAMIN. "
                    "Utilisé pour exporter des valeurs avant DtDerObs"
        },
        "rrta": {
            "short": "[optionnel] Exporter RR et TA ?",
            "help": "[optionnel] Exporter les donnees associees RR"
                    " et TA ? (default=False)"
        },
        "scenarios": {
            "short": "[optionnel] Liste des codes scenarios",
            "help": "[optionnel] Liste des codes scenarios (separateur: ',')"
        },
        "scen_P0": {
            "short": "[optionnel] Code du scenario de pluie nulle",
            "help": "[optionnel] Code du scenario de pluie nulle"
        },
        "scen_PP": {
            "short": "[optionnel] Code du scenario de pluie parfaite",
            "help": "[optionnel] Code du scenario de pluie parfaite"
        },
        "simul": {
            "short": "[optionnel] Exporter les simulations",
            "help": "[optionnel] Exporter les simulations en spécifiant le "
                    "code modèle"
        },
        "suffix": {
            "short": "[optionnel] Suffixe des fichiers prv",
            "help": "[optionnel] Suffixe des fichiers prv, distinguant les "
                    "fichiers issus du meme instant de production"
                    " (défaut: 'num')"
        },
        "unique": {
            "short": "[optionnel] Exporter des previsions uniques ?",
            "help": "[optionnel] Exporter des previsions uniques ? "
                    "(default=False)"
        },
        "verbose": {
            "short": "[optionnel] Mode verbeux",
            "help": "[optionnel] Mode verbeux (default=False)"
        }
    }
    # -----------------------------------------------
    #    ETAPE 2 : Création de l'instance du parser
    # -----------------------------------------------
    description = "Conversion des sorties de GRP (mode temps-réel ou "\
        "mode différé) au format PRV Scores, compatible avec "\
        "EAO/EXPRESSO et OTAMIN"""
    parser = argparse.ArgumentParser(description=description)
    # -----------------------------------------------
    #    ETAPE 3 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-G", "--GRP-version",
                        action="store",
                        dest="grp_version",
                        choices=GRP_VERSIONS,
                        default=GRP_VERSIONS[0],
                        help=info["grp_version"]["help"])
    parser.add_argument("-O", "--OTAMIN-version",
                        action="store",
                        dest="otamin_version",
                        choices=OTAMIN_VERSIONS,
                        default=OTAMIN_VERSIONS[0],
                        help=info["otamin_version"]["help"])
    parser.add_argument("-i", "--in-folder",
                        action="store",
                        dest="in_folder",
                        required=True,
                        help=info["in_folder"]["help"])
    parser.add_argument("-m", "--mode",
                        action="store",
                        dest="mode",
                        choices=sorted(list(MODES.keys())),
                        required=True,
                        help=info["mode"]["help"])
    parser.add_argument("-p", "--modele-pom",
                        action="store",
                        dest="modele",
                        required=True,
                        help=info["modele"]["help"])
    parser.add_argument("-o", "--otamin-folder",
                        action="store",
                        dest="otamin_folder",
                        required=True,
                        help=info["otamin_folder"]["help"])
    parser.add_argument("-f", "--out-folder",
                        action="store",
                        dest="out_folder",
                        help=info["out_folder"]["help"])
    parser.add_argument("-s", "--scenarios",
                        action="store",
                        dest="scenarios",
                        help=info["scenarios"]["help"])
    parser.add_argument("-0", "--scen-P0",
                        action="store",
                        dest="scen_P0",
                        help=info["scen_P0"]["help"])
    parser.add_argument("-P", "--scen-PP",
                        action="store",
                        dest="scen_PP",
                        help=info["scen_PP"]["help"])
    parser.add_argument("-x", "--suffix",
                        action="store",
                        dest="suffix",
                        choices=['num', 'mod'],
                        default='num',
                        help=info["suffix"]["help"])
    parser.add_argument("-u", "--unique-forecast",
                        dest="unique",
                        action="store_true",
                        default=False,
                        help=info["unique"]["help"])
    parser.add_argument("-d", "--hmet-data",
                        dest="rrta",
                        action="store_true",
                        default=False,
                        help=info["rrta"]["help"])
    parser.add_argument("-1", "--onefile-byloc",
                        dest="onebyloc",
                        action="store_true",
                        default=False,
                        help=info["onebyloc"]["help"])
    parser.add_argument("-g", "--grp-simul",
                        dest="simul",
                        action="store",
                        help=info["simul"]["help"])
    parser.add_argument("-v", "--verbose",
                        dest="verbose",
                        action="store_true",
                        default=False,
                        help=info["verbose"]["help"])
    return parser.parse_args()


# *****************************************************************************
# --- FONCTIONS DATE ---
# *****************************************************************************
def date_parser(date):
    """
    Convertir la date en objet datetime

    Parameters
    ----------
    date : str
        Date sous forme de texte

    Returns
    -------
    datetime
        Date sous forme d'objet python datetime

    """
    txt = str(int(date))
    return dt.strptime(txt, lenstr2dtfmt(txt))


def lenstr2dtfmt(txt=None):
    """
    Définir le format de la date selon la longueur de la chaine de caractères

    Parameters
    ----------
    txt : str
        Chaine de caractères

    Returns
    -------
    dtfmt : str
        Format de la date

    """
    dtstr = 'YYYYmmddHHMM'
    return dtstr[:len(txt)].replace('YYYY', '%Y').replace('mm', '%m')\
        .replace('dd', '%d').replace('HH', '%H').replace('MM', '%M')


# *****************************************************************************
# --- FONCTIONS ---
# *****************************************************************************
def isunique(data=None, cdata=None, loc=None, unique=False):
    """
    Déclarer si une série est identique à une série de réféfence

    Parameters
    ----------
    data : dict
        Dictionnaire des séries de données (pnd.DataFrame)
    cdata : pnd.DataFrame
        Série de données courantes
    loc : str
        Identifiant du lieu
    unique : bool
        Conserver uniquement les prévisions distinctes ?
        Par défaut: False

    Returns
    -------
    bool
        True si série identique et si unique = True
    """
    if not unique:
        return False
    if not data:
        return False
    # Censure si la série de débit est identique
    # à une série déjà intégrée dans le dictionnaire
    # de pnd.DataFrame <data>
    ctest = pnd.DataFrame(cdata)
    for ckey in data:
        if ckey[0] == loc and ckey[1] == 'Q':
            ctest.columns = [tuple(ckey)]
            if data[ckey].equals(ctest):
                return True
    return False


def msg_content(content=None, label='', label1='', verbose=False):
    """
    Message sur le contenu

    Parameters
    ----------
    content : list
        Contenu
    label : str
        Libellé si 'content' contient plusieurs éléments
    label1 : str
        Libellé si 'content' contient au plus un élément
    verbose : bool
        Mode bavard ?
        Par défaut: False

    Returns
    -------
    None

    """
    if content is None:
        content = []
    if len(content) > 1:
        v_print(label.format(len(content)), verbose=verbose)
    else:
        v_print(label1.format(len(content)), verbose=verbose)


def msg_exit(verbose=False):
    """
    Message de sortie

    Parameters
    ----------
    verbose : bool
        Mode bavard ?
        Par défaut: False

    Returns
    -------
    None

    """
    v_print('', verbose=verbose)
    v_print('='*50, verbose=verbose)
    v_print(" Fin de l'exécution de {}".format(os.path.basename(sys.argv[0])),
            verbose=verbose)
    v_print('='*50, verbose=verbose)


def set_idsim(scen, delta=-1000):
    """
    Définir un code scénarion 'simulation'
    à partir d'un code de scénario 'prévision'

    Parameters
    ----------
    scen : str, int
        Identifiant du scénario 'prévision'

    Other Parameters
    ----------------
    delta : str, int
        Incrément pour obtenir l'identiant du scénario 'simulation'
        Par défaut: -1000

    Returns
    -------
    str
        Identifiant du scénario 'simulation'

    """
    return str(int(float(scen)) + int(float(delta)))


def sort_filenames(filenames=None, element=None):
    """
    Trier une liste de fichier selon la présence d'un élément dans leur nom

    Parameters
    ----------
    filenames : list
        Liste des fichiers à trier
    element : str
        Elément servant à définir le premier fichier

    Returns
    -------
    filenames : list
        Liste des fichiers triés

    """
    f0 = [f for f in filenames if element in f]
    f0.extend([f for f in filenames if element not in f])
    return f0


def strip_dframe_values(text):
    """
    Retirer les caractères superflus en début/fin de mot (espace)

    Parameters
    ----------
    text : str
        Chaine de caractères d'origine

    Returns
    -------
    str
        Chaine de caractères nettoyée

    """
    try:
        return text.strip()
    except AttributeError:
        return text


def tofloat(text):
    """
    Convertir une chaine de caractère en réel

    Parameters
    ----------
    text : str
        Chaine de caractères d'origine

    Returns
    -------
    float
        Valeur

    """
    try:
        return float(text.strip())
    except AttributeError:
        return text


def v_print(chaine='\n', encodage=sys.stdout.encoding, verbose=False):
    """
    Fonction pour afficher ou non la chaîne de caractères encodée

    Parameters
    ----------
    chaine : str
        Chaine de caractères

    Other Parameters
    ----------------
    encodage : str
        Identifiant de l'encodage.
        Par défaut: sys.stdout.encoding ou, à défaut, 'utf-8'
    verbose : bool
        Mode bavard ?
        Par défaut: False

    Returns
    -------
    None

    """
    if encodage is None:
        encodage = 'utf-8'
    if verbose:
        print(chaine.encode(encodage))


# *****************************************************************************
# --- FONCTIONS GRP ---
# *****************************************************************************
def check_version_grp(version):
    """
    Vérifier la version de GRP

    Parameters
    ----------
    version : str
        Version de GRP

    Raises
    ------
    ValueError
        Si la version est incorrecte

    """
    if version not in GRP_VERSIONS:
        raise ValueError("La version de GRP est incorrecte")


def get_grp_scenario(filename):
    """
    Obtenir le code scénario à partir du fichier

    Parameters
    ----------
    filename : str
        Chemin du fichier de prévision/simulation

    Returns
    -------
    scen : str
        Identifiant du scénario

    """
    return os.path.basename(filename).split('_')[-1].split('.')[0]


def get_grp_area(filename):
    """
    Renvoie la surface du bassin versant définie dans un fichier BASSIN.DAT

    Parameters
    ----------
    filename : str
        Chemin du fichier BASSIN.DAT

    Returns
    -------
    area : float
        Surface du bassin versant

    """
    area = None
    with open(filename, 'r') as f:
        for line in f.readlines():
            # if line.startswith('S'):  # UnicodeDecodeError with GRP-v2018
            if line[0] == 'S':
                try:
                    area = float(line[1:].split('!')[0].strip())
                except ValueError:
                    area = None
                break
    return area


def read_grp_file(filename=None, mode=None, model=None, grp_version=None,
                  tag=None, sim_model=None, scen=None, scen_P0=None,
                  unique=False, export_rr_ta=False,
                  data=None, data_other=None,
                  dtderobs_loc=None, dtderobs=None, dtderobs_other=None,
                  ):
    """
    Lire et organiser les données des fichiers de GRP

    Parameters
    ----------
    filename : str
        Fichier GRP
    mode : str
        Mode d'utilisation
    model : str
        Code modèle POM
    grp_version : str
        Version de GRP parmi ['GRPv2016', 'GRPv2018', 'GRPv2020']
    tag : str
        Type de fichier GRP, parmi ['obs', 'prv', 'sim']

    Other Parameters
    ----------------
    sim_model : str
        Code modèle POM des simulations
        Par défaut: None
    scen_P0 : str
        Identifiant du scénario pluie nulle. Utilisée lorsque les prévisions
        identiques sont ignorées (voir argument unique)
    unique : bool
        Conserver uniquement les prévisions distinctes ?
        Par défaut: False
    export_rr_ta : bool
        Exporter les pluies (RR) et température (TA) ?
        Par défaut: False

    data : dict
        Dictionnaire des séries de données 'Q' (pnd.DataFrame)
    data_other : dict
        Dictionnaire des séries de données 'autre' (pnd.DataFrame)
    dtderobs_loc : dict
        Dictionnaire de dates de fin d'observation, par lieu
    dtderobs : dict
        Dictionnaire de dates de fin d'observation, par série 'Q'
    dtderobs_other : dict
        Dictionnaire de dates de fin d'observation, par série 'autre'

    Returns
    -------
    dtderobs_loc : dict
        Dictionnaire de dates de fin d'observation, par lieu
    dtderobs : dict
        Dictionnaire de dates de fin d'observation, par série 'Q'
    dtderobs_other : dict
        Dictionnaire de dates de fin d'observation, par série 'autre'
    data : dict
        Dictionnaire des séries de données 'Q' (pnd.DataFrame)
    data_other : dict
        Dictionnaire des séries de données 'autre' (pnd.DataFrame)

    """
    # ------------------------------------------------------------------
    # 0-- CONTROLES
    # ------------------------------------------------------------------
    check_args(filename is not None, 'filename')
    check_args(mode is not None, 'mode')
    check_args(model is not None, 'model')
    check_version_grp(grp_version)
    check_args(tag in ['obs', 'prv', 'sim'], 'tag')
    # ------------------------------------------------------------------
    # 1-- INITIALISATION
    # ------------------------------------------------------------------
    dtderobs_loc = init_args(dtderobs_loc, default=collections.OrderedDict())
    dtderobs = init_args(dtderobs, default=collections.OrderedDict())
    dtderobs_other = init_args(
        dtderobs_other, default=collections.OrderedDict())
    data = init_args(data, default=collections.OrderedDict())
    data_other = init_args(data_other, default=collections.OrderedDict())
    cols = {
        # Colonnes du fichier GRP - Version 2016
        ('GRPv2016', 'index_col'): 2, ('GRPv2016', 'loc'): 1,
        ('GRPv2016', 'Q'): 2, ('GRPv2016', 'RR'): 3, ('GRPv2016', 'TA'): 4,
        ('GRPv2016', 'ts'): None,
        # Colonnes du fichier GRP - Version 2018
        ('GRPv2018', 'index_col'): 3, ('GRPv2018', 'loc'): 1,
        ('GRPv2018', 'Q'): 3, ('GRPv2018', 'RR'): 4, ('GRPv2018', 'TA'): 5,
        ('GRPv2018', 'ts'): 2,
        # Colonnes du fichier GRP - Version 2020
        ('GRPv2020', 'index_col'): 3, ('GRPv2020', 'loc'): 1,
        ('GRPv2020', 'Q'): 3, ('GRPv2020', 'RR'): 4, ('GRPv2020', 'TA'): 5,
        ('GRPv2020', 'ts'): 2,
    }
    # ------------------------------------------------------------------
    # 2-- LECTURE DU FICHIER GRP
    # ------------------------------------------------------------------
    content = pnd.read_csv(
        filename, sep=str(";"), index_col=cols[(grp_version, 'index_col')],
        engine="python", skipfooter=1, date_parser=date_parser,
        na_values=GRP_NAVALUES)
    # ------------------------------------------------------------------
    # 3-- LIEUX, MODELES et SCENARIOS
    # ------------------------------------------------------------------
    stations = sorted(set(content.iloc[:, cols[(grp_version, 'loc')]]))
    if grp_version in ['GRPv2018', 'GRPv2020']:
        tss = sorted(set(content.iloc[:, cols[(grp_version, 'ts')]]))
        if len(tss) == 1:
            models = {tss[0]: model}
        else:
            models = {ts: set_grp_submodel(model, ts.strip()) for ts in tss}
    else:
        models = {model: model}
    cases = [(s, m) for s in stations for m in sorted(models.keys())]
    # Identifiant du scénario
    if tag == 'obs':
        scen = 'Tous'
#    else:
#        scen = get_grp_scenario(filename)  # TODO : paramètre scen de la fct
    # Pas d'export RR/TA des simulations
    if tag == 'sim':
        export_rr_ta = False
    # ------------------------------------------------------------------
    # 4-- TRAITEMENT SERIE PAR SERIE
    # ------------------------------------------------------------------
    # Boucle sur les couples (station, modele)
    for case in cases:
        s = case[0]
        ss = s.strip()
        m = case[1]
        mm = m.strip()
        model = models[m]
        if grp_version in ['GRPv2018', 'GRPv2020']:
            ts = convert_otamin_tstep(mm)
            t2 = content[
                (content.iloc[:, cols[(grp_version, 'loc')]] == s) &
                (content.iloc[:, cols[(grp_version, 'ts')]] == m)
            ]
            # Cas où le 'case' n'existe pas dans le fichier
            if t2.empty:
                continue
            sm = set_grp_simmodel(sim_model, mm)
        else:
            ts = tdelta(hours=1)
            t2 = content[content.iloc[:, cols[(grp_version, 'loc')]] == s]
            sm = sim_model
        dQ = t2.iloc[:, cols[(grp_version, 'Q')]]
        # ------------------------------------------------------------------
        # 4.1-- FICHIER GRP OBSERVATION
        # ------------------------------------------------------------------
        if tag == 'obs':
            if pnd.notnull(dQ).any():
                ddo = max(dQ[pnd.notnull(dQ)].index)
            else:
                ddo = min(dQ.index) - ts
                v_print("Avertissement : aucun débit observé au site '{}'. "
                        "L'instant de départ de la prévision est fixé à '{}'"
                        "".format(s, ddo.strftime('%Y-%m-%d %H:%M')),
                        verbose=True)
            dtderobs_loc[(ss, mm)] = ddo
        # ------------------------------------------------------------------
        # 4.2-- FICHIER GRP PREVISION
        # ------------------------------------------------------------------
        elif tag == 'prv':
            kP = IdSeries(ss, 'Q', model, scen)
            # Scénario de référence
            if scen == scen_P0:
                data[kP] = pnd.DataFrame(dQ)
                data[kP].columns = [tuple(kP)]
                try:
                    dtderobs[kP] = dtderobs_loc.get(
                        (ss, mm), min(dQ.index).to_datetime() - ts)
                except AttributeError:
                    dtderobs[kP] = dtderobs_loc.get(
                        (ss, mm), min(dQ.index).to_pydatetime() - ts)
            elif isunique(data=data, cdata=dQ, loc=ss, unique=unique):
                # Censure si la série de débit est identique
                # à une série déjà intégrée et si unique = True
                continue
            else:
                data[kP] = pnd.DataFrame(dQ)
                data[kP].columns = [tuple(kP)]
                try:
                    dtderobs[kP] = dtderobs_loc.get(
                        (ss, mm), min(dQ.index).to_datetime() - ts)
                except AttributeError:
                    dtderobs[kP] = dtderobs_loc.get(
                        (ss, mm), min(dQ.index).to_pydatetime() - ts)
        # ------------------------------------------------------------------
        # 4.3-- FICHIER GRP SIMULATION
        # ------------------------------------------------------------------
        elif tag == 'sim':
            kS = IdSeries(ss, 'Q', sm, set_idsim(scen))
            kP = IdSeries(ss, 'Q', model, scen)
            # Ignorer les séries non retenues lors du traitement des prévisions
            # notamment si on applique l'unicité des sorties
            if kP not in data:
                continue
            df = pnd.DataFrame(dQ)
            df.columns = [tuple(kS)]
            if kS in data_other:
                data_other[kS] = data_other[kS].combine_first(df)
            else:
                data_other[kS] = df
                try:
                    dtderobs[kS] = dtderobs_loc.get(
                        (ss, mm), min(df.index).to_datetime() - ts)
                except AttributeError:
                    dtderobs[kS] = dtderobs_loc.get(
                        (ss, mm), min(df.index).to_pydatetime() - ts)
        # ------------------------------------------------------------------
        # 4.4-- SERIES 'autre'
        # ------------------------------------------------------------------
        if export_rr_ta:
            for v in ['RR', 'TA']:
                dV = t2.iloc[:, cols[(grp_version, v)]]
                kV = IdSeries(ss, v, model, scen)
                data_other[kV] = pnd.DataFrame(dV)
                data_other[kV].columns = [tuple(kV)]
                if tag == 'obs':
                    dtderobs_other[kV] = dtderobs_loc[(ss, mm)]
                else:
                    try:
                        dtderobs_other[kV] = dtderobs_loc.get(
                            (ss, mm), min(dV.index).to_datetime() - ts)
                    except AttributeError:
                        dtderobs_other[kV] = dtderobs_loc.get(
                            (ss, mm), min(dV.index).to_pydatetime() - ts)
    # ------------------------------------------------------------------
    # 5-- RETOUR
    # ------------------------------------------------------------------
    return (dtderobs_loc, dtderobs, dtderobs_other, data, data_other)


def read_grp_intern(grp_version=None, sim_folder=None,
                    filename_bv=None, filename_simint=None,
                    sim_model=None, sim_depth=None, scen_P0=None,
                    date_der_obs_Q=None, date_der_obs=None, data=None,
                    verbose=False):
    """
    Lire et organiser les données des fichiers 'internes' de GRP

    Parameters
    ----------
    grp_version : str
        Version de GRP parmi ['GRPv2016', 'GRPv2018', 'GRPv2020']
    sim_folder : str
        Répertoire des simulations (GRPv2016: 'BD_Bassins')
        Par défaut: os.path.join(in_folder, '..', 'BD_Bassins')
    filename_bv : str
        Nom du fichier BASSIN contenant la surface du bassin
    filename_simint : str
        Nom du fichier contenant les simulations internes de GRP
    sim_model : str
        Code modèle POM des simulations
        Par défaut: None
    sim_depth : int
        Profondeur avant/après DtDerObs
        Par défaut: 120
    scen_P0 : str
        Identifiant du scénario pluie nulle. Utilisée lorsque les prévisions
        identiques sont ignorées (voir argument unique)
    date_der_obs_Q : dict
        Dictionnaire de dates de fin d'observation, par lieu
    date_der_obs : dict
        Dictionnaire de dates de fin d'observation, par série 'Q'
    data : dict
        Dictionnaire des séries de données 'Q' (pnd.DataFrame)
    verbose : bool
        Mode bavard ?
        Par défaut: False

    Returns
    -------
    date_der_obs_sim : dict
        Dictionnaire de dates de fin d'observation, par série 'Q'
    data_sim : dict
        Dictionnaire des séries de données 'Q' (pnd.DataFrame)

    """
    # ------------------------------------------------------------------
    # 0-- CONTROLES
    # ------------------------------------------------------------------
    # ------------------------------------------------------------------
    # 1-- INITIALISATION
    # ------------------------------------------------------------------
    data_sim = collections.OrderedDict()
    date_der_obs_sim = collections.OrderedDict()
    locs = sorted({k[0] for k in data.keys()})
    # ------------------------------------------------------------------
    # 2-- LECTURE DU FICHIER GRP
    # ------------------------------------------------------------------
    for loc in locs:
        # ------------------------------------------------------------------
        # 2.0-- SPECIFICITE PAR VERSION
        # ------------------------------------------------------------------
        if grp_version in ['GRPv2018', 'GRPv2020']:
            bv_filename = glob.glob(
                os.path.join(sim_folder, loc, '*', filename_bv))[0]
            filenames = glob.glob(
                os.path.join(sim_folder, loc, '*', filename_simint))
            tss = [convert_otamin_tstep(os.path.basename(os.path.dirname(f)))
                   for f in filenames]
        else:
            bv_filename = os.path.join(sim_folder, loc, filename_bv)
            filenames = [os.path.join(sim_folder, loc, filename_simint)]
            tss = [tdelta(hours=1)]
        # ------------------------------------------------------------------
        # 2.1-- SURFACE DU BASSIN
        # ------------------------------------------------------------------
        v_print("-> Lecture du fichier {}".format(bv_filename),
                verbose=verbose)
        bv_area = get_grp_area(bv_filename)
        if not isinstance(bv_area, float):
            continue
        # ------------------------------------------------------------------
        # 2.2-- PERIODE TEMPORELLE DES DONNEES INTERNES
        # ------------------------------------------------------------------
        sim_date = None
        for k in data:
            if k[0] == loc and k in date_der_obs:
                sim_date = date_der_obs[k]
                break
        if sim_date is None:
            continue
        # ------------------------------------------------------------------
        # 2.3-- LECTURE DU FICHIER INTERNE
        # ------------------------------------------------------------------
        for filename, ts in zip(filenames, tss):
            sim_period = [
                sim_date - int(sim_depth.total_seconds() / ts.total_seconds())
                * ts, sim_date]
            v_print("-> Lecture du fichier {}".format(filename),
                    verbose=verbose)
            sim_data = pnd.read_csv(
                filename, sep=str(";"), index_col=0, skiprows=[0, 1, 3, 4],
                converters={0: strip_dframe_values, 1: tofloat, 2: tofloat},
                date_parser=date_parser, na_values=GRP_NAVALUES)
        # ------------------------------------------------------------------
        # 2.4-- Conversion Q(mm/PDT) -> Q (M3/s)
        # ------------------------------------------------------------------
            ratio = bv_area * 1000 / ts.total_seconds()
            sim_data = sim_data.iloc[:, 0] * ratio
            subindex = [i for i in sim_data.index
                        if sim_period[0] <= i <= sim_period[-1]]
            sim_data = sim_data.reindex(subindex).to_frame()
            if grp_version in ['GRPv2018', 'GRPv2020']:
                sm = set_grp_simmodel(
                    sim_model, os.path.basename(os.path.dirname(filename)))
            else:
                sm = sim_model
            key = IdSeries(loc, 'Q', sm, set_idsim(scen_P0))
            data_sim[key] = sim_data
            data_sim[key].columns = [tuple(key)]
            try:
                date_der_obs_sim[key] = date_der_obs_Q.get(
                    loc, max(subindex).to_datetime())
            except AttributeError:
                date_der_obs_sim[key] = date_der_obs_Q.get(
                    loc, max(subindex).to_pydatetime())
    return (date_der_obs_sim, data_sim)


def set_grp_files(in_folder='.', mode=None, verbose=False):
    """
    Définir la liste des fichiers GRP

    Parameters
    ----------
    in_folder : str
        Dossier d'entree de GRP
    mode : str
        Mode d'utilisation
    verbose : bool
        Mode bavard ?
        Par défaut: False

    Returns
    -------
    tuple
        Nom ou liste de noms des fichiers:
            - bassin
            - observation
            - simulation interne
            - prévisions avec assimilation
            - prévisions sans assimilation (simulations)

    """
    check_mode(mode)
    realtime = bool(mode == 'TR')
    label = MODES[mode]
    v_print('Recherche des fichiers "{}" :'.format(label), verbose=verbose)
    filename_bv = GRP_FILENAMES[('bv', realtime)]
    filename_obs = GRP_FILENAMES[('obs', realtime)]
    filename_simint = GRP_FILENAMES[('sim', realtime)]
    filenames_prv = [
        f for f in os.listdir(in_folder)
        if os.path.basename(f).startswith(GRP_PREFIX[('prv', realtime)])]
    filenames_sim = [
        f for f in os.listdir(in_folder)
        if os.path.basename(f).startswith(GRP_PREFIX[('sim', realtime)])]
    return (filename_bv, filename_obs, filename_simint,
            filenames_prv, filenames_sim)


def set_grp_scenarios(scenarios=None, scen_P0=None, scen_PP=None,
                      filenames=None, filenames_sim=None):
    """
    Définir la liste des scénarios et l'identifiant du scénario pluie-nulle

    Parameters
    ----------
    scenarios : list of str
        Liste des identifiants des scénarios
    scen_P0 : str
        Identifiant du scénario pluie nulle. Utilisée lorsque les prévisions
        identiques sont ignorées (voir argument unique)
    scen_PP : str
        Identifiant du scénario pluie parfaite.
    filenames : list
        Noms des fichiers de prévision avec assimilation
    filenames_sim : list
        Noms des fichiers de prévision sans assimilation (simulation)

    Returns
    -------
    scenarios : list of str
        Liste des identifiants des scénarios
    scen_P0 : str
        Identifiant du scénario pluie nulle. Utilisée lorsque les prévisions
        identiques sont ignorées (voir argument unique)
    filenames : list
        Noms des fichiers de prévision avec assimilation
    filenames_sim : list
        Noms des fichiers de prévision sans assimilation (simulation)


    """
    # ---------------------------------------------------------------------
    # Scénarios disponibles selon les fichiers
    # ---------------------------------------------------------------------
    gscens = [get_grp_scenario(f) for f in filenames]
    # ---------------------------------------------------------------------
    # Cas AVEC sélection des scénarios par l'utilisateur
    # ---------------------------------------------------------------------
    if scenarios is not None:
        # -----------------------------------------------------------------
        # Avertissement si un scénario n'est pas proposé par GRP
        # -----------------------------------------------------------------
        for s in scenarios:
            if s not in gscens and \
                    not (s == scen_P0 and GRP_EXTRAS[0] in gscens) and \
                    not (s == scen_PP and GRP_EXTRAS[1] in gscens):
                v_print("Avertissement: le scenario {} ne correspond à aucun"
                        " fichier.".format(s), verbose=True)
        # -----------------------------------------------------------------
        # Ajout, si besoin, du scénario pluie nulle
        # -----------------------------------------------------------------
        scenarios = [s for s in gscens if s in scenarios]
        if scen_P0 in gscens and scen_P0 not in scenarios:
            scenarios.insert(0, scen_P0)
        elif GRP_EXTRAS[0] in gscens and scen_P0 is not None:
            scenarios.insert(0, GRP_EXTRAS[0])
        elif scen_P0 is None:
            scen_P0 = scenarios[0]
            v_print("Avertissement: le scenario considere en premier pour "
                    "l'unicité des sorties (ex: pluie nulle) n'est pas "
                    "défini. Le scenario {} servira de reference."
                    "".format(scen_P0), verbose=True)
        # -----------------------------------------------------------------
        # Ajout, si besoin, du scénario pluie parfaite
        # -----------------------------------------------------------------
        if scen_PP in gscens and scen_PP not in scenarios:
            scenarios.append(scen_PP)
        elif GRP_EXTRAS[1] in gscens and scen_PP is not None:
            scenarios.append(GRP_EXTRAS[1])
        # -----------------------------------------------------------------
        # Définition finale des scénarios et des listes de fichier
        # -----------------------------------------------------------------
        scenarios = [get_grp_scenario(f)
                     for f in filenames if get_grp_scenario(f) in scenarios]
        filenames = [f for f in filenames if get_grp_scenario(f) in scenarios]
        filenames_sim = [f for f in filenames_sim
                         if get_grp_scenario(f) in scenarios]
        if (len(scenarios) != len(filenames) or
                (filenames_sim and len(scenarios) != len(filenames_sim))):
            v_print("Avertissement: il y a une incohérence entre les "
                    "scénarios demandés et les fichiers disponibles.",
                    verbose=True)
        # -----------------------------------------------------------------
        # Remplacement des P0 / PP par un code utilisateur
        # -----------------------------------------------------------------
        for x in range(len(scenarios)):
            if scenarios[x] == GRP_EXTRAS[0] and scen_P0 is not None:
                scenarios[x] = scen_P0
            if scenarios[x] == GRP_EXTRAS[1] and scen_PP is not None:
                scenarios[x] = scen_PP

    # ---------------------------------------------------------------------
    # Cas SANS sélection des scénarios par l'utilisateur
    # ---------------------------------------------------------------------
    else:
        scenarios = gscens
        # -----------------------------------------------------------------
        # Remplacement des P0 / PP par un code utilisateur
        # -----------------------------------------------------------------
        for x in range(len(scenarios)):
            if scenarios[x] == GRP_EXTRAS[0] and scen_P0 is not None:
                scenarios[x] = scen_P0
            if scenarios[x] == GRP_EXTRAS[1] and scen_PP is not None:
                scenarios[x] = scen_PP
        # -----------------------------------------------------------------
        # Ajout du scénario pluie nulle si non défini par l'utilisateur
        # -----------------------------------------------------------------
        if scen_P0 not in scenarios:
            if scen_P0 is not None:
                v_print("Avertissement: le scenario considere en premier "
                        "(ex: pluie nulle) ne correspond pas aux sorties GRP. "
                        "Il est remplace par le code {}"
                        "".format(scen_P0), verbose=True)
            scen_P0 = scenarios[0]

    # ---------------------------------------------------------------------
    # Mise en avant du scénario pluie nulle
    # ---------------------------------------------------------------------
    idx0 = scenarios.index(scen_P0)
    scenarios.insert(0, scenarios.pop(idx0))
    filenames.insert(0, filenames.pop(idx0))
    try:
        filenames_sim.insert(0, filenames_sim.pop(idx0))
    except IndexError:
        pass

    return (scenarios, scen_P0, filenames, filenames_sim)


def set_grp_submodel(model, ts):
    """
    Définir un nouveau code modèle selon le code modèle principal
    et le pas de temps de la modélisation

    Parameters
    ----------
    model : str
        Code modèle POM
    ts : str
        Pas de temps de la modélisation GRP

    Returns
    -------
    submodel : str
        Nouveau code modèle POM

    """
    return model[:7] + clean_otamin_tstep(ts)[:3]


def set_grp_simmodel(model, ts):
    """
    Définir un nouveau code modèle selon le code modèle principal
    et le pas de temps de la modélisation
    CAS DE MODELE DE SIMULATION

    Parameters
    ----------
    model : str
        Code modèle POM
    ts : str
        Pas de temps de la modélisation GRP

    Returns
    -------
    simmodel : str
        Nouveau code modèle POM

    """
    if model is None:
        return None
    return model[:8] + clean_otamin_tstep(ts)[-2:]


# *****************************************************************************
# --- FONCTIONS OTAMIN ---
# *****************************************************************************
def check_version_otamin(version):
    """
    Vérifier la version de OTAMIN

    Parameters
    ----------
    version : str
        Version de OTAMIN

    Raises
    ------
    ValueError
        Si la version est incorrecte

    """
    if version not in OTAMIN_VERSIONS:
        raise ValueError("La version de OTAMIN est incorrecte")


def set_otamin_exports(sample=None, dtprod=None, forecast=None,
                       dirname=".", suffix='num', locs=None):
    """
    Lister les exports OTAMIN,
    avec sélection possible par lieu, grandeur, modèle

    Parameters
    ----------
    sample : dict
        Dictionnaire de pnd.DataFrame : les séries de données
    dtprod : datetime
        Instant de production
    forecast : bool
        Données de prévision (True) ou d'observation (False)

    Other Parameters
    ----------------
    dirname : str
        Répertoire du fichier
    suffix : str
        Mode de gestion du suffixe
        - 'num' : suffixe numérique incrémenté de 1 à ...
        - autre : suffixe est défini par le contenu de l'argument <suffix>
    locs : set, list
        Sélection par lieu (1 fichier par lieu)

    Returns
    -------
    list
        Liste des export: (filename, subsample)
    filename : str
        Nom du fichier prv
    subsample : dict
        Dictionnaire de pnd.DataFrame : les séries de données

    """
#    print()
#    print('='*80)
#    print(locs)
    # Contrôles
    check_args(sample is not None, 'sample')
    check_args(dtprod is not None, 'dtprod')
    check_args(forecast is not None, 'forecast')
    # Initialisation
    exports = []
    varnames = list({k.varname for k in sample.keys()})
    # Définir les configurations d'export
    if locs is None:
        clocs = [list({k.loc for k in sample.keys()})]
    else:
        clocs = [[c] for c in locs]
    cmodels = list({k.model for k in sample.keys()})
    if len(cmodels) > 1 and suffix != 'num':
        v_print("Avertissement : le suffix '{}' n'est pas compatible avec "
                "un jeu de données correspondant à plusieurs modèles. Ce "
                "paramètre est forcé à 'num'.".format(suffix),
                verbose=True)
        suffix = 'num'
    cases = [(c, m) for c in clocs for m in cmodels]
    # Construire la liste d'export
    for case in cases:
        # IdSeries à retenir
        keys = [k for k in sample.keys()
                if k.loc in case[0] and k.model in case[1]]
        # Sous-échantillon à retenir
        subsample = collections.OrderedDict()
        for k in keys:
            subsample[k] = sample[k]
        # Fichier du sous-échantillon
        basename = OTAMIN_PREFIX
        basename += dtprod.strftime("%Y%m%d_%H%M")
        if locs is not None and len(cmodels) == 1:
            basename += '_' + case[0][0]
        elif locs is not None:
            basename += '_' + case[0][0] + '-' + case[1]
        elif len(cmodels) > 1:
            basename += '_' + case[1]
        basename += OTAMIN_TAG[(forecast, 'RR' in varnames)]
        basename += OTAMIN_EXT
        filename = set_otamin_filename(
            basename=basename, dirname=dirname, suffix=suffix)
        filename = os.path.join(dirname, filename)
        exports.append((filename, subsample))
#    print('='*80)
    return exports


def set_otamin_filename(basename=None, dirname=".", suffix='num'):
    """
    Renvoie le nom du fichier avec ajout d'un suffixe pour éviter d'écraser
    un fichier existant sous le même nom de base.

    Parameters
    ----------
    basename : str
        Nom de base du fichier

    Other Parameters
    ----------------
    dirname : str
        Répertoire du fichier
    suffix : str
        Mode de gestion du suffixe
        - 'num' : suffixe numérique incrémenté de 1 à ...
        - autre : suffixe est défini par le contenu de l'argument <suffix>

    Returns
    -------
    str
        Nom du fichier unique

    """
    if basename is None:
        return None
    [shortname, extname] = os.path.splitext(basename)
    if suffix == 'num':
        filename = basename
        inc = 0
        while os.path.isfile(os.path.join(dirname, filename)):
            inc += 1
            filename = "{}_{}{}".format(shortname, inc, extname)
        return filename
    return "{}_{}{}".format(shortname, suffix, extname)


def clean_otamin_tstep(tstep=None):
    """
    Réduire le pas de temps fourni sous la forme
    d'une chaine de caractères 'jjJhhHmmM' en retirant
    - 00J
    - 00H
    - 00M

    Parameters
    ----------
    tstep : str
        'abC'

    """
    # Renvoi None si tstep est None
    if tstep is None:
        return None
    return tstep.replace('00J', '').replace('00H', '').replace('00M', '')


def convert_otamin_tstep(tstep=None):
    """
    Définir le pas de temps à partir de la chaine de caractères 'jjJhhHmmM'

    Parameters
    ----------
    tstep : str
        'jjJhhHmmM'

    Returns
    -------
    tdelta : timedelta
        pas de temps

    """
    # Renvoi None si tstep est None
    if tstep is None:
        return None
    # Contrôle de la chaine de caractères
    if not OTAMIN_RE_JHM.search(tstep):
        raise ValueError("La chaine de caractère '{}' est incorrecte"
                         "".format(tstep))
    # Découpage de la chaine de caractères
    d = int(tstep[0:2])
    h = int(tstep[3:5])
    m = int(tstep[6:8])
    return tdelta(days=d, hours=h, minutes=m)


def write_prv(dataset=None, date_der_obs=None, filename=None, date_prod=None,
              strict=True, probs=False, verbose=False):
    """
    Ecriture du fichier prv

    Parameters
    ----------
    dataset : dict of pnd.DataFrame
        clé : (station, grandeur, modele, scenario)
        valeur : pnd.DataFrame des séries
    date_der_obs : dict of datetimes
        clé : (station, grandeur, modele, scenario)
        valeur : date de la dernière observation liée à la série
    filename : str
        Nom du fichier PRV
    date_prod : datetime
        Date de production de la prévision

    Other Parameters
    ----------------
    strict : bool
        Exporter uniquement les valeurs post-dtderobs.
        Par défaut: True
    probs : bool
        Ecrire la ligne Probas avec -1
        Par défaut: False
    verbose : bool
        Mode bavard
        Par défaut: False

    Returns
    -------
    None

    """
    # -----------------------------------------------
    #    ETAPE 0 : Contrôles
    # -----------------------------------------------
    check_args(isinstance(dataset, dict), 'dataset')
    check_args(isinstance(date_der_obs, dict), 'date_der_obs')
    check_args(isinstance(filename, (str, unicode)), 'filename')
    check_args(date_prod is not None, 'date_prod')
    # -----------------------------------------------
    #    ETAPE 1 : Pré-traitement de l'entête
    # -----------------------------------------------
    en_tete_stations = ["Stations"]
    en_tete_grandeurs = ["Grandeurs"]
    en_tete_idseries = ["IdSeries"]
    en_tete_modeles = ["# Modeles"]
    en_tete_scenarios = ["# Scenarios"]
    en_tete_derobs = ["# DtDerObs"]
    en_tete_probas = ["# Probas"]
    for k in dataset.keys():
        en_tete_stations.append(k[0])
        en_tete_grandeurs.append(k[1])
        en_tete_idseries.append("{}_{}".format(k[2], k[3]))
        en_tete_modeles.append(k[2])
        en_tete_scenarios.append(k[3])
        en_tete_derobs.append(date_der_obs[k].strftime("%d-%m-%Y %H:%M"))
        en_tete_probas.append('-1')
    if strict:
        series = [v.reindex([i for i in v.index if i > date_der_obs[k]])
                  for k, v in dataset.items()]
        comment = ' (censuré à dtderobs)'
    else:
        series = list(dataset.values())
        comment = ''
    table = pnd.concat(series, axis=1)
    # -----------------------------------------------
    #    ETAPE 2 : Ecriture du fichier PRV
    # -----------------------------------------------
    # Entête
    with open(filename, 'w') as f:
        f.write("# Sorties de GRP du {} (UTC)\n"
                "".format(date_prod.strftime("%d/%m/%Y %H:%M"))
                .encode('utf-8'))
        f.write("# TZ ; UTC\n".encode('utf-8'))
        f.write("# Q ; m3/s \n".encode('utf-8'))
        f.write("# RR ; mm \n".encode('utf-8'))
        f.write("# TA ; Celsius \n".encode('utf-8'))
        # Écriture de l'en-tête du fichier prv
        headers = [en_tete_stations, en_tete_grandeurs, en_tete_idseries,
                   en_tete_modeles, en_tete_scenarios, en_tete_derobs]
        if probs:
            headers.append(en_tete_probas)
        for h in headers:
            f.write(";".join(h).encode('utf-8'))
            f.write("\n".encode('utf-8'))
    # Séries de données
    if not os.path.exists(filename):
        v_print("Avertissement: L'écriture de l'entête du fichier {} a échoué"
                "".format(filename), verbose=True)
        return None
    table[table == -99.9999] = np.nan
    table[table == -9.9999] = np.nan
    table.index = [d.to_pydatetime().strftime("%d-%m-%Y %H:%M")
                   for d in table.index]
    table.to_csv(filename, sep=str(";"), header=False, mode="a",
                 na_rep=-999.999, float_format='%.3f',
                 line_terminator='\n')
    v_print("Le fichier {}{} a été enregistré dans {}"
            .format(os.path.basename(filename), comment,
                    os.path.dirname(filename)), verbose=verbose)
    return None


# *****************************************************************************
# --- LANCER GRP2prv ---
# *****************************************************************************
def run_observations(in_folder=None, mode=None, model=None, otamin_folder=None,
                     grp_version=None, suffix=None, export_rr_ta=False,
                     onebyloc=False, filename_obs=None, verbose=False):
    """
    Traiter les observations de GRP

    Parameters
    ----------
    in_folder : str
        Dossier d'entree de GRP
    mode : str
        Mode d'utilisation
    model : str
        Code modèle POM
    otamin_folder : str
        Dossier de sortie OTAMIN
    grp_version : str
        Version de GRP parmi ['GRPv2016', 'GRPv2018', 'GRPv2020']
    suffix : str
        Nature du suffixe des fichiers uniques
        Par défaut: 'num'
    export_rr_ta : bool
        Exporter les pluies (RR) et température (TA) ?
        Par défaut: False
    onebyloc : bool
        Exporter 1 fichier par lieu ?
        Par défaut: False
    filename_obs : str
        Nom du fichier d'observation en sortie de GRP

    Other Parameters
    ----------------
    verbose : bool
        Mode bavard ?
        Par défaut: False

    Returns
    -------
    date_der_obs_Q : dict
        Dictionnaire de dates de fin d'observation, par lieu
    dtprod : datetime
        Instant de production

    """
    # --------------------------------------------------------------
    # 0 - Contrôles
    # --------------------------------------------------------------
    check_args(in_folder is not None, 'in_folder')
    check_args(mode is not None, 'mode')
    check_args(model is not None, 'model')
    check_args(otamin_folder is not None, 'otamin_folder')
    check_version_grp(grp_version)
    check_args(suffix is not None, 'suffix')
    check_args(export_rr_ta is not None, 'export_rr_ta')
    check_args(onebyloc is not None, 'onebyloc')
    check_args(filename_obs is not None, 'filename_obs')
    # --------------------------------------------------------------
    # 1 - Si le fichier n'existe pas
    # --------------------------------------------------------------
    if not os.path.exists(filename_obs):
        return collections.OrderedDict(), None
    # --------------------------------------------------------------
    # 2 - Si le fichier n'existe pas
    # --------------------------------------------------------------
    msg_content(
        content=[filename_obs],
        label="-> {} fichiers d'observation GRP trouvés dans " + in_folder,
        label1="-> {} fichier d'observation GRP trouvé dans " + in_folder,
        verbose=verbose)
    v_print('', verbose=verbose)
    v_print("TRAITEMENT DES OBSERVATIONS :", verbose=verbose)
    v_print("-> Lecture du fichier {}".format(filename_obs),
            verbose=verbose)
    # --------------------------------------------------------------
    #    2.1 - Détermination de DtProd, en UTC
    # --------------------------------------------------------------
    dtprod = dt.fromtimestamp(os.path.getmtime(filename_obs))
    dtprod = dtprod.replace(tzinfo=TIMEZONE)
    dtprod = dtprod.astimezone(pytz.utc)
    # --------------------------------------------------------------
    #    2.2 - Lecture
    # --------------------------------------------------------------
    (date_der_obs_Q, _, date_der_obs, _, observations) = read_grp_file(
        filename=filename_obs, mode=mode, model=model, grp_version=grp_version,
        tag='obs', export_rr_ta=export_rr_ta)
    # --------------------------------------------------------------
    #    2.3 - Ecriture du fichier PRV des obsverations associées
    # --------------------------------------------------------------
    if export_rr_ta:
        msg_content(
            content=observations,
            label="-> {} séries d'observation associée (RR, TA) trouvées.",
            label1="-> {} série d'observation associée (RR, TA) trouvée.",
            verbose=verbose)
        exports = set_otamin_exports(
            sample=observations, dtprod=dtprod, forecast=False,
            dirname=otamin_folder, suffix=suffix,
            locs={k.loc for k in observations.keys()} if onebyloc else None
        )
        for export in exports:
            write_prv(dataset=export[1], date_der_obs=date_der_obs,
                      filename=export[0], date_prod=dtprod, strict=False,
                      verbose=verbose)
    # --------------------------------------------------------------
    #    2.4 - Renvois
    # --------------------------------------------------------------
    return date_der_obs_Q, dtprod


def run_simulations(in_folder=None, mode=None, model=None, otamin_folder=None,
                    grp_version=None, out_folder=None,
                    scenarios=None, scen_P0=None,
                    suffix=None, onebyloc=False, sim_model=None,
                    sim_folder=None, sim_depth=None,
                    filenames_sim=None, filename_simint=None, filename_bv=None,
                    date_der_obs_Q=None, date_der_obs=None, data=None,
                    dtprod=None, verbose=False):
    """
    Traiter les simulations de GRP

    Parameters
    ----------
    in_folder : str
        Dossier d'entree de GRP
    mode : str
        Mode d'utilisation
    model : str
        Code modèle POM
    otamin_folder : str
        Dossier de sortie OTAMIN
    grp_version : str
        Version de GRP parmi ['GRPv2016', 'GRPv2018', 'GRPv2020']
    out_folder : str
        Dossier de sortie non-OTAMIN
        Par défaut: otamin_folder
    scenarios : list
        Identifiants des scénarios
    scen_P0 : str
        Identifiant du scénario pluie nulle. Utilisée lorsque les prévisions
        identiques sont ignorées (voir argument unique)
    suffix : str
        Nature du suffixe des fichiers uniques
        Par défaut: 'num'
    onebyloc : bool
        Exporter 1 fichier par lieu ?
        Par défaut: False
    sim_model : str
        Code modèle POM des simulations
        Par défaut: None
    sim_folder : str
        Répertoire des simulations
            - GRPv2016: 'BD_Bassins'
            - GRPv2018: 'BD_Modeles'
            - GRPv2020: 'BD_Modeles'
    sim_depth : int
        Profondeur avant/après DtDerObs
        Par défaut: 120
    filenames_sim : list
        Noms des fichiers de prévision sans assimilation (simulation)
    filename_simint : str
        Nom du fichier contenant les simulations internes de GRP
    filename_bv : str
        Nom du fichier BASSIN contenant la surface du bassin
    date_der_obs_Q : dict
        Dictionnaire de dates de fin d'observation, par lieu
    date_der_obs : dict
        Dictionnaire de dates de fin d'observation, par série 'Q'
    data : dict
        Dictionnaire des séries de données 'Q' (pnd.DataFrame)

    Other Parameters
    ----------------
    verbose : bool
        Mode bavard ?
        Par défaut: False

    """
    # --------------------------------------------------------------
    # 0 - Contrôles
    # --------------------------------------------------------------
    check_args(in_folder is not None, 'in_folder')
    check_args(mode is not None, 'mode')
    check_args(model is not None, 'model')
    check_args(otamin_folder is not None, 'otamin_folder')
    check_version_grp(grp_version)
    check_args(out_folder is not None, 'out_folder')
    check_args(scen_P0 is not None, 'scen_P0')
    check_args(suffix is not None, 'suffix')
    check_args(onebyloc is not None, 'onebyloc')
    check_args(sim_model is not None, 'sim_model')
    check_args(sim_folder is not None, 'sim_folder')
    sim_depth = init_args(sim_depth, default=120 * tdelta(hours=1))
    check_args(filenames_sim is not None, 'filenames_sim')
    check_args(filename_simint is not None, 'filename_simint')
    check_args(filename_bv is not None, 'filename_bv')
    check_args(date_der_obs_Q is not None, 'date_der_obs_Q')
    check_args(date_der_obs is not None, 'date_der_obs')
    check_args(data is not None, 'data')
    check_args(dtprod is not None, 'dtprod')
    # --------------------------------------------------------------
    # 1 - Simulation à partir du fichier interne
    # --------------------------------------------------------------
    v_print('', verbose=verbose)
    v_print("TRAITEMENT DES SIMULATIONS :", verbose=verbose)
    (date_der_obs_sim, data_sim) = read_grp_intern(
        grp_version=grp_version,
        sim_folder=sim_folder, filename_bv=filename_bv,
        filename_simint=filename_simint, sim_model=sim_model,
        sim_depth=sim_depth, scen_P0=scen_P0,
        date_der_obs_Q=date_der_obs_Q, date_der_obs=date_der_obs,
        data=data, verbose=verbose)
    msg_content(
        content=data_sim,
        label="-> {} séries de simulation interne trouvées.",
        label1="-> {} série de simulation interne trouvée.",
        verbose=verbose)
    # --------------------------------------------------------------
    # 2 - Simulation, i.e. prévisions sans assimilation
    # --------------------------------------------------------------
    filenames_sim = [os.path.join(in_folder, f) for f in filenames_sim]
    msg_content(
        content=filenames_sim,
        label="-> {} fichiers de simulation GRP trouvés dans " + in_folder,
        label1="-> {} fichier de simulation GRP trouvé dans " + in_folder,
        verbose=verbose)
    for scen, filename in zip(scenarios, filenames_sim):
        v_print("-> Lecture du fichier {}".format(filename),
                verbose=verbose)
        (_, date_der_obs_sim, _, data, data_sim) = \
            read_grp_file(
                filename=filename, mode=mode, model=model,
                grp_version=grp_version, sim_model=sim_model, scen=scen,
                tag='sim', data=data, data_other=data_sim,
                dtderobs_loc=date_der_obs_Q, dtderobs=date_der_obs_sim,
                export_rr_ta=False)
    # --------------------------------------------------------------
    # 3 - Ecriture des fichiers PRV
    # --------------------------------------------------------------
    msg_content(
        content=data_sim,
        label="-> {} séries de simulation GRP trouvés.",
        label1="-> {} série de simulation GRP trouvé.",
        verbose=verbose)
    # Export OTAMIN
    exports = set_otamin_exports(
        sample=data_sim, dtprod=dtprod, forecast=False,
        dirname=otamin_folder, suffix=suffix,
        locs={k.loc for k in data_sim.keys()} if onebyloc else None
    )
    for export in exports:
        write_prv(dataset=export[1], date_der_obs=date_der_obs_sim,
                  filename=export[0], date_prod=dtprod, verbose=verbose)
    # Export non-OTAMIN
    exports = set_otamin_exports(
        sample=data_sim, dtprod=dtprod, forecast=False,
        dirname=out_folder, suffix=suffix,
        locs={k.loc for k in data_sim.keys()} if onebyloc else None
    )
    for export in exports:
        write_prv(dataset=export[1], date_der_obs=date_der_obs_sim,
                  filename=export[0], date_prod=dtprod, strict=False,
                  probs=True, verbose=verbose)


def run_GRP2prv(in_folder=None, mode=None, model=None, otamin_folder=None,
                grp_version=None, otamin_version=None,
                out_folder=None, scenarios=None, scen_P0=None, scen_PP=None,
                suffix='num', export_rr_ta=False, unique=False, onebyloc=False,
                export_simul=False, sim_model=None, sim_folder=None,
                sim_depth=None, verbose=False):
    """
    Conversion des sorties de GRP (mode temps-réel ou mode différé)
    au format PRV Scores, compatible avec l'EAO et OTAMIN

    Parameters
    ----------
    in_folder : str
        Dossier d'entree de GRP
    mode : str
        Mode d'utilisation
    model : str
        Code modèle POM
    otamin_folder : str
        Dossier de sortie OTAMIN

    Other Parameters
    ----------------
    grp_version : str
        Version de GRP parmi ['GRPv2016', 'GRPv2018', 'GRPv2020']
    otamin_version : str
        Version de OTAMIN parmi ['OTAMINv2016', 'OTAMINv2018']
    out_folder : str
        Dossier de sortie non-OTAMIN
        Par défaut: otamin_folder
    scenarios : list of str
        Liste des identifiants des scénarios
    scen_P0 : str
        Identifiant du scénario pluie nulle. Utilisée lorsque les prévisions
        identiques sont ignorées (voir argument unique)
    scen_PP : str
        Identifiant du scénario pluie parfaite.
    suffix : str
        Nature du suffixe des fichiers uniques
        Par défaut: 'num'
    export_rr_ta : bool
        Exporter les pluies (RR) et température (TA) ?
        Par défaut: False
    unique : bool
        Conserver uniquement les prévisions distinctes ?
        Par défaut: False
    onebyloc : bool
        Exporter 1 fichier par lieu ?
        Par défaut: False
    export_simul : bool
        Export les simulations
        Par défaut: False
    sim_model : str
        Code modèle POM des simulations
        Par défaut: None
    sim_folder : str
        Répertoire des simulations (GRPv2016: 'BD_Bassins')
        Par défaut: os.path.join(in_folder, '..', 'BD_Bassins')
    sim_depth : timedelta
        Profondeur avant/après DtDerObs
        Par défaut: 120 * tdelta(hours=1)
    verbose : bool
        Mode bavard ?
        Par défaut: False

    Returns
    -------
    None

    """
    # ==================================================================
    #    1-- GESTION DES ARGUMENTS
    # ==================================================================
    grp_version = init_args(grp_version, default=GRP_VERSIONS[0])
    otamin_version = init_args(otamin_version, default=OTAMIN_VERSIONS[0])
    check_args(in_folder is not None, 'in_folder')
    check_args(mode is not None, 'mode')
    check_args(model is not None, 'model')
    check_args(otamin_folder is not None, 'otamin_folder')
    # Dossier où écrire les fichiers transcrits hors OTAMIN
    out_folder = init_args(out_folder, default=otamin_folder)
    # Répertoire des fichiers internes de GRP
    if grp_version in ['GRPv2018', 'GRPv2020']:
        d = os.path.abspath(os.path.join(in_folder, '..', 'BD_Modeles'))
        try:
            d = os.path.relpath(d)
        except ValueError:
            # Correctif si lettre de in_folder est différente de CWD
            #     exemple: cmd exécuté sur F: et in_folder = 'K:'
            pass
        sim_folder = init_args(sim_folder, default=d)
    else:
        d = os.path.abspath(os.path.join(in_folder, '..', 'BD_Bassins'))
        try:
            d = os.path.relpath(d)
        except ValueError:
            # Correctif si lettre de in_folder est différente de CWD
            #     exemple: cmd exécuté sur F: et in_folder = 'K:'
            pass
        sim_folder = init_args(sim_folder, default=d)
    # Dossier où écrire les fichiers transcrits hors OTAMIN
    sim_depth = init_args(sim_depth, default=120 * tdelta(hours=1))
    # ==================================================================
    #    2-- INITIALISATION DE LA CONVERSION
    # ==================================================================
    # --------------------------------------------------------------
    #    2.1 - Affichage des paramètres
    # --------------------------------------------------------------
    v_print('', verbose=verbose)
    v_print('='*50, verbose=verbose)
    v_print("Début de l'exécution de {}"
            "".format(os.path.basename(sys.argv[0])), verbose=verbose)
    v_print('='*50, verbose=verbose)
    v_print('', verbose=verbose)
    v_print("ARGUMENTS DE L'UTILISATEUR :")
    v_print("-> Dossier d'entree de GRP         {}"
            "".format(in_folder), verbose=verbose)
    v_print("-> Version de GRP                  {}"
            "".format(grp_version), verbose=verbose)
    v_print("-> Dossier de sortie OTAMIN        {}"
            "".format(otamin_folder), verbose=verbose)
    v_print("-> Version de OTAMIN               {}"
            "".format(otamin_version), verbose=verbose)
    v_print("-> Dossier de sortie non-OTAMIN    {}"
            "".format(out_folder), verbose=verbose)
    v_print("-> Mode d'utilisation              {}"
            "".format(mode), verbose=verbose)
    v_print("-> Modele                          {}"
            "".format(model), verbose=verbose)
    v_print("-> Scenarios                       {}"
            "".format(scenarios), verbose=verbose)
    v_print("-> Scenario Pluie Nulle            {}"
            "".format(scen_P0), verbose=verbose)
    v_print("-> Scenario Pluie Parfaite         {}"
            "".format(scen_PP), verbose=verbose)
    v_print("-> Suffixe des fichiers            {}"
            "".format(suffix), verbose=verbose)
    v_print("-> Exporter RR et TA ?             {}"
            "".format(export_rr_ta), verbose=verbose)
    v_print("-> Unicite des previsions ?        {}"
            "".format(unique), verbose=verbose)
    v_print("-> 1 fichier par lieu ?            {}"
            "".format(onebyloc), verbose=verbose)
    v_print("-> Exporter les simulations ?      {}"
            "".format(export_simul), verbose=verbose)
    v_print("-> Dossier des simulations         {}"
            "".format(sim_folder), verbose=verbose and export_simul)
    v_print('', verbose=verbose)
    # --------------------------------------------------------------
    #    2.2 - Fichiers à traiter
    # --------------------------------------------------------------
    v_print('', verbose=verbose)
    v_print("FICHIERS A LIRE :", verbose=verbose)
    # Définir la liste des fichiers GRP
    (filename_bv, filename_obs, filename_simint, filenames, filenames_sim) = \
        set_grp_files(in_folder=in_folder, mode=mode, verbose=verbose)
    #    Définir la liste des scénarios et l'identifiant du scénario P0
    (scenarios, scen_P0, filenames, filenames_sim) = \
        set_grp_scenarios(scenarios=scenarios,
                          scen_P0=scen_P0, scen_PP=scen_PP,
                          filenames=filenames, filenames_sim=filenames_sim)
    msg_content(
        content=filenames,
        label="-> {} fichiers de prévision GRP trouvés dans " + in_folder,
        label1="-> {} fichier de prévision GRP trouvé dans " + in_folder,
        verbose=verbose)
    if not filenames:
        msg_exit(verbose=verbose)
        return None
    # ==================================================================
    #    3-- TRAITEMENT DES OBSERVATIONS
    # ==================================================================
    filename_obs = os.path.join(in_folder, filename_obs)
    date_der_obs_Q, dtprod = run_observations(
        in_folder=in_folder, mode=mode, model=model,
        otamin_folder=otamin_folder, grp_version=grp_version,
        suffix=suffix, export_rr_ta=export_rr_ta, onebyloc=onebyloc,
        filename_obs=filename_obs, verbose=verbose)
    # ==================================================================
    #    4-- TRAITEMENT DES PREVISIONS
    # ==================================================================
    v_print('', verbose=verbose)
    v_print("TRAITEMENT DES PREVISIONS :", verbose=verbose)
    # --------------------------------------------------------------
    #    4.1 - Traitement du scénario 'Pluie nulle' en premier
    # --------------------------------------------------------------
    filenames = [os.path.join(in_folder, f) for f in filenames]
    # --------------------------------------------------------------
    #    4.2 - Détermination de DtProd, en UTC
    #          Conservation de la date de production
    #          du fichier Obs si connue
    # --------------------------------------------------------------
    if dtprod is None:
        dtprod = dt.fromtimestamp(
            max([os.path.getmtime(f) for f in filenames]))
        dtprod = dtprod.replace(tzinfo=TIMEZONE)
        dtprod = dtprod.astimezone(pytz.utc)
        v_print("-> Date de production : {}".format(dtprod),
                verbose=verbose and mode == 'TR')
    # --------------------------------------------------------------
    #    4.3 - Dictionnaire des pnd.DataFrame
    # --------------------------------------------------------------
    # Initialisation du dictionnaire des pnd.DataFrame
    data = collections.OrderedDict()
    date_der_obs = collections.OrderedDict()
    data_hmet = collections.OrderedDict()
    date_der_obs_hmet = collections.OrderedDict()
    for scen, filename in zip(scenarios, filenames):
        v_print("-> Lecture du fichier {}".format(filename), verbose=verbose)
        (_, date_der_obs, date_der_obs_hmet, data, data_hmet) = read_grp_file(
            filename=filename, mode=mode, model=model, grp_version=grp_version,
            scen=scen, scen_P0=scen_P0, tag='prv',
            data=data, data_other=data_hmet,
            dtderobs_loc=date_der_obs_Q, dtderobs=date_der_obs,
            dtderobs_other=date_der_obs_hmet, export_rr_ta=export_rr_ta,
            unique=unique)
    v_print('', verbose=verbose)
    v_print("EXPORT AU FORMAT PRV :", verbose=verbose)
    # --------------------------------------------------------------
    #    4.4 - Décompte des séries de prévision
    # --------------------------------------------------------------
    if unique:
        msg_content(
            content=data,
            label="-> {} séries de prévision uniques trouvées.",
            label1="-> {} série de prévision unique trouvée.",
            verbose=verbose)
    else:
        msg_content(
            content=data,
            label="-> {} séries de prévision trouvées.",
            label1="-> {} série de prévision trouvée.",
            verbose=verbose)
    # --------------------------------------------------------------
    #    4.5 - Export OTAMIN - Q, H
    # --------------------------------------------------------------
    exports = set_otamin_exports(
        sample=data, dtprod=dtprod, forecast=True,
        dirname=otamin_folder, suffix=suffix,
        locs={k.loc for k in data.keys()} if onebyloc else None
    )
    for export in exports:
        write_prv(dataset=export[1], date_der_obs=date_der_obs,
                  filename=export[0], date_prod=dtprod, verbose=verbose)
    # --------------------------------------------------------------
    #    4.6 - Export OTAMIN - RR, TA
    # --------------------------------------------------------------
    if export_rr_ta:
        if unique:
            msg_content(
                content=data_hmet,
                label="-> {} séries de données associées uniques trouvées.",
                label1="-> {} série de données associées unique trouvée.",
                verbose=verbose)
        else:
            msg_content(
                content=data_hmet,
                label="-> {} séries de données associées trouvées.",
                label1="-> {} série de données associées trouvée.",
                verbose=verbose)
        exports = set_otamin_exports(
            sample=data_hmet, dtprod=dtprod, forecast=True,
            dirname=otamin_folder, suffix=suffix,
            locs={k.loc for k in data_hmet.keys()} if onebyloc else None
        )
        for export in exports:
            write_prv(dataset=export[1], date_der_obs=date_der_obs_hmet,
                      filename=export[0], date_prod=dtprod, strict=False,
                      verbose=verbose)
    # ==================================================================
    #    5-- TRAITEMENT DES SIMULATIONS
    # ==================================================================
    if export_simul:
        run_simulations(
            in_folder=in_folder, mode=mode, model=model,
            otamin_folder=otamin_folder, grp_version=grp_version,
            out_folder=out_folder,
            scenarios=scenarios, scen_P0=scen_P0,
            suffix=suffix, onebyloc=onebyloc,
            sim_model=sim_model, sim_folder=sim_folder, sim_depth=sim_depth,
            filenames_sim=filenames_sim, filename_simint=filename_simint,
            filename_bv=filename_bv,
            date_der_obs_Q=date_der_obs_Q, date_der_obs=date_der_obs,
            data=data, dtprod=dtprod, verbose=verbose)
    # ==================================================================
    #    6-- FIN DE L'EXECUTION
    # ==================================================================
    msg_exit(verbose=verbose)
    return None


# *****************************************************************************
# ---     EXECUTION
# *****************************************************************************
if __name__ == "__main__":
    # ==================================================================
    #    1-- LECTURE DES ARGUMENTS
    # ==================================================================
    args = process_args()
    # ==================================================================
    #    2-- CONVERSION
    # ==================================================================
    run_GRP2prv(
        # Dossier où se situent les fichiers produits par le logiciel GRP
        in_folder=args.in_folder,
        # Mode de fonctionnement (TR pour temps réel ou TD pour temps différé)
        mode=args.mode,
        # Code du modèle GRP dans le référéntiel de la POM
        model=args.modele.encode('utf-8'),
        # Dossier où écrire les fichiers transcrits pour OTAMIN
        otamin_folder=args.otamin_folder,
        # Version de GRP
        grp_version=args.grp_version,
        # Version de OTAMIN
        otamin_version=args.otamin_version,
        # Dossier où écrire les fichiers transcrits hors OTAMIN
        out_folder=args.out_folder,
        # Scénarios dont il faut transcrire les prévisions
        scenarios=args.scenarios.split(',')
        if args.scenarios is not None else args.scenarios,
        # Scénario de pluie nulle
        scen_P0=args.scen_P0,
        # Scénario de pluie parfaite
        scen_PP=args.scen_PP,
        # Suffixe des noms de fichiers créés par le script
        suffix=args.modele.encode('utf-8')
        if args.suffix == 'mod' else args.suffix,
        # Exporter RR et TA
        export_rr_ta=args.rrta,
        # Exporter des prévisions uniques
        unique=args.unique,
        # Exporter par lieu
        onebyloc=args.onebyloc,
        # Exporter les simulations internes de GRP
        export_simul=args.simul is not None,
        # Code du modèle GRP simulation
        sim_model=args.simul,
        # Script bavard ou silencieux
        verbose=args.verbose
    )

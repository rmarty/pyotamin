#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyOTAMIN>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les sorties GRP Calage en fichier d'entrée pour OTAMIN Calage
Documentation: voir doc/html/grp4otamin.html

:Auteur:
    Renaud Marty (SPC Loire-Allier-Cher-Indre)

Version 2024.11.21
    - Ajout de l'option INPUT_ENCODING pour gérer les différents 
      encodages des fichiers d'entrée
    - Ajout de l'option APPLY_SCAL permettant d'ignorer les couples où 
      la valeur prévue est inférieure au seuil de calage de GRP

Version 2022.02.09
    - Compatibiltié avec GRPv2020
    - Refonte du namedtuple GrpRun
    - Correction de bogues mineurs

Version 2021.03.30
    - Compatibiltié avec GRPv2016 et GRPv2018
    - Compatibiltié avec OTAMINv2016 et OTAMINv2018
    - Ajout des paramètres INPUT_VERSION, OUTPUT_VERSION et POM_MODEL
    - Introduction de tests unitaires

Version 2019.04.04
    adaptation à OTAMIN-2018 : nommage des horizons horaires 024 -> 024H

"""
# *****************************************
# --- IMPORTS -----------------------------
# *****************************************
from collections import namedtuple
from datetime import datetime as dt, timedelta as td
from functools import partial
import glob
import os.path
import pandas as pnd

# *****************************************
# --- PARAMETRES --------------------------
# *****************************************
# ------------------------------------
# PARAMETRES : CONFIGURATION DES RUNS GRP
#   INPUT_DIRNAME   : Répertoire du dossier des sorties GRP
#   INPUT_SUFFIX    : Suffixe des fichiers à considérer
#   INPUT_VERSION   : Version de GRP
#   INPUT_ENCODING  : Encodage des fichiers de GRP
#   APPLY_SCAL      : Censurer les débits prévus < Scal.
#   LEADTIMES       : Si GRPv2016 = Echéances de prévision à considérer (int)
#                     Si GRPv2018 = Indices parmi les échéances possible
#   MODEL_PREFIX    : Préfixe du modèle selon la POM
#   OUTPUT_DIRNAME  : Répertoire de sortie
#   OUTPUT_VERSION  : Version d'OTAMIN
# ------------------------------------
INPUT_DIRNAME = 'tmp'
INPUT_SUFFIX = "_PP_P0P0.TXT"
INPUT_VERSION = "GRPv2018"  # "GRPv2016"
INPUT_ENCODING = 'iso-8859-1'
APPLY_SCAL = False
if INPUT_VERSION == "GRPv2016":
    # échéances libres (GRPv2016)
    # -> possibilité de ne conserver que certaines échéances d'entre elles
    LEADTIMES = [1]  # t0 + 1
    LEADTIMES.extend(range(3, 15, 3))  # t0 + 3 à t0 +12
    LEADTIMES.extend(range(18, 78, 6))  # t0 +18 à t0 +72
else:
    # echéances fixes (GRPv2018)
    # -> possibilité de ne conserver que les 10 premières échéances
    LEADTIMES = range(10)
POM_MODEL = "45gGRPd000"  # Code modèle au sens POM
OUTPUT_DIRNAME = r'tmp'
OUTPUT_VERSION = "OTAMINv2018"  # "OTAMINv2016"


# *****************************************
# --- FONCTIONS ---------------------------
# *****************************************
def check_compatibility(grp_version, otamin_version, timedelta):
    """
    Vérifier la compatibilité des versions de GRP et d'OTAMIN

    Parameters
    ----------
    grp_version : str
        Version de GRP
    otamin_version : str
        Version de OTAMIN
    timedelta : datetime.timedelta
        Pas de temps de GRP

    Raises
    ------
    ValueError
        Si les versions et pas de temps sont incompatibles

    """
    tests = [
        grp_version in ['GRPv2018', 'GRPv2020']
        and otamin_version == 'OTAMINv2016' and timedelta < td(hours=1)
    ]
    if any(tests):
        raise ValueError(
            "La version '{}' au pas de temps {} est incompatible "
            "avec '{}'".format(grp_version, otamin_version, timedelta))


def check_leadtimes(df, leadtimes, timedelta, version, verbose):
    """
    Vérifier la compatibilité des échéances demandées
    et des échéances du tableau de données

    Parameters
    ----------
    df : pandas.DataFrame
        Tableau de données du fichier GRP
    leadtimes : list of timedelta
        Echéances de prévision à extraire de GRP.
        Elles seront multipliées par le pas de temps du modèle GRP.
        Par défaut, toutes les échéances seront extraites
    timedelta : datetime.timedelta
        Pas de temps
    version : str
        Version de GRP
    verbose : bool
        Afficher les avertissements (True). Par défaut: False

    Returns
    -------
    leadtimes : list of timedelta
        Echéances de prévision à extraire de GRP.

    """
    lts = {c[1] for c in df.columns if c[0] == 'PRV'}
    if leadtimes is None:
        leadtimes = lts
    elif version == 'GRPv2016':
        leadtimes = {x * timedelta for x in leadtimes}
    elif version in ['GRPv2018', 'GRPv2020']:
        leadtimes = {
            x[1]
            for k, x in enumerate([c for c in df.columns if c[0] == 'PRV'])
            if k in leadtimes}
    else:
        raise NotImplementedError
    if len(lts.difference(leadtimes)) > 0 and verbose:
        print("Les échéances suivantes sont ignorées, comme demandé :"
              "\n - {}".format(
                  "\n - ".join(
                      [str(x) for x in sorted(lts.difference(leadtimes))])))
    if len(leadtimes.difference(lts)) > 0 and verbose:
        print("Les échéances suivantes sont indisponibles :\n - {}".format(
            "\n - ".join([str(x) for x in sorted(leadtimes.difference(lts))])))
    return sorted(lts.intersection(leadtimes))


def check_version_grp(version):
    """
    Vérifier la version de GRP

    Parameters
    ----------
    version : str
        Version de GRP

    Raises
    ------
    ValueError
        Si la version n'est pas incluse dans
        ['GRPv2016', 'GRPv2018', 'GRPv2020']

    """
    if version not in ['GRPv2016', 'GRPv2018', 'GRPv2020']:
        raise ValueError("La version de GRP est incorrecte")


def check_version_otamin(version):
    """
    Vérifier la version de OTAMIN

    Parameters
    ----------
    version : str
        Version de OTAMIN

    Raises
    ------
    ValueError
        Si la version n'est pas incluse dans ['OTAMINv2016', 'OTAMINv2018']

    """
    if version not in ['OTAMINv2016', 'OTAMINv2018']:
        raise ValueError("La version de OTAMIN est incorrecte")


def read_grp(filename, version, timedelta=None, encoding='utf-8'):
    """
    Lire le fichier de prévision GRP

    Parameters
    ----------
    filename : str
        Fichier de GRP à convertir
    version : str
        Version de GRP. Voir la fonction check_version_grp

    Other Parameters
    ----------------
    pandas.DataFrame
        Tableau de données du fichier

    """
    dt_fmt = None
    na_values = None
    if version == 'GRPv2016':
        dt_fmt = "%Y%m%d%H"
        na_values = '-9.9000'
    elif version in ['GRPv2018', 'GRPv2020'] and timedelta < td(hours=1):
        dt_fmt = "%Y%m%d%H%M"
        na_values = '-99.9000'
    elif version in ['GRPv2018', 'GRPv2020'] and timedelta < td(days=1):
        dt_fmt = "%Y%m%d%H"
        na_values = '-99.9000'
    elif version in ['GRPv2018', 'GRPv2020']:
        dt_fmt = "%Y%m%d"
        na_values = '-99.9000'
    else:
        raise NotImplementedError
    df = pnd.read_csv(
        filename, sep=';', header=0, skiprows=5, index_col=0,
        parse_dates=True, na_values=na_values,
        date_parser=lambda x: dt.strptime(x, dt_fmt),
        encoding=encoding  # encoding_errors='strict',
    )
    df = df.rename(columns=lambda x: x.strip())
    return df


def set_GrpRun(version):
    """
    Définir le run de Grp selon sa version

    Parameters
    ----------
    version : str
        Version de GRP

    Returns
    -------
    GrpRun : namedtuple
        Run de GRP

    """
    if version in ["GRPv2016", "GRPv2018"]:
        d = [None]*8
        d.append(version)
        return namedtuple(
            'GrpRun',
            ['loc', 'model', 'snow', 'error', 'leadtime', 'timedelta',
             'rainfall', 'periods', 'version'], defaults=d)
    if version in ['GRPv2020']:
        d = [None]*9
        d.append(version)
        return namedtuple(
            'GrpRun',
            ['loc', 'model', 'snow', 'error', 'timedelta', 'leadtime',
             'calthreshold', 'rainfall', 'periods', 'version'], defaults=d)
    raise NotImplementedError('set_GrpRun avec version={}'.format(version))


def split_colname(colname, version=None, timedelta=None):
    """
    Décomposer le nom des colonnes des données GRP

    Parameters
    ----------
    colname : str
        Identifiant de la colonne
    version : str
        Version de GRP
    timedelta : datetime.timedelta
        Pas de temps

    Returns
    -------
    tag : str
        Etiquette, parmi ['OBS', 'PRV']
    ltime : timedelta
        Echéance de prévision

    Examples
    --------
    >>> split_colname('OBS006', 'GRPv2016')
    ('OBS', 6:00:00)
    >>> split_colname('OBS00J06H00M', 'GRPv2018', td(hours=1))
    ('OBS', 6:00:00)

    """
    tag = colname[:3]
    ltime = colname[3:]
    if version == 'GRPv2016':
        return (tag, int(ltime) * timedelta)
    if version in ['GRPv2018', 'GRPv2020']:
        return (tag, str2td_grp(ltime))
    raise NotImplementedError


def split_grpbasename(filename, version):
    """
    Décomposer le nom du fichier GRP

    Parameters
    ----------
    filename : str
        Nom du fichier GRP
    version : str
        Version de GRP

    Returns
    -------
    namedtuple GrpRun
        Méta-données du run GRP

    Examples
    --------

    Cas de la version 2016

    >>> run = split_grpbasename('H_K0403010_GRP_SMN_TAN_003_PP_P0P0.TXT',
    ...                         'GRPv2016')
    >>> run
    GrpRun(loc='K0403010', model='GRP', snow='SMN', error='TAN',
           leadtime=datetime.timedelta(seconds=10800),
           timedelta=datetime.timedelta(seconds=3600),
           rainfall='PP', periods='P0P0', version='GRPv2016')


    Cas de la version 2018

    >>> run = split_grpbasename(
    ...     'H_U2345020_GRP_AMN_RNA_HOR_00J06H00M_PDT_00J01H00M_PP_P0P0.TXT',
    ...     'GRPv2018')
    >>> run
    GrpRun(loc='U2345020', model='GRP', snow='AMN', error='RNA',
           leadtime=datetime.timedelta(seconds=21600),
           timedelta=datetime.timedelta(seconds=3600),
           rainfall='PP', periods='P0P0', version='GRPv2018')


    Cas de la version 2020

    >>> run = split_grpbasename(
    ...     'H_RH10585x_GRP_SMN_TAN_PDT_00J01H00M_HOR_00J03H00M'
    ...     '_Scal_5d00_PP_P0P0.TXT', 'GRPv2020')
    >>> run
    GrpRun(loc='RH10585x', model='GRP', snow='SMN', error='TAN',
           timedelta=datetime.timedelta(seconds=3600),
           leadtime=datetime.timedelta(seconds=10800),
           calthreshold=5.0, rainfall='PP', periods='P0P0', version='GRPv2020')

    """
    metadata = os.path.splitext(os.path.basename(filename))[0].split('_')
    GrpRun = set_GrpRun(version)
    if version == 'GRPv2016':
        return GrpRun(
            *metadata[1:5],
            int(float(metadata[5])) * td(hours=1),
            td(hours=1),
            *metadata[6:8]
        )
    if version == 'GRPv2018':
        return GrpRun(
            *metadata[1:5],
            str2td_grp(metadata[6]),
            str2td_grp(metadata[8]),
            *metadata[9:11]
        )
    if version == 'GRPv2020':
        return GrpRun(
            *metadata[1:5],
            str2td_grp(metadata[6]),
            str2td_grp(metadata[8]),
            float(metadata[10].replace('d', '.')),
            *metadata[11:13]
        )
    raise NotImplementedError


def str2td_grp(text):
    """
    Convertir une chaîne de caractères en timedelta

    Parameters
    ----------
    text : str
        Chaîne de caractères de type jjJhhHmmM

    Returns
    -------
    timedelta

    Examples
    --------
    >>> str2td('00J00H05M')
    0:05:00
    >>> str2td('00J00H30M')
    0:30:00
    >>> str2td('00J01H00M')
    1:00:00
    >>> str2td('00J06H00M')
    6:00:00
    >>> str2td('01J00H00M')
    1 day, 0:00:00

    """
    return td(days=int(text[:-7]), hours=int(text[-6:-4]),
              minutes=int(text[-3:-1]))


def write_otamin(df, version, dirname, loc, model, leadtime, timedelta):
    """
    Ecire le fichier pour OTAMIN

    Parameters
    ----------
    df : pandas.DataFrame
        Tableau de données dont les colonnes sont ['OBS', 'PRV']
    version : str
        Version de OTAMIN. Voir la fonction check_version_otamin
    dirname : str
        Répertoire de destination pour les fichiers OTAMIN
    loc : str
        Identifiant du lieu de prévision
    model : str
        Identifiant du modèle de prévision
    leadtime : datetime.timedelta
        Echéance de prévision
    timedelta : datetime.timedelta
        Pas de temps du modèle

    Returns
    -------
    filename : str
        Nom du fichier OTAMIN créé

    """
    # --------------------------------
    # 1- Fichier OTAMIN
    # --------------------------------
    date_format = '%d-%m-%Y %H:%M'
    date_header = '# JJ-MM-AAAA HH:MM'
    if version == 'OTAMINv2016':
        lstr = "{0:03d}".format(int(leadtime / td(hours=1)))
    elif version == 'OTAMINv2018' and timedelta < td(hours=1):
        lstr = "{0:03d}M".format(int(leadtime / td(minutes=1)))
    elif version == 'OTAMINv2018' and timedelta < td(days=1):
        lstr = "{0:03d}H".format(int(leadtime / td(hours=1)))
    elif version == 'OTAMINv2018':
        lstr = "{0:03d}J".format(int(leadtime / td(days=1)))
        date_format = '%d-%m-%Y'
        date_header = '# JJ-MM-AAAA'
    filename = os.path.join(dirname, "{loc}_{model}_{lstr}.csv".format(
        loc=loc, model=model, lstr=lstr))
    # --------------------------------
    # 2- Données OTAMIN
    # --------------------------------
    # Forcer l'ordre des colonnes
    df['PREV'] = df['PRV']
    df = df.reindex(['OBS', 'PREV'], axis=1)
    # Retirer les lignes avec au moins 1 valeur manquante
    df = df.dropna(axis=0, how='any')
    # Titre de l'index
    df.index.name = date_header
    with open(filename, 'w', encoding='utf-8', newline='\n') as f:
        f.write('Stations;{}\n'.format(loc))
        f.write('Grandeurs;Q\n')
        f.write('Modeles;{}\n'.format(model))
    df.to_csv(
        filename,
        mode='a',
        sep=';',
        float_format='%.4f',
        na_rep='-99.900',
        header=True,
        date_format=date_format,
        line_terminator='\n'
    )
    return filename


# *****************************************
# --- RUN GRP4OTAMIN ----------------------
# *****************************************
def run_grp4otamin(grp_filename, grp_version, otamin_dirname, otamin_version,
                   pom_model, leadtimes=None, verbose=False,
                   grp_encoding='utf-8', grp_scal=False):
    """
    Exécuter grp4otamin

    Parameters
    ----------
    grp_filename : str
        Fichier de GRP à convertir
    grp_version : str
        Version de GRP. Voir la fonction check_version_grp
    otamin_dirname : str
        Répertoire de destination pour les fichiers OTAMIN
    otamin_version : str
        Version de OTAMIN. Voir la fonction check_version_otamin
    pom_model : str
        Codification du modèle selon la convention POM
    leadtimes : list of int
        Echéances de prévision à extraire de GRP.
        Elles seront multipliées par le pas de temps du modèle GRP.
        Par défaut, toutes les échéances seront extraites
    verbose : bool
        Afficher les avertissements (True). Par défaut: False
    grp_encoding : str
        Encodage des fichiers GRP. Défaut: 'utf-8'. Autres valeurs possibles:
            'ascii', 'iso-8859-1'
    grp_scal : bool
        Ignorer tous les débits prévus < seuil calage GRP ? Défaut: False.
    """
    # --------------------------------
    # 0- Contrôles
    # --------------------------------
    check_version_grp(grp_version)
    check_version_otamin(otamin_version)
    # --------------------------------
    # 1- Lecture du fichier GRP
    #    -> méta-données
    #    -> données
    # --------------------------------
    grp_run = split_grpbasename(grp_filename, grp_version)
    check_compatibility(grp_version, otamin_version, grp_run.timedelta)
    grp_df = read_grp(
        grp_filename, grp_version, grp_run.timedelta, grp_encoding)
    # --------------------------------
    # 2- Transformation du DataFrame pour OTAMIN
    #    -> timedelta dans les noms de colonnes
    #    -> dictionnaire de tableau de données {échéance: tableau}
    # --------------------------------
    df = grp_df.rename(columns=partial(
        split_colname, version=grp_version, timedelta=grp_run.timedelta))
    leadtimes = check_leadtimes(
        df, leadtimes, grp_run.timedelta, grp_version, verbose)
    dfs = {x: pnd.concat([df[c] for c in df.columns if c[1] == x], axis=1)
           for x in leadtimes}
    for x in dfs:
        dfs[x].columns = dfs[x].columns.droplevel(1)
        dfs[x].index = [i + x for i in dfs[x].index]
        # Remplacer les valeurs négatives par 0.001
        for c in dfs[x]:
            dfs[x][c] = dfs[x][c].mask(dfs[x][c] < 0.001, 0.001)
        if grp_scal:
            dfs[x] = dfs[x][dfs[x]['PRV'] >= grp_run.calthreshold]
    dfs = {x: dfs[x] for x in dfs if list(dfs[x].columns) == ['OBS', 'PRV']}
    del_ltimes = set(leadtimes).difference(set(dfs.keys()))
    if len(del_ltimes) > 0 and verbose:
        print("Les échéances suivantes sont ignorées car incomplètes :"
              "\n - {}".format(
                  "\n - ".join([str(x) for x in sorted(del_ltimes)])))
    # --------------------------------
    # 3- Export OTAMIN
    # --------------------------------
    filenames = []
    if not os.path.exists(otamin_dirname):
        os.makedirs(otamin_dirname)
    for x in dfs:
        filenames.append(write_otamin(
            dfs[x], otamin_version, otamin_dirname, grp_run.loc, pom_model,
            x, grp_run.timedelta))
    return filenames


# *****************************************
# --- MAIN SCRIPT -------------------------
# *****************************************
if __name__ == "__main__":
    targets = os.path.join(INPUT_DIRNAME, '*{}'.format(INPUT_SUFFIX))
    for filename in glob.glob(targets):
        # ------------------
        # Nom du fichier GRP
        # ------------------
        print("-- Fichier GRP : {}".format(os.path.basename(filename)))
        # -------------------------
        # Traitement du fichier GRP
        # -------------------------
        output_filenames = run_grp4otamin(
            filename, INPUT_VERSION, OUTPUT_DIRNAME, OUTPUT_VERSION,
            POM_MODEL, leadtimes=LEADTIMES, verbose=True, 
            grp_encoding=INPUT_ENCODING, grp_scal=APPLY_SCAL)
        # ------------------------
        # Noms des fichiers OTAMIN
        # ------------------------
        for f in output_filenames:
            print("   ---> Fichier OTAMIN : {}".format(os.path.basename(f)))

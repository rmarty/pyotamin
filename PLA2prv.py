#!/usr/bin/python
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyOTAMIN>.
# Copyright (C) 2015-2020  L. Berthet, J. Barat and R. Marty
#   (lionel.berthet@developpement-durable.gouv.fr)
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Conversion des sorties de PLATHYNES (mode temps-réel ou mode différé)
au format PRV Scores, compatible avec l'EAO et OTAMIN

:Auteur:
    Renaud Marty (SPC Loire-Allier-Cher-Indre)

Version : 2019.10.25
    Ajout option -f pour export dans un répertoire autre qu'OTAMIN
    Utilisé pour exporter des valeurs avant <DtDerObs>

Version : 2019.10.14
    Ajout option -g pour recalage avec décroissance linéaire

Version : 2019.10.01
    Première version, s'inspirant de GRP2prv

"""
# ------------------------------------
# IMPORTS
# ------------------------------------
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import division as division
from __future__ import print_function
from datetime import datetime as dt
import glob
import numpy as np
import os
import os.path
import pandas as pnd
import sys
import argparse
import collections
import pytz


# *****************************************************************************
# ---              FONCTIONS
# *****************************************************************************
def process_args():
    """
    Récupérer les arguments fournis au script <PLA2prv.py>
    """
    # -----------------------------------------------
    #    ETAPE 1 : Initialisation
    # -----------------------------------------------
    info = {
        "in_folder": {
            "short": "Dossier du modèle PLA",
            "help": "Dossier du modèle PLA"
        },
        "mode": {
            "short": "Mode de fonctionnement",
            "help": "Mode de fonctionnement :  temps reel (TR)/temps differe"
                    "(TD)"
        },
        "modele": {
            "short": "Code modele (POM)",
            "help": "Code du modele au sens de la POM (ex: 45sPLAd000)"
        },
        "otamin_folder": {
            "short": "Dossier de sortie OTAMIN",
            "help": "Dossier de sortie des fichiers PRV pour OTAMIN"
        },
        "out_folder": {
            "short": "Dossier d'export dans un répertoire autre qu'OTAMIN",
            "help": "Dossier d'export dans un répertoire autre qu'OTAMIN. "
                    "Utilisé pour exporter des valeurs avant DtDerObs"
        },
        "postprocessing": {
            "short": "[optionnel] Post-traitement des séries",
            "help": "[optionnel] Post-traitement des séries: recalage à t0 "
                    "avec décroissance linéaire de l'erreur sur <N> pas de "
                    "temps. Le code modèle de la série brute est remplacé par "
                    "<POM> et, si besoin, le code scénario modifié en "
                    "incrémentant par <dScen>. Cet argument peut être défini "
                    "plusieurs fois. "
                    "Exemple 1: -g 999 45sPLAdARR "
                    "Exemple 2: -g 999 45sPLAdARR 100 "
                    "Exemple 3: -g 999 45sPLAdARR 100 -g 30 45sPLAdPRO 200"
        },
        "rr": {
            "short": "[optionnel] Exporter RR ?",
            "help": "[optionnel] Exporter les previsions associees RR ?"
                    ""
        },
        "scenarios": {
            "short": "[optionnel] Liste des codes scenarios",
            "help": "[optionnel] Liste des codes scenarios (separateur: ',')"
        },
        "scen_P0": {
            "short": "[optionnel] Code du scenario de pluie nulle",
            "help": "[optionnel] Code du scenario de pluie nulle"
        },
        "suffix": {
            "short": "[optionnel] Suffixe des fichiers prv",
            "help": "[optionnel] Suffixe des fichiers prv, distinguant les "
                    "fichiers issus du meme instant de production"
                    " (défaut: 'num')"
        },
        "unique": {
            "short": "[optionnel] Exporter des previsions uniques ?",
            "help": "[optionnel] Exporter des previsions uniques ? "
                    "(default=False)"
        },
        "verbose": {
            "short": "[optionnel] Mode verbeux",
            "help": "[optionnel] Mode verbeux (default=False)"
        }
    }
    # -----------------------------------------------
    #    ETAPE 2 : Création de l'instance du parser
    # -----------------------------------------------
    description = "Conversion des sorties de PLATHYNES (mode temps-réel ou "\
        "mode différé) au format PRV Scores, compatible avec EAO/EXPRESSO et "\
        "OTAMIN"""
    parser = argparse.ArgumentParser(description=description)
    # -----------------------------------------------
    #    ETAPE 3 : ajout des arguments dans le parser
    # -----------------------------------------------
    parser.add_argument("-i", "--in-folder",
                        action="store",
                        dest="in_folder",
                        required=True,
                        help=info["in_folder"]["help"])
    parser.add_argument("-m", "--mode",
                        action="store",
                        dest="mode",
                        choices=["TR", "TD"],
                        required=True,
                        help=info["mode"]["help"])
    parser.add_argument("-p", "--modele-pom",
                        action="store",
                        dest="modele",
                        required=True,
                        help=info["modele"]["help"])
    parser.add_argument("-o", "--otamin-folder",
                        action="store",
                        dest="otamin_folder",
                        required=True,
                        help=info["otamin_folder"]["help"])
    parser.add_argument("-f", "--out-folder",
                        action="store",
                        dest="out_folder",
                        help=info["out_folder"]["help"])
    parser.add_argument("-g", "--post-processing",
                        dest="pprocessing",
                        action="append",
                        nargs='*',
                        help=info["postprocessing"]["help"])
    parser.add_argument("-d", "--hmet-data",
                        dest="rr",
                        action="store_true",
                        default=False,
                        help=info["rr"]["help"])
    parser.add_argument("-s", "--scenarios",
                        action="store",
                        dest="scenarios",
                        help=info["scenarios"]["help"])
    parser.add_argument("-0", "--scen-P0",
                        action="store",
                        dest="scen_P0",
                        help=info["scen_P0"]["help"])
    parser.add_argument("-x", "--suffix",
                        action="store",
                        dest="suffix",
                        choices=['num', 'mod'],
                        default='num',
                        help=info["suffix"]["help"])
    parser.add_argument("-u", "--unique-forecast",
                        dest="unique",
                        action="store_true",
                        default=False,
                        help=info["unique"]["help"])
    parser.add_argument("-v", "--verbose",
                        dest="verbose",
                        action="store_true",
                        default=False,
                        help=info["verbose"]["help"])
    return parser.parse_args()


def date_parser(date):
    """
    """
    return dt.strptime(date, "%Y-%m-%d %H:%M:%S")


def linear_postprocessing(index, values, depth, obs0):
    """Recalage par décroissance linéaire"""
    ppvalues = []
    ltime = 0
    for i, v in zip(index, values):
        if i:
            try:
                ratio = max(0, 1 - float(ltime) / depth)
            except ZeroDivisionError:
                ratio = 0
            if ltime == 0:
                sim0 = v
            ppval = max(0,
                        np.around(np.asscalar(v + ratio * (obs0 - sim0)), 3))
            ltime += 1
        else:
            ppval = np.nan
        ppvalues.append(ppval)
    return ppvalues


def read_prj(filename):
    """Lecture fichier prj"""
    locs = {}
    with open(filename, 'r') as f:
        for line in f.readlines():
            if line.startswith('Station hydro'):
                info = line.split(':')[-1].split(' ')
                locs.setdefault(info[1], info[2])
    return locs


def read_head_obs(filename):
    """Lecture de l'entete du fichier obs"""
    loc = None
    with open(filename, 'r') as f:
        line = f.readline()
        loc = line.split(',')[0].split(' ')[-1]
    return loc


def set_filename(basename=None, dirname=".", suffix='num'):
    """
    Renvoie le nom du fichier avec ajout d'un suffixe pour éviter d'écraser
    un fichier existant sous le même nom de base.

    3 ARGUMENTS
    basename                (str) Nom de base du fichier
    dirname                 (str) Chemin du fichier
    suffix                  (str) Mode de gestion du suffixe
        - 'num' : suffixe numérique incrémenté de 1 à ...
        - autre : suffixe est défini par le contenu de l'argument <suffix>
    """
    if basename is not None:
        [shortname, extname] = os.path.splitext(basename)
        if suffix == 'num':
            filename = basename
            inc = 0
            while os.path.isfile(os.path.join(dirname, filename)):
                inc += 1
                filename = "{}_{}{}".format(shortname, inc, extname)
            return filename
        return "{}_{}{}".format(shortname, suffix, extname)


def strip_dframe_values(text):
    """
    """
    try:
        return text.strip()
    except AttributeError:
        return text


def tofloat_dframe_values(text):
    """
    """
    try:
        return float(text.strip())
    except AttributeError:
        return text


def v_print(chaine='\n', encodage=sys.stdout.encoding):
    """
    Fonction pour afficher ou non la chaîne de caractères

    2 ARGUMENTS
    chaine                  chaîne de caractères à afficher
    encodage                encodage de la chaîne de caractères
    """
    if encodage is None:
        encodage = 'utf-8'
    print(chaine.encode(encodage))


def write_prv(dataset=None, date_der_obs=None, filename=None, date_prod=None,
              strict=True, probs=False):
    """
    Ecriture du fichier prv

    4 ARGUMENTS
    dataset         dictionnaire de pnd.DataFrame
        clé : (station, grandeur, modele, scenario)
        valeur : pdn.DataFrame des séries

    date_der_obs    dictionnaire de pnd.DataFrame
        clé : (station, grandeur, modele, scenario)
        valeur : date de la dernière observation liée à la série

    filename        (str) Nom du fichier PRV

    date_prod       (datetime) Date de production de la prévision

    strict          (bool) Exporter uniquement les valeurs post-dtderobs
                           Défaut: True
    probs          (bool) Ecrire la ligne Probas avec -1
                           Défaut: False
    """
    if isinstance(dataset, dict) and isinstance(date_der_obs, dict) and\
            isinstance(filename, (str, unicode)) and date_prod is not None:
        # -----------------------------------------------
        #    ETAPE 1 : Pré-traitement de l'entête
        # -----------------------------------------------
        en_tete_stations = ["Stations"]
        en_tete_grandeurs = ["Grandeurs"]
        en_tete_idseries = ["IdSeries"]
        en_tete_modeles = ["# Modeles"]
        en_tete_scenarios = ["# Scenarios"]
        en_tete_derobs = ["# DtDerObs"]
        en_tete_probas = ["# Probas"]
        en_tete_stations.extend([k[0]
                                 for k in dataset.keys()])
        en_tete_grandeurs.extend([k[1]
                                 for k in dataset.keys()])
        en_tete_idseries.extend(["{}_{}".format(k[2], k[3])
                                 for k in dataset.keys()])
        en_tete_modeles.extend([k[2]
                                for k in dataset.keys()])
        en_tete_scenarios.extend([k[3]
                                  for k in dataset.keys()])
        en_tete_derobs.extend([date_der_obs[k].strftime("%d-%m-%Y %H:%M")
                               for k in dataset.keys()])
        en_tete_probas.extend(['-1' for k in dataset.keys()])
        if strict:
            series = [v.reindex([i
                                 for i in v.index
                                 if i > date_der_obs[k]])
                      for k, v in dataset.items()]
        else:
            series = list(dataset.values())
        table = pnd.concat(
            series,
            axis=1
        )

        # -----------------------------------------------
        #    ETAPE 2 : Ecriture du fichier PRV
        # -----------------------------------------------
        # Entête
        with open(filename, 'w') as f:
            f.write("# Sorties de PLATHYNES du {} (UTC)\n"
                    "".format(date_prod.strftime("%d/%m/%Y %H:%M"))
                    .encode('utf-8'))
            f.write("# TZ ; UTC\n".encode('utf-8'))
            f.write("# Q ; m3/s \n".encode('utf-8'))
            f.write("# RR ; mm \n".encode('utf-8'))
            # Écriture de l'en-tête du tableau
            f.write(";".join(en_tete_stations).encode('utf-8'))
            f.write("\n".encode('utf-8'))
            f.write(";".join(en_tete_grandeurs).encode('utf-8'))
            f.write("\n".encode('utf-8'))
            f.write(";".join(en_tete_idseries).encode('utf-8'))
            f.write("\n".encode('utf-8'))
            f.write(";".join(en_tete_modeles).encode('utf-8'))
            f.write("\n".encode('utf-8'))
            f.write(";".join(en_tete_scenarios).encode('utf-8'))
            f.write("\n".encode('utf-8'))
            f.write(";".join(en_tete_derobs).encode('utf-8'))
            f.write("\n".encode('utf-8'))
            if probs:
                f.write(";".join(en_tete_probas).encode('utf-8'))
                f.write("\n".encode('utf-8'))
        # Séries de données
        if os.path.exists(filename):
            table[table == -99.9999] = np.nan
            table[table == -9.9999] = np.nan
            table.index = [d.to_pydatetime().strftime("%d-%m-%Y %H:%M")
                           for d in table.index]
            table.to_csv(filename, sep=str(";"), header=False, mode="a",
                         na_rep=-999.999, float_format='%.3f',
                         line_terminator='\n')
            if strict:
                dest = ' (censuré à dtderobs)'
            else:
                dest = ''
            v_print("Le fichier {}{} a été enregistré dans {}"
                    .format(
                        os.path.basename(filename),
                        dest,
                        os.path.dirname(filename)))


# *****************************************************************************
# ---              FONCTION PRINCIPALE
# *****************************************************************************
def main():
    """
    Conversion des sorties de PLATHYNES (mode temps-réel ou mode différé)
    au format PRV Scores, compatible avec l'EAO et OTAMIN
    """
    # ==================================================================
    #    1-- LECTURE DES ARGUMENTS
    # ==================================================================
    args = process_args()
    # Dossier où se situent les fichiers produits par le logiciel PLATHYNES
    in_folder = args.in_folder
    id_project = os.path.basename(in_folder)
    # Mode de fonctionnement (TR pour temps réel ou TD pour temps différé)
    mode = args.mode
    # Code du modèle PLATHYNES dans le référéntiel de la POM
    # pour identification par la PHyC)
    modele = args.modele.encode('utf-8')
    # Dossier où écrire les fichiers transcrits pour OTAMIN
    otamin_folder = args.otamin_folder
    # Dossier où écrire les fichiers transcrits hors OTAMIN
    out_folder = args.out_folder
    if out_folder is None:
        out_folder = otamin_folder
    # Scénarios dont il faut transcrire les prévisions
    if args.scenarios is not None:
        scenarios = args.scenarios.split(',')
    else:
        scenarios = None
    # Scénario de pluie nulle
    scen_P0 = args.scen_P0
    # Suffixe des noms de fichiers créés par le script
    # Si l'utilisateur demande la configuration 'mod',
    #   alors le suffix vaut le code modèle
    suffix = args.suffix
    if suffix == 'mod':
        suffix = modele
    # Exporter RR
    export_rr = args.rr
    # Exporter des prévisions uniques
    unique = args.unique
    # Post-traitements
    tprocessing = args.pprocessing
    if tprocessing is None:
        do_processing = False
    else:
        do_processing = True
        pprocessing = collections.OrderedDict()
        for t in tprocessing:
            try:
                n = int(float(t[0]))
            except ValueError:
                raise ValueError('N doit etre un entier')
            else:
                if n < 0:
                    raise ValueError('N doit etre un entier positif ou nul')
            pprocessing.setdefault(n, {'pom': '', 'dscen': ''})
            try:
                p = t[1]
            except IndexError:
                p = modele
            pprocessing[n]['pom'] = p
            try:
                s = int(float(t[2]))
            except IndexError:
                s = 0
            pprocessing[n]['ds'] = s
    # Script bavard ou silencieux
    verbose = args.verbose
    # ==================================================================
    #    2-- INITIALISATION DE LA CONVERSION
    # ==================================================================
    if verbose:
        v_print()
        v_print('='*50)
        v_print("Début de l'exécution de {}"
                "".format(os.path.basename(sys.argv[0])))
        v_print('='*50)
        v_print()
        v_print("ARGUMENTS DE L'UTILISATEUR :")
        v_print("-> Dossier d'entree                {}".format(in_folder))
        v_print("-> Dossier de sortie OTAMIN        {}".format(otamin_folder))
        v_print("-> Dossier de sortie non-OTAMIN    {}".format(out_folder))
        v_print("-> Projet                          {}".format(id_project))
        v_print("-> Mode d'utilisation              {}".format(mode))
        v_print("-> Modele                          {}".format(modele))
        v_print("-> Scenarios                       {}".format(scenarios))
        v_print("-> Scenario Pluie Nulle            {}".format(scen_P0))
        v_print("-> Suffixe des fichiers            {}".format(suffix))
        v_print("-> Exporter RR ?                   {}".format(export_rr))
        v_print("-> Unicite des previsions ?        {}".format(unique))
        v_print("-> Post-traitement ?               {}".format(do_processing))
        if do_processing:
            for n in pprocessing:
                v_print("   -> N = {}".format(n))
                v_print("      -> Modele = {}".format(pprocessing[n]['pom']))
                v_print("      -> dScen  = {}".format(pprocessing[n]['ds']))

        v_print('')
    # --------------------------------------------------------------
    #    2.1 - Paramètres temporels
    # --------------------------------------------------------------
    # Fuseau horaire de Europe/Paris <timezone>
    timezone = pytz.timezone('Europe/Paris').localize(dt.now()).tzinfo
    # Dictionnaire contenant les DtDerObs
    dtderobs = None
    dtprod = None
    # --------------------------------------------------------------
    #    2.2 - Fichiers à traiter
    # --------------------------------------------------------------
    v_print('')
    v_print("FICHIERS A LIRE :")
    filenames = glob.glob(os.path.join(
        in_folder, 'Ev_*', 'Results',
        '*_ResultsRaw.txt'
    ))
    filenames_obs = glob.glob(os.path.join(
        in_folder, 'Ev_*',
        '*.mqo'
    ))
    if scenarios is not None:
        if scen_P0 is not None and scen_P0 not in scenarios:
            scenarios.insert(0, scen_P0)
        elif scen_P0 is None:
            scen_P0 = scenarios[0]
            v_print("Avertissement: le scenario considere en premier pour "
                    "l'unicité des sorties (ex: pluie nulle) n'est pas défini."
                    " Le scenario {} servira de reference."
                    "".format(scen_P0))
        filenames = [f for f in filenames
                     if os.path.basename(f).split('_')[-2] in scenarios]
        filenames_obs = [f for f in filenames_obs
                         if os.path.basename(f).split('_')[0] in scenarios]
    else:
        scenarios = [os.path.basename(f).split('_')[-2]
                     for f in filenames]
        if scen_P0 not in scenarios:
            scen_P0 = scenarios[0]
            v_print("Avertissement: le scenario considere en premier "
                    "(ex: pluie nulle) ne correspond pas aux sorties PLATHYNES"
                    ". Il est remplace par le code {}"
                    "".format(scen_P0))
    if len(filenames) > 1:
        v_print("-> {} fichiers de prévision PLATHYNES trouvés dans {}"
                .format(len(filenames), in_folder))
    else:
        v_print("-> {} fichier de prévision PLATHYNES trouvé dans {}"
                "".format(len(filenames), in_folder))
    if len(filenames_obs) > 1:
        v_print("-> {} fichiers d'observation PLATHYNES trouvés dans {}"
                .format(len(filenames_obs), in_folder))
    else:
        v_print("-> {} fichier d'observation PLATHYNES trouvé dans {}"
                "".format(len(filenames_obs), in_folder))

    if not filenames:
        v_print()
        v_print('='*50)
        v_print(" Fin de l'exécution de {}"
                "".format(os.path.basename(sys.argv[0])))
        v_print('='*50)
        sys.exit(0)
    # --------------------------------------------------------------
    #    2.3 - Informations du projet
    # --------------------------------------------------------------
    v_print('')
    v_print("INFORMATIONS SUR LE PROJET :")
    filename_proj = os.path.relpath(
        os.path.join(in_folder, '..', '{}.prj'.format(id_project)))
    v_print("-> Lecture du fichier {}".format(filename_proj))
    locs = read_prj(filename_proj)
    # ==================================================================
    #    3-- TRAITEMENT DES OBSERVATIONS
    # ==================================================================
    id_loc = id_project
    filename_obs = filenames_obs[0]
    if os.path.exists(filename_obs):
        # --------------------------------------------------------------
        #    3.0 - Lecture
        # --------------------------------------------------------------
        loc = read_head_obs(filename_obs)
        id_loc = locs[loc]
        v_print('-> Exutoire du modèle : {} ({})'.format(loc, id_loc))
        # --------------------------------------------------------------
        #    3.1 - Lecture
        # --------------------------------------------------------------
        v_print('')
        v_print("TRAITEMENT DES OBSERVATIONS :")
        v_print("-> Lecture du fichier {}".format(filename_obs))
        obs = pnd.read_fwf(
            filename_obs,
            colspecs=[(0, 20), (21, 33)],
            names=[id_loc],
            index_col=0,
            converters={0: tofloat_dframe_values},
            skiprows=6,
            na_values='-1.000',
            keep_default_na=True,
            parse_dates=True,
            date_parser=date_parser
        )
        # --------------------------------------------------------------
        #    3.2 - Détermination de DtDerObs, en UTC
        # --------------------------------------------------------------
        dtderobs = obs.last_valid_index().to_pydatetime()
        valderobs = obs.loc[dtderobs][id_loc]
        v_print("-> Date de dernière observation pour le lieu {} : {}"
                "".format(id_loc, dtderobs))
        v_print("-> Valeur de dernière observation pour le lieu {} : {}"
                "".format(id_loc, valderobs))

    # ==================================================================
    #    4-- TRAITEMENT DES PREVISIONS
    # ==================================================================
    v_print('')
    v_print("TRAITEMENT DES PREVISIONS :")
    # --------------------------------------------------------------
    #    4.1 - Traitement du scénario 'Pluie nulle' en premier
    # --------------------------------------------------------------
    f0 = [f for f in filenames
          if os.path.basename(f).split('_')[-2] == scen_P0]
    f0.extend([f for f in filenames
               if os.path.basename(f).split('_')[-2] != scen_P0])
    filenames = [f for f in f0]
    # --------------------------------------------------------------
    #    4.2 - Détermination de DtProd, en UTC
    #          Conservation de la date de production
    #          du fichier Obs si connue
    # --------------------------------------------------------------
    if dtprod is None:
        dtprod = dt.fromtimestamp(max([os.path.getmtime(f)
                                       for f in filenames]))
        dtprod = dtprod.replace(tzinfo=timezone)
        dtprod = dtprod.astimezone(pytz.utc)
        if mode == 'TD':
            v_print("-> Date de production pour le lieu {} : {}"
                    "".format(id_loc, dtderobs))
        else:
            v_print("-> Date de production pour le lieu {} : {}"
                    "".format(id_loc, dtprod))

    # --------------------------------------------------------------
    #    4.3 - Dictionnaire des pnd.DataFrame
    # --------------------------------------------------------------
    # Initialisation du dictionnaire des pnd.DataFrame
    data = collections.OrderedDict()
    date_der_obs = collections.OrderedDict()
    val_der_obs = collections.OrderedDict()
    data_hmet = collections.OrderedDict()
    date_der_obs_hmet = collections.OrderedDict()
    for filename in filenames:
        v_print("-> Lecture du fichier {}".format(filename))
        # pnd.DataFrame du fichier courant
        raw_data = pnd.read_fwf(
            filename,
            widths=[19, 16, 13, 11],
            names=['RR', 'Q', 'X'],
            index_col=0,
            converters={0: tofloat_dframe_values,
                        1: tofloat_dframe_values,
                        2: tofloat_dframe_values},
            skiprows=6,
            na_values='-1.000',
            keep_default_na=True,
            parse_dates=True,
            date_parser=date_parser
        )
        # Code scénario du fichier courant
        scenario = os.path.basename(filename).split('_')[-2]
        # Clé de la série Q
        serie_Q = (id_loc, 'Q', modele, scenario)
        # Contenu de la série Q
        selec_Q = raw_data['Q']  # .to_frame()
        # Contenu de la série RR si besoin
        if export_rr:
            serie_RR = (id_loc, 'RR', modele, scenario)
            selec_RR = raw_data['RR']  # .to_frame()
        # Traitement des séries Q et RR
        if scenario == scen_P0:
            data[serie_Q] = pnd.DataFrame(selec_Q)
            data[serie_Q].columns = [serie_Q]
            date_der_obs[serie_Q] = dtderobs
            val_der_obs[serie_Q] = valderobs
            if export_rr:
                data_hmet[serie_RR] = pnd.DataFrame(selec_RR)
                data_hmet[serie_RR].columns = [serie_RR]
                date_der_obs_hmet[serie_RR] = dtderobs
        else:
            # Censure si la série de débit est identique
            # à une série déjà intégrée dans le dictionnaire
            # de pnd.DataFrame <data>
            match = False
            if data and unique:
                selec_Qtest = pnd.DataFrame(selec_Q)
                for serie_Qtest in data:
                    if serie_Qtest[0] == id_loc and serie_Qtest[1] == 'Q':
                        selec_Qtest.columns = [serie_Qtest]
                        if data[serie_Qtest].equals(selec_Qtest):
                            match = True
                            break
            if not match:
                data[serie_Q] = pnd.DataFrame(selec_Q)
                data[serie_Q].columns = [serie_Q]
                date_der_obs[serie_Q] = dtderobs
                val_der_obs[serie_Q] = valderobs
                if export_rr:
                    data_hmet[serie_RR] = pnd.DataFrame(selec_RR)
                    data_hmet[serie_RR].columns = [serie_RR]
                    date_der_obs_hmet[serie_RR] = dtderobs

    # ==================================================================
    #    5-- POST-TRAITEMENT
    # ==================================================================
    if do_processing:
        v_print('')
        v_print("POST-TRAITEMENTS :")
        counter = 0
        keys = list(data.keys())
        for key in keys:
            counter += 1
            loc = key[0]
            var = key[1]
            mod = key[2]
            sce = int(float(key[3]))
            v_print("-> Série #{}".format(counter))
            v_print('   -> Scénario brut       : {0} / {1} / {2:04d}'
                    ''.format(loc, mod, sce))
            for n in pprocessing:
                rmod = pprocessing[n]['pom']
                rsce = pprocessing[n]['ds'] + sce
                v_print('   -> Scénario recalé {0:03d} : {1} / {2} / {3:04d}'
                        ''.format(n, loc, rmod, rsce))
                values = linear_postprocessing(
                    data[key].index >= date_der_obs[key],
                    data[key].values,
                    n,
                    val_der_obs[key]
                )
                rkey = (loc, var, rmod, str(rsce))
                data[rkey] = pnd.DataFrame(values, index=data[key].index)
                date_der_obs[rkey] = date_der_obs[key]

    # ==================================================================
    #    6-- EXPORT AU FORMAT PRV
    # ==================================================================
    v_print('')
    v_print("EXPORT AU FORMAT PRV :")
    # --------------------------------------------------------------
    #    6.1 - Décompte des séries de prévision
    # --------------------------------------------------------------
    if unique:
        if len(data) > 1:
            v_print("-> {} séries de prévision uniques trouvées."
                    .format(len(data)))
        else:
            v_print("-> {} série de prévision unique trouvée."
                    .format(len(data)))
    else:
        if len(data) > 1:
            v_print("-> {} séries de prévision trouvées."
                    .format(len(data)))
        else:
            v_print("-> {} série de prévision trouvée."
                    .format(len(data)))
    # --------------------------------------------------------------
    #    6.2 - Export OTAMIN - Q, H
    # --------------------------------------------------------------
    filename = set_filename(
        basename="PLA_B_{}.prv".format(
            dtprod.strftime("%Y%m%d_%H%M")),
        dirname=otamin_folder,
        suffix=suffix)
    write_prv(dataset=data,
              date_der_obs=date_der_obs,
              filename=os.path.join(otamin_folder, filename),
              date_prod=dtprod)
    # --------------------------------------------------------------
    #    6.3 - Export hors OTAMIN - Q, H
    # --------------------------------------------------------------
    filename = set_filename(
        basename=os.path.basename(filename),
        dirname=out_folder,
        suffix=suffix)
    write_prv(dataset={k: v
                       for k, v in data.items()
                       if k[2] == modele and k[3] == scen_P0},
              date_der_obs=date_der_obs,
              filename=os.path.join(out_folder, filename),
              date_prod=dtprod,
              strict=False,
              probs=True)
    # --------------------------------------------------------------
    #    6.4 - Export OTAMIN - RR, TA
    # --------------------------------------------------------------
    if export_rr:
        if unique:
            if len(data_hmet) > 1:
                v_print("-> {} séries de données associées uniques trouvées."
                        .format(len(data_hmet)))
            else:
                v_print("-> {} série de données associées unique trouvée."
                        .format(len(data_hmet)))
        else:
            if len(data_hmet) > 1:
                v_print("-> {} séries de données associées trouvées."
                        .format(len(data_hmet)))
            else:
                v_print("-> {} série de données associées trouvée."
                        .format(len(data_hmet)))
        filename_rr = set_filename(
            basename="PLA_B_{}_DA.prv".format(
                dtprod.strftime("%Y%m%d_%H%M")),
            dirname=otamin_folder,
            suffix=suffix)
        write_prv(dataset=data_hmet,
                  date_der_obs=date_der_obs_hmet,
                  filename=os.path.join(otamin_folder, filename_rr),
                  date_prod=dtprod,
                  strict=False)

    # ==================================================================
    #    7-- FIN DE L'EXECUTION
    # ==================================================================
    v_print()
    v_print('='*50)
    v_print(" Fin de l'exécution de {}".format(os.path.basename(sys.argv[0])))
    v_print('='*50)


# *****************************************************************************
# ---     EXECUTION
# *****************************************************************************
if __name__ == "__main__":
    main()

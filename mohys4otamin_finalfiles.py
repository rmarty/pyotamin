#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyOTAMIN>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Script créant les fichiers pour OTAMIN à partir des séries produites par mohys4otamin.py
"""
# *****************************************
#               PARAMETRES
# *****************************************
import collections
import glob
import os.path

INVENTORY_FILENAME = 'bilan.csv'
RESULTS_DIRNAME = '.'
OTAMIN_DIRNAME = '_OTAMIN_in'
MODEL_POM = '45hMOHd'
TABLES = {
    999: '45hMOHdARR',
    0: '45hMOHdBRU'
}


def read_inventory(filename):
    """
    """
    data = collections.OrderedDict()
    with open(filename, 'r', encoding='utf-8', newline='\n') as f:
        header = [h.strip()
                  for h in f.readline().strip('\n').split(';')]
        for line in f.readlines():
            info = line.strip('\n').split(';')
            info = {h: int(info[k]) if h == 'Durée recalage' else info[k]
                    for k, h in enumerate(header)}
            data.setdefault(info['Code'], info)
    if data:
        return data
    return None


def write_finalfile(input_filename='', model='',
                    otamin='', output_dirname=''):
    """
    """
    input_basename = os.path.basename(input_filename)
    output_basename = input_basename.replace(model, otamin)
    output_filename = os.path.join(output_dirname, output_basename)
    with open(input_filename, 'r', encoding='utf-8', newline='\n') as fi:
        with open(output_filename, 'w', encoding='utf-8', newline='\n') as fo:
            for line in fi.readlines():
                line2 = line.replace(model, otamin)
                fo.write(line2)
    return output_filename


print('-- Création des fichiers pour OTAMIN '
      'selon les temps de recalage retenus')
INVENTORY = read_inventory(INVENTORY_FILENAME)
for code in INVENTORY:
    print('   - Lieu = {}'.format(code))
    errors = list(set([0, 999, INVENTORY[code]['Durée recalage']]))
    models = ['{0}{1:03d}'.format(MODEL_POM, e) for e in errors]
    otamins = [TABLES[e] if e in TABLES else '45hMOHdPRO'
               for e in errors]
    for model, otamin in zip(models, otamins):
        filenames = glob.glob(os.path.join(
            RESULTS_DIRNAME,
            code + '*',
            model + '*',
            '{}_{}*.csv'.format(code, model)
        ))
        print('     - {} ===> {} : traitement de {} fichiers'
              ''.format(model, otamin, len(filenames)))
        for filename in filenames:
            msg = write_finalfile(
                input_filename=filename,
                model=model,
                otamin=otamin,
                output_dirname=OTAMIN_DIRNAME
            )
#            print(msg)
#            break
#        break
#    break

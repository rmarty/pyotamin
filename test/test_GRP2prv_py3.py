#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyOTAMIN>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for GRP2prv_py3 in GRP2prv_py3

To run all tests just type:
    python -m unittest test_GRP2prv_py3

To run only a class test:
    python -m unittest test_GRP2prv_py3.TestGRP2prv_py3

To run only a specific test:
    python -m unittest test_GRP2prv_py3.TestGRP2prv_py3.test_checks

"""
# Imports
from datetime import datetime as dt, timedelta as td
import filecmp
import glob
import os
import unittest
import sys
sys.path.insert(0, '..')

# Imports
from GRP2prv_py3 import (
    check_version_grp, check_version_otamin,
    date_parser, set_idsim,
    clean_otamin_tstep, convert_otamin_tstep,
    get_grp_area, get_grp_scenario,
    set_grp_simmodel, set_grp_submodel, set_grp_files, set_grp_scenarios,
    run_GRP2prv_py3
)


def remove_prv_files(p):
    """
    Supprimer les fichiers prv d'un répertoire
    """
    for f in glob.glob(os.path.join(p, '*.prv')):
        os.remove(f)


class TestGRP2prv_py3(unittest.TestCase):
    """
    Class test for common functions
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # ====================================================================
        self.home = 'GRP2prv_py3'
        self.grp16 = os.path.join(self.home, 'grp16')
        self.grp18 = os.path.join(self.home, 'grp18')
        self.grp20 = os.path.join(self.home, 'grp20')
        self.otamin18 = os.path.join(self.home, 'otamin18')
        self.otamin18_out = os.path.join(self.otamin18, 'out')
        self.otamin18_ctl = os.path.join(self.otamin18, 'ctl')
        # ====================================================================

    def test_versions(self):
        """
        Test des fonctions de version GRP/OTAMIN
        """
        # ====================================================================
        valid = ['GRPv2016', 'GRPv2018', 'GRPv2020']
        unvalid = ['OTAMINv2016', 'OTAMINv2018', 'OTAMINv2020', 'GRPv2022']
        for v in valid:
            try:
                check_version_grp(v)
            except ValueError:
                self.fail("Erreur dans le contrôle de la version GRP")
        for u in unvalid:
            with self.assertRaises(ValueError):
                check_version_grp(u)
        # ====================================================================
        valid = ['OTAMINv2016', 'OTAMINv2018']
        unvalid = ['GRPv2016', 'GRPv2018', 'GRPv2020', 'OTAMINv2020']
        for v in valid:
            try:
                check_version_otamin(v)
            except ValueError:
                self.fail("Erreur dans le contrôle de la version GRP")
        for u in unvalid:
            with self.assertRaises(ValueError):
                check_version_otamin(u)
        # ====================================================================

    def test_dates(self):
        """
        Test des fonctions de dates
        """
        # ====================================================================
        valid = {
            '20081101': dt(2008, 11, 1),
            '2008110100': dt(2008, 11, 1),
            '2021082614': dt(2021, 8, 26, 14),
            '202108261430': dt(2021, 8, 26, 14, 30),
        }
        for k, v in valid.items():
            self.assertEqual(v, date_parser(k))
        # ====================================================================

    def test_bvfile(self):
        """
        Test des fonctions de lecture des fichiers BASSIN.DAT
        """
        # ====================================================================
        filename = os.path.join(
            self.grp16, 'BD_Bassins', 'K1773010', 'BASSIN.DAT')
        self.assertEqual(get_grp_area(filename), 1465.0)
        # ====================================================================
        filename = os.path.join(
            self.grp18, 'BD_Modeles', 'RH10585x', '00J01H00M', 'BASSIN.DAT')
        self.assertEqual(get_grp_area(filename), 31.0)
        # ====================================================================
        filename = os.path.join(
            self.grp20, 'BD_Modeles', 'RH10585x', '00J01H00M', 'BASSIN.DAT')
        self.assertEqual(get_grp_area(filename), 31.0)
        # ====================================================================

    def test_idsim(self):
        """
        Test de la codification des scénarios
        """
        # ====================================================================
        valid = {'2001': '1001', '2010': '1010'}
        for k, v in valid.items():
            self.assertEqual(v, set_idsim(k))
        # ====================================================================
        valid = {('2001', -1000): '1001', ('2010', '1000'): '3010'}
        for k, v in valid.items():
            self.assertEqual(v, set_idsim(*k))
        # ====================================================================

    def test_clean_otamin_tstep(self):
        """
        Test clean_otamin_tstep
        """
        valid = {
            '00J01H00M': '01H',
            '01J00H00M': '01J',
            '00J00H01M': '01M',
            '01J12H00M': '01J12H',
            None: None,
        }
        for k, v in valid.items():
            self.assertEqual(clean_otamin_tstep(k), v)

    def test_convert_otamin_tstep(self):
        """
        Test convert_otamin_tstep
        """
        valid = {
            '00J01H00M': td(hours=1),
            '01J00H00M': td(days=1),
            '00J00H01M': td(seconds=60),
            '01J12H00M': td(days=1, hours=12),
            None: None,
        }
        for k, v in valid.items():
            self.assertEqual(convert_otamin_tstep(k), v)

    def test_get_grp_scenario(self):
        """
        Test get_grp_scenario
        """
        valid = {
            'GRP_D_Prev_2001.txt': '2001',
            'GRP_D_Simu_2003.txt': '2003',
            'GRP_D_Prev_P0.txt': 'P0',
            'GRP_D_Prev_PP.txt': 'PP',
            'GRP_Prev_P0.txt': 'P0'
        }
        for k, v in valid.items():
            self.assertEqual(get_grp_scenario(k), v)

    def test_set_grp_files(self):
        """
        Test set_grp_files
        """
        # ===================================================================
        in_folder = 'GRP2prv/grp16/Sorties'
        mode = 'TD'
        scens = ['2001', '2002', '2003', '2004', '2005', '2006', '2007',
                 '2008', '2009', '2010']
        fb, fo, fsi, fp, fs = set_grp_files(in_folder=in_folder, mode=mode)
        self.assertEqual(fb, 'BASSIN.DAT')
        self.assertEqual(fo, 'GRP_D_Obs.txt')
        self.assertEqual(fsi, 'PQE_1A_D.DAT')
        self.assertListEqual(
            fp, ['GRP_D_Prev_{}.txt'.format(s) for s in scens])
        self.assertListEqual(
            fs, ['GRP_D_Simu_{}.txt'.format(s) for s in scens])
        # ===================================================================
        in_folder = 'GRP2prv/grp18/Sorties'
        mode = 'TD'
        scens = ['2001', '2002', '2003']
        fb, fo, fsi, fp, fs = set_grp_files(in_folder=in_folder, mode=mode)
        self.assertEqual(fb, 'BASSIN.DAT')
        self.assertEqual(fo, 'GRP_D_Obs.txt')
        self.assertEqual(fsi, 'PQE_1A_D.DAT')
        self.assertListEqual(
            fp, ['GRP_D_Prev_{}.txt'.format(s) for s in scens])
        self.assertListEqual(
            fs, ['GRP_D_Simu_{}.txt'.format(s) for s in scens])
        # ===================================================================
        in_folder = 'GRP2prv/grp20/Sorties'
        mode = 'TD'
        scens = ['2001', '2002', '2003']
        fb, fo, fsi, fp, fs = set_grp_files(in_folder=in_folder, mode=mode)
        self.assertEqual(fb, 'BASSIN.DAT')
        self.assertEqual(fo, 'GRP_D_Obs.txt')
        self.assertEqual(fsi, 'PQE_1A_D.DAT')
        self.assertListEqual(
            fp, ['GRP_D_Prev_{}.txt'.format(s) for s in scens])
        self.assertListEqual(
            fs, ['GRP_D_Simu_{}.txt'.format(s) for s in scens])
        # ===================================================================
        in_folder = 'GRP2prv/grp20/Sorties_EXTRA'
        mode = 'TD'
        scens = ['2000', 'P0', 'PP']
        fb, fo, fsi, fp, fs = set_grp_files(in_folder=in_folder, mode=mode)
        self.assertEqual(fb, 'BASSIN.DAT')
        self.assertEqual(fo, 'GRP_D_Obs.txt')
        self.assertEqual(fsi, 'PQE_1A_D.DAT')
        self.assertListEqual(
            fp, ['GRP_D_Prev_{}.txt'.format(s) for s in scens])
        self.assertListEqual(fs, [])
        # ===================================================================
        in_folder = 'GRP2prv/grp20/Sorties_EXTRA'
        mode = 'TR'
        scens = ['2007', 'P0']
        fb, fo, fsi, fp, fs = set_grp_files(in_folder=in_folder, mode=mode)
        self.assertEqual(fb, 'BASSIN.DAT')
        self.assertEqual(fo, 'GRP_Obs.txt')
        self.assertEqual(fsi, 'PQE_1A.DAT')
        self.assertListEqual(
            fp, ['GRP_Prev_{}.txt'.format(s) for s in scens])
        self.assertListEqual(fs, [])
        # ===================================================================

    def test_set_grp_scenarios(self):
        """
        Test set_grp_scenarios
        """
        # ===================================================================
        # UNIQUEMENT A PARTIR DES FICHIERS
        # -------------------------------------------------------------------
        args = {
            'scenarios': None,
            'scen_P0': None,
            'filenames': ['GRP_D_Prev_2001.txt', 'GRP_D_Prev_2002.txt',
                          'GRP_D_Prev_2003.txt', 'GRP_D_Prev_2004.txt',
                          'GRP_D_Prev_2005.txt', 'GRP_D_Prev_2006.txt',
                          'GRP_D_Prev_2007.txt', 'GRP_D_Prev_2008.txt',
                          'GRP_D_Prev_2009.txt', 'GRP_D_Prev_2010.txt'],
            'filenames_sim': ['GRP_D_Simu_2001.txt', 'GRP_D_Simu_2002.txt',
                              'GRP_D_Simu_2003.txt', 'GRP_D_Simu_2004.txt',
                              'GRP_D_Simu_2005.txt', 'GRP_D_Simu_2006.txt',
                              'GRP_D_Simu_2007.txt', 'GRP_D_Simu_2008.txt',
                              'GRP_D_Simu_2009.txt', 'GRP_D_Simu_2010.txt'],
        }
        valid = {
            'scenarios': ['2001', '2002', '2003', '2004', '2005', '2006',
                          '2007', '2008', '2009', '2010'],
            'scen_P0': '2001',
            'filenames': ['GRP_D_Prev_2001.txt', 'GRP_D_Prev_2002.txt',
                          'GRP_D_Prev_2003.txt', 'GRP_D_Prev_2004.txt',
                          'GRP_D_Prev_2005.txt', 'GRP_D_Prev_2006.txt',
                          'GRP_D_Prev_2007.txt', 'GRP_D_Prev_2008.txt',
                          'GRP_D_Prev_2009.txt', 'GRP_D_Prev_2010.txt'],
            'filenames_sim': ['GRP_D_Simu_2001.txt', 'GRP_D_Simu_2002.txt',
                              'GRP_D_Simu_2003.txt', 'GRP_D_Simu_2004.txt',
                              'GRP_D_Simu_2005.txt', 'GRP_D_Simu_2006.txt',
                              'GRP_D_Simu_2007.txt', 'GRP_D_Simu_2008.txt',
                              'GRP_D_Simu_2009.txt', 'GRP_D_Simu_2010.txt'],
        }
        content = set_grp_scenarios(**args)
        self.assertEqual(content[0], valid['scenarios'])
        self.assertEqual(content[1], valid['scen_P0'])
        self.assertEqual(content[2], valid['filenames'])
        self.assertEqual(content[3], valid['filenames_sim'])
        # ===================================================================
        # FICHIERS INCOMPLETS (2003) AVEC SELECTION SANS P0 (2007)
        # -------------------------------------------------------------------
        args = {
            'scenarios': ['2001', '2003'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_2001.txt', 'GRP_D_Prev_2007.txt'],
            'filenames_sim': ['GRP_D_Simu_2001.txt', 'GRP_D_Simu_2007.txt'],
        }
        valid = {
            'scenarios': ['2007', '2001'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_2007.txt', 'GRP_D_Prev_2001.txt'],
            'filenames_sim': ['GRP_D_Simu_2007.txt', 'GRP_D_Simu_2001.txt'],
        }
        content = set_grp_scenarios(**args)
        self.assertEqual(content[0], valid['scenarios'])
        self.assertEqual(content[1], valid['scen_P0'])
        self.assertEqual(content[2], valid['filenames'])
        self.assertEqual(content[3], valid['filenames_sim'])
        # ===================================================================
        # FICHIERS INCOMPLETS (2003) AVEC SELECTION AVEC P0 (2007)
        # -------------------------------------------------------------------
        args = {
            'scenarios': ['2001', '2003', '2007'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_2001.txt', 'GRP_D_Prev_2007.txt'],
            'filenames_sim': ['GRP_D_Simu_2001.txt', 'GRP_D_Simu_2007.txt'],
        }
        valid = {
            'scenarios': ['2007', '2001'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_2007.txt', 'GRP_D_Prev_2001.txt'],
            'filenames_sim': ['GRP_D_Simu_2007.txt', 'GRP_D_Simu_2001.txt'],
        }
        content = set_grp_scenarios(**args)
        self.assertEqual(content[0], valid['scenarios'])
        self.assertEqual(content[1], valid['scen_P0'])
        self.assertEqual(content[2], valid['filenames'])
        self.assertEqual(content[3], valid['filenames_sim'])
        # ===================================================================
        # FICHIERS AVEC SELECTION SANS P0 (2007)
        # -------------------------------------------------------------------
        args = {
            'scenarios': ['2001', '2003'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_2001.txt', 'GRP_D_Prev_2002.txt',
                          'GRP_D_Prev_2003.txt', 'GRP_D_Prev_2004.txt',
                          'GRP_D_Prev_2005.txt', 'GRP_D_Prev_2006.txt',
                          'GRP_D_Prev_2007.txt', 'GRP_D_Prev_2008.txt',
                          'GRP_D_Prev_2009.txt', 'GRP_D_Prev_2010.txt'],
            'filenames_sim': ['GRP_D_Simu_2001.txt', 'GRP_D_Simu_2002.txt',
                              'GRP_D_Simu_2003.txt', 'GRP_D_Simu_2004.txt',
                              'GRP_D_Simu_2005.txt', 'GRP_D_Simu_2006.txt',
                              'GRP_D_Simu_2007.txt', 'GRP_D_Simu_2008.txt',
                              'GRP_D_Simu_2009.txt', 'GRP_D_Simu_2010.txt'],
        }
        valid = {
            'scenarios': ['2007', '2001', '2003'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_2007.txt', 'GRP_D_Prev_2001.txt',
                          'GRP_D_Prev_2003.txt'],
            'filenames_sim': ['GRP_D_Simu_2007.txt', 'GRP_D_Simu_2001.txt',
                              'GRP_D_Simu_2003.txt'],
        }
        content = set_grp_scenarios(**args)
        self.assertEqual(content[0], valid['scenarios'])
        self.assertEqual(content[1], valid['scen_P0'])
        self.assertEqual(content[2], valid['filenames'])
        self.assertEqual(content[3], valid['filenames_sim'])
        # ===================================================================
        # FICHIERS AVEC SELECTION AVEC P0 (2007)
        # -------------------------------------------------------------------
        args = {
            'scenarios': ['2001', '2003', '2007'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_2001.txt', 'GRP_D_Prev_2002.txt',
                          'GRP_D_Prev_2003.txt', 'GRP_D_Prev_2004.txt',
                          'GRP_D_Prev_2005.txt', 'GRP_D_Prev_2006.txt',
                          'GRP_D_Prev_2007.txt', 'GRP_D_Prev_2008.txt',
                          'GRP_D_Prev_2009.txt', 'GRP_D_Prev_2010.txt'],
            'filenames_sim': ['GRP_D_Simu_2001.txt', 'GRP_D_Simu_2002.txt',
                              'GRP_D_Simu_2003.txt', 'GRP_D_Simu_2004.txt',
                              'GRP_D_Simu_2005.txt', 'GRP_D_Simu_2006.txt',
                              'GRP_D_Simu_2007.txt', 'GRP_D_Simu_2008.txt',
                              'GRP_D_Simu_2009.txt', 'GRP_D_Simu_2010.txt'],
        }
        valid = {
            'scenarios': ['2007', '2001', '2003'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_2007.txt', 'GRP_D_Prev_2001.txt',
                          'GRP_D_Prev_2003.txt'],
            'filenames_sim': ['GRP_D_Simu_2007.txt', 'GRP_D_Simu_2001.txt',
                              'GRP_D_Simu_2003.txt'],
        }
        content = set_grp_scenarios(**args)
        self.assertEqual(content[0], valid['scenarios'])
        self.assertEqual(content[1], valid['scen_P0'])
        self.assertEqual(content[2], valid['filenames'])
        self.assertEqual(content[3], valid['filenames_sim'])
        # ===================================================================
        # SANS SCENARIOS - SORTIES EXTRA
        # -------------------------------------------------------------------
        args = {
            'scenarios': None,
            'scen_P0': None,
            'filenames': ['GRP_D_Prev_2000.txt', 'GRP_D_Prev_P0.txt',
                          'GRP_D_Prev_PP.txt', ],
            'filenames_sim': [],
        }
        valid = {
            'scenarios': ['2000', 'P0', 'PP'],
            'scen_P0': '2000',
            'filenames': ['GRP_D_Prev_2000.txt', 'GRP_D_Prev_P0.txt',
                          'GRP_D_Prev_PP.txt'],
            'filenames_sim': [],
        }
        content = set_grp_scenarios(**args)
        self.assertEqual(content[0], valid['scenarios'])
        self.assertEqual(content[1], valid['scen_P0'])
        self.assertEqual(content[2], valid['filenames'])
        self.assertEqual(content[3], valid['filenames_sim'])
        # ===================================================================
        # SANS SCENARIOS - SORTIES EXTRA - AVEC SCEN P0
        # -------------------------------------------------------------------
        args = {
            'scenarios': None,
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_2000.txt', 'GRP_D_Prev_P0.txt',
                          'GRP_D_Prev_PP.txt', ],
            'filenames_sim': [],
        }
        valid = {
            'scenarios': ['2007', '2000', 'PP'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_P0.txt', 'GRP_D_Prev_2000.txt',
                          'GRP_D_Prev_PP.txt'],
            'filenames_sim': [],
        }
        content = set_grp_scenarios(**args)
        self.assertEqual(content[0], valid['scenarios'])
        self.assertEqual(content[1], valid['scen_P0'])
        self.assertEqual(content[2], valid['filenames'])
        self.assertEqual(content[3], valid['filenames_sim'])
        # ===================================================================
        # SANS SCENARIOS - SORTIES EXTRA - AVEC SCEN P0 et PP
        # -------------------------------------------------------------------
        args = {
            'scenarios': None,
            'scen_P0': '2007',
            'scen_PP': '0000',
            'filenames': ['GRP_D_Prev_2000.txt', 'GRP_D_Prev_P0.txt',
                          'GRP_D_Prev_PP.txt', ],
            'filenames_sim': [],
        }
        valid = {
            'scenarios': ['2007', '2000', '0000'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_P0.txt', 'GRP_D_Prev_2000.txt',
                          'GRP_D_Prev_PP.txt'],
            'filenames_sim': [],
        }
        content = set_grp_scenarios(**args)
        self.assertEqual(content[0], valid['scenarios'])
        self.assertEqual(content[1], valid['scen_P0'])
        self.assertEqual(content[2], valid['filenames'])
        self.assertEqual(content[3], valid['filenames_sim'])
        # ===================================================================
        # AVEC SCENARIOS - SORTIES EXTRA - AVEC SCEN P0 et PP
        # -------------------------------------------------------------------
        args = {
            'scenarios': ['0000', '2000', '2007'],
            'scen_P0': '2007',
            'scen_PP': '0000',
            'filenames': ['GRP_D_Prev_2000.txt', 'GRP_D_Prev_P0.txt',
                          'GRP_D_Prev_PP.txt', ],
            'filenames_sim': [],
        }
        valid = {
            'scenarios': ['2007', '2000', '0000'],
            'scen_P0': '2007',
            'filenames': ['GRP_D_Prev_P0.txt', 'GRP_D_Prev_2000.txt',
                          'GRP_D_Prev_PP.txt'],
            'filenames_sim': [],
        }
        content = set_grp_scenarios(**args)
        self.assertEqual(content[0], valid['scenarios'])
        self.assertEqual(content[1], valid['scen_P0'])
        self.assertEqual(content[2], valid['filenames'])
        self.assertEqual(content[3], valid['filenames_sim'])
        # ===================================================================

    def test_set_grp_submodel(self):
        """
        CAS DE MODELE DE PREVISION - GRP v2018 (et +)
        """
        valid = {
            ('69gGRPd000', '00J01H00M'): '69gGRPd01H',
            ('69gGRPd000', '01J00H00M'): '69gGRPd01J',
        }
        for k, v in valid.items():
            self.assertEqual(set_grp_submodel(*k), v)

    def test_set_grp_simmodel(self):
        """
        CAS DE MODELE DE SIMULATION - GRP v2018 (et +)
        """
        valid = {
            ('69gGRPdB', '00J01H00M'): '69gGRPdB1H',
            ('69gGRPdB', '01J00H00M'): '69gGRPdB1J',
        }
        for k, v in valid.items():
            self.assertEqual(set_grp_simmodel(*k), v)


class TestGRP2prv_py3_g16o18(unittest.TestCase):
    """
    GRP-v2016 -> OTAMIN-v2018 class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # ====================================================================
        self.home = 'GRP2prv_py3'
        self.grp16 = os.path.join(self.home, 'grp16')
        self.otamin18 = os.path.join(self.home, 'otamin18')
        # ====================================================================
        self.in_folder = os.path.join(self.grp16, 'Sorties')
        # ====================================================================
        test_folder = os.path.join(
            self.otamin18,
            'grp16_{}'.format(self.id().split('.')[-1].replace('test_', ''))
        )
        self.otamin_folder = os.path.join(test_folder, 'out', 'otamin')
        self.out_folder = os.path.join(test_folder, 'out', 'propag')
        self.otamin_ctl = os.path.join(test_folder, 'ctl', 'otamin')
        self.out_ctl = os.path.join(test_folder, 'ctl', 'propag')
        for p in [self.otamin_folder, self.out_folder,
                  self.otamin_ctl, self.out_ctl]:
            if not os.path.exists(p):
                os.makedirs(p)
        for p in [self.otamin_folder, self.out_folder]:
            remove_prv_files(p)
        # ====================================================================

    def test_defaut(self):
        """
        Test selon la configuration par défaut
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '45gGRPd000',
            'otamin_folder': self.otamin_folder,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_scen0(self):
        """
        Test selon la configuration avec scenP0
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '45gGRPd000',
            'otamin_folder': self.otamin_folder,
            'scen_P0': '2007',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_scen(self):
        """
        Test selon la configuration avec liste des scénarios
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '45gGRPd000',
            'otamin_folder': self.otamin_folder,
            'scenarios': ['2001', '2003'],
            'scen_P0': '2007',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_extmod(self):
        """
        Test selon la configuration avec nommage des fichiers par 'code POM'
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '45gGRPd000',
            'otamin_folder': self.otamin_folder,
            'scenarios': ['2001', '2003'],
            'scen_P0': '2007',
            'suffix': '45gGRPd000',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_rrta(self):
        """
        Test selon la configuration avec export RR et TA
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '45gGRPd000',
            'otamin_folder': self.otamin_folder,
            'scenarios': ['2001', '2003'],
            'scen_P0': '2007',
            'export_rr_ta': True,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_unique(self):
        """
        Test selon la configuration avec unicité des scénarios
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '45gGRPd000',
            'otamin_folder': self.otamin_folder,
            'scenarios': ['2001', '2003'],
            'scen_P0': '2007',
            'unique': True,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_onebyloc(self):
        """
        Test selon la configuration avec export d'1 fichier par lieu
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '45gGRPd000',
            'otamin_folder': self.otamin_folder,
            'scenarios': ['2001', '2003'],
            'scen_P0': '2007',
            'onebyloc': True,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_simul(self):
        """
        Test selon la configuration avec export d'1 fichier par lieu
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '45gGRPd000',
            'otamin_folder': self.otamin_folder,
            'out_folder': self.out_folder,
            'scenarios': ['2001', '2003'],
            'scen_P0': '2007',
            'export_simul': True,
            'sim_model': '45gGRPdBRU',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        self.assertTrue(
            filecmp.dircmp(self.out_folder, self.out_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.out_folder, self.out_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.out_folder, self.out_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.out_folder, self.out_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        remove_prv_files(self.out_folder)
        # ====================================================================


class TestGRP2prv_py3_g18o18(unittest.TestCase):
    """
    GRP-v2018 -> OTAMIN-v2018 class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.home = 'GRP2prv_py3'
        self.grp18 = os.path.join(self.home, 'grp18')
        self.otamin18 = os.path.join(self.home, 'otamin18')
        # =====================================================================
        self.in_folder = os.path.join(self.grp18, 'Sorties')
        # =====================================================================
        test_folder = os.path.join(
            self.otamin18,
            'grp18_{}'.format(self.id().split('.')[-1].replace('test_', ''))
        )
        self.otamin_folder = os.path.join(test_folder, 'out', 'otamin')
        self.out_folder = os.path.join(test_folder, 'out', 'propag')
        self.otamin_ctl = os.path.join(test_folder, 'ctl', 'otamin')
        self.out_ctl = os.path.join(test_folder, 'ctl', 'propag')
        for p in [self.otamin_folder, self.out_folder,
                  self.otamin_ctl, self.out_ctl]:
            if not os.path.exists(p):
                os.makedirs(p)
        for p in [self.otamin_folder, self.out_folder]:
            remove_prv_files(p)
        # =====================================================================

    def test_defaut(self):
        """
        Test selon la configuration par défaut
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2018',
            'verbose': False
        }
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_scen0(self):
        """
        Test selon la configuration avec scenP0
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2018',
            'scen_P0': '2003',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_scen(self):
        """
        Test selon la configuration avec liste des scénarios
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2018',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_extmod(self):
        """
        Test selon la configuration avec nommage des fichiers par 'code POM'
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2018',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'suffix': 'toto',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_rrta(self):
        """
        Test selon la configuration avec export RR et TA
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2018',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'export_rr_ta': True,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_unique(self):
        """
        Test selon la configuration avec unicité des scénarios
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2018',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'unique': True,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_onebyloc(self):
        """
        Test selon la configuration avec export d'1 fichier par lieu
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2018',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'onebyloc': True,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_simul(self):
        """
        Test selon la configuration avec export d'1 fichier par lieu
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'out_folder': self.out_folder,
            'grp_version': 'GRPv2018',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'export_simul': True,
            'sim_model': '69gGRPdB',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        self.assertTrue(
            filecmp.dircmp(self.out_folder, self.out_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.out_folder, self.out_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.out_folder, self.out_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.out_folder, self.out_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        remove_prv_files(self.out_folder)
        # ====================================================================


class TestGRP2prv_g20o18(unittest.TestCase):
    """
    GRP-v2020 -> OTAMIN-v2018 class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.home = 'GRP2prv_py3'
        self.grp20 = os.path.join(self.home, 'grp20')
        self.otamin18 = os.path.join(self.home, 'otamin18')
        # =====================================================================
        self.in_folder = os.path.join(self.grp20, 'Sorties')
        self.in_folder_E = os.path.join(self.grp20, 'Sorties_EXTRA')
        # =====================================================================
        test_folder = os.path.join(
            self.otamin18,
            'grp20_{}'.format(self.id().split('.')[-1].replace('test_', ''))
        )
        self.otamin_folder = os.path.join(test_folder, 'out', 'otamin')
        self.out_folder = os.path.join(test_folder, 'out', 'propag')
        self.otamin_ctl = os.path.join(test_folder, 'ctl', 'otamin')
        self.out_ctl = os.path.join(test_folder, 'ctl', 'propag')
        for p in [self.otamin_folder, self.out_folder,
                  self.otamin_ctl, self.out_ctl]:
            if not os.path.exists(p):
                os.makedirs(p)
        for p in [self.otamin_folder, self.out_folder]:
            remove_prv_files(p)
        # =====================================================================

    def test_defaut(self):
        """
        Test selon la configuration par défaut
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2020',
            'verbose': False
        }
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_scen0(self):
        """
        Test selon la configuration avec scenP0
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2020',
            'scen_P0': '2003',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_scen(self):
        """
        Test selon la configuration avec liste des scénarios
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2020',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_extmod(self):
        """
        Test selon la configuration avec nommage des fichiers par 'code POM'
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2020',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'suffix': 'toto',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_rrta(self):
        """
        Test selon la configuration avec export RR et TA
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2020',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'export_rr_ta': True,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_unique(self):
        """
        Test selon la configuration avec unicité des scénarios
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2020',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'unique': True,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_onebyloc(self):
        """
        Test selon la configuration avec export d'1 fichier par lieu
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2020',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'onebyloc': True,
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

    def test_simul(self):
        """
        Test selon la configuration avec export d'1 fichier par lieu
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder,
            'mode': 'TD',
            'model': '69gGRPd000',
            'otamin_folder': self.otamin_folder,
            'out_folder': self.out_folder,
            'grp_version': 'GRPv2020',
            'scenarios': ['2001', '2002'],
            'scen_P0': '2003',
            'export_simul': True,
            'sim_model': '69gGRPdB',
            'verbose': False
        }
        # ====================================================================
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        self.assertTrue(
            filecmp.dircmp(self.out_folder, self.out_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.out_folder, self.out_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.out_folder, self.out_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.out_folder, self.out_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        remove_prv_files(self.out_folder)
        # ====================================================================

    def test_extra_TD(self):
        """
        Test selon les fichiers liés à la balise EXTRA de GRP
        """
        # ====================================================================
        args = {
            'in_folder': self.in_folder_E,
            'mode': 'TD',
            'model': '45gGRPd000',
            'otamin_folder': self.otamin_folder,
            'grp_version': 'GRPv2020',
            'scen_P0': '2007',
            'scen_PP': '0000',
            'verbose': False
        }
        run_GRP2prv_py3(**args)
        self.assertTrue(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).same_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).diff_files)
        self.assertFalse(
            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).left_only)
#        self.assertFalse(
#            filecmp.dircmp(self.otamin_folder, self.otamin_ctl).right_only)
        remove_prv_files(self.otamin_folder)
        # ====================================================================

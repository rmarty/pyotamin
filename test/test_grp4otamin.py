#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyOTAMIN>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Test program for grp4otamin in grp4otamin

To run all tests just type:
    python -m unittest test_grp4otamin

To run only a class test:
    python -m unittest test_grp4otamin.Testgrp4otamin

To run only a specific test:
    python -m unittest test_grp4otamin.Testgrp4otamin.test_checks

"""
# Imports
from datetime import timedelta as td
import filecmp
import os
import pandas as pnd
import unittest
import sys
sys.path.insert(0, '..')

# Imports
from grp4otamin import (
    check_compatibility, check_version_grp, check_version_otamin,
    read_grp, split_grpbasename, str2td_grp,
    run_grp4otamin
)


class Testgrp4otamin(unittest.TestCase):
    """
    Otamin_Data class test
    """
    def setUp(self):
        """
        Hook method for setting up the test fixture before exercising it.
        """
        # =====================================================================
        self.grp4otamin = 'grp4otamin'
        # =====================================================================
        self.grp16 = os.path.join('grp4otamin', 'grp16')
        self.leadtimes16 = [0, 1]
        self.leadtimes16.extend(range(3, 15, 3))  # t0 + 3 à t0 +12
        # =====================================================================
        self.grp18 = os.path.join('grp4otamin', 'grp18')
        self.leadtimes18 = range(5)
        # =====================================================================
        self.grp20 = os.path.join('grp4otamin', 'grp20')
        self.leadtimes20 = range(5)
        # =====================================================================
        self.otamin16 = os.path.join('grp4otamin', 'otamin16')
        self.otamin18 = os.path.join('grp4otamin', 'otamin18')
        self.pom_model = '45gGRPd000'
        # =====================================================================

    def test_checks(self):
        """
        Test des fonctions de vérification
        """
        # =====================================================================
        valid = ['GRPv2016', 'GRPv2018', 'GRPv2020']
        unvalid = ['OTAMINv2016', 'OTAMINv2018']
        for v in valid:
            try:
                check_version_grp(v)
            except ValueError:
                self.fail("Erreur dans le contrôle de la version GRP")
        for u in unvalid:
            with self.assertRaises(ValueError):
                check_version_grp(u)
        # =====================================================================
        valid = ['OTAMINv2016', 'OTAMINv2018']
        unvalid = ['GRPv2016', 'GRPv2018', 'GRPv2020']
        for v in valid:
            try:
                check_version_otamin(v)
            except ValueError:
                self.fail("Erreur dans le contrôle de la version GRP")
        for u in unvalid:
            with self.assertRaises(ValueError):
                check_version_otamin(u)
        # =====================================================================
        with self.assertRaises(ValueError):
            check_compatibility('GRPv2018', 'OTAMINv2016', td(minutes=30))
        with self.assertRaises(ValueError):
            check_compatibility('GRPv2020', 'OTAMINv2016', td(minutes=5))
        # =====================================================================

    def test_read(self):
        """
        Test de la lecture du fichier GRP
        """
        # =====================================================================
        filename = os.path.join(
            self.grp16, 'H_K0403010_GRP_SMN_TAN_003_PP_P0P0.TXT')
        version = 'GRPv2016'
        df = read_grp(filename, version)
        self.assertIsInstance(df, pnd.DataFrame)
        self.assertEqual(len(df.index), 72)
        self.assertEqual(len(df.columns), 31)
        # =====================================================================
        filename = filename = os.path.join(
            self.grp18,
            'H_U2345030_GRP_AMN_RNA_HOR_01J00H00M_PDT_00J01H00M_PP_P0P0.TXT')
        version = 'GRPv2018'
        df = read_grp(filename, version, timedelta=td(hours=1))
        self.assertIsInstance(df, pnd.DataFrame)
        self.assertEqual(len(df.index), 72)
        self.assertEqual(len(df.columns), 33)
        # =====================================================================
        filename = filename = os.path.join(
            self.grp20,
            'H_RH10585x_GRP_SMN_TAN_PDT_00J01H00M_HOR_00J03H00M'
            '_Scal_5d00_PP_P0P0.TXT')
        version = 'GRPv2020'
        df = read_grp(filename, version, timedelta=td(hours=1))
        self.assertIsInstance(df, pnd.DataFrame)
        self.assertEqual(len(df.index), 42)
        self.assertEqual(len(df.columns), 33)
        # =====================================================================

    def test_run_grp2016_otamin2016(self):
        """
        Test de l'exécution de grp4otamin - GRP v2016 - OTAMIN v2016
        """
        # =====================================================================
        grp_filename = os.path.join(
            self.grp16, 'H_K0403010_GRP_SMN_TAN_003_PP_P0P0.TXT')
        grp_version = 'GRPv2016'
        otamin_dirname = self.grp4otamin
        otamin_version = 'OTAMINv2016'
        tmp_filenames = run_grp4otamin(
            grp_filename, grp_version, otamin_dirname, otamin_version,
            self.pom_model, self.leadtimes16)
        for f in tmp_filenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.otamin16, os.path.basename(f)),
                f
            ))
            os.remove(f)
        # =====================================================================

    def test_run_grp2016_otamin2018(self):
        """
        Test de l'exécution de grp4otamin - GRP v2016 - OTAMIN v2018
        """
        # =====================================================================
        grp_filename = os.path.join(
            self.grp16, 'H_K0403010_GRP_SMN_TAN_003_PP_P0P0.TXT')
        grp_version = 'GRPv2016'
        otamin_dirname = self.grp4otamin
        otamin_version = 'OTAMINv2018'
        tmp_filenames = run_grp4otamin(
            grp_filename, grp_version, otamin_dirname, otamin_version,
            self.pom_model, self.leadtimes16)
        for f in tmp_filenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.otamin18, os.path.basename(f)),
                f
            ))
            os.remove(f)
        # =====================================================================

    def test_run_grp2018_otamin2016(self):
        """
        Test de l'exécution de grp4otamin - GRP v2018 - OTAMIN v2016
        """
        # =====================================================================
        grp_filename = os.path.join(
            self.grp18,
            'H_U2345030_GRP_AMN_RNA_HOR_01J00H00M_PDT_00J01H00M_PP_P0P0.TXT')
        grp_version = 'GRPv2018'
        otamin_dirname = self.grp4otamin
        otamin_version = 'OTAMINv2016'
        tmp_filenames = run_grp4otamin(
            grp_filename, grp_version, otamin_dirname, otamin_version,
            self.pom_model, self.leadtimes18)
        for f in tmp_filenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.otamin16, os.path.basename(f)),
                f
            ))
            os.remove(f)
        # =====================================================================

    def test_run_grp2018_otamin2018(self):
        """
        Test de l'exécution de grp4otamin - GRP v2018 - OTAMIN v2018
        """
        # =====================================================================
        grp_filename = os.path.join(
            self.grp18,
            'H_U2345030_GRP_AMN_RNA_HOR_01J00H00M_PDT_00J01H00M_PP_P0P0.TXT')
        grp_version = 'GRPv2018'
        otamin_dirname = self.grp4otamin
        otamin_version = 'OTAMINv2018'
        tmp_filenames = run_grp4otamin(
            grp_filename, grp_version, otamin_dirname, otamin_version,
            self.pom_model, self.leadtimes18)
        for f in tmp_filenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.otamin18, os.path.basename(f)),
                f
            ))
            os.remove(f)
        # =====================================================================

    def test_run_grp2020_otamin2018(self):
        """
        Test de l'exécution de grp4otamin - GRP v2020 - OTAMIN v2018
        """
        # =====================================================================
        grp_filename = os.path.join(
            self.grp20,
            'H_RH10585x_GRP_SMN_TAN_PDT_00J01H00M_HOR_00J03H00M'
            '_Scal_5d00_PP_P0P0.TXT')
        grp_version = 'GRPv2020'
        otamin_dirname = self.grp4otamin
        otamin_version = 'OTAMINv2018'
        tmp_filenames = run_grp4otamin(
            grp_filename, grp_version, otamin_dirname, otamin_version,
            self.pom_model, self.leadtimes18)
        for f in tmp_filenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.otamin18, os.path.basename(f)),
                f
            ))
            os.remove(f)
        # =====================================================================

    def test_sacn_encoding(self):
        """Test du fichier SACN reçu en novembre 2024"""
        # =====================================================================
        filename = 'H_I5021020_GRP_SMN_RNA_PDT_00J01H00M_HOR_00J06H00M_'\
            'Scal_1d00_PP_P0P0.TXT'
        version = 'GRPv2020'
        run = split_grpbasename(filename, version)
        self.assertEqual(run.loc, 'I5021020')
        self.assertEqual(run.model, 'GRP')
        self.assertEqual(run.snow, 'SMN')
        self.assertEqual(run.error, 'RNA')
        self.assertEqual(run.leadtime, td(hours=6))
        self.assertEqual(run.timedelta, td(hours=1))
        self.assertEqual(run.rainfall, 'PP')
        self.assertEqual(run.periods, 'P0P0')
        grp_filename = os.path.join(self.grp20, filename)
        grp_version = 'GRPv2020'
        otamin_dirname = self.grp4otamin
        otamin_version = 'OTAMINv2018'
        tmp_filenames = run_grp4otamin(
            grp_filename, grp_version, otamin_dirname, otamin_version,
            self.pom_model, self.leadtimes18, grp_encoding='iso-8859-1')
        for f in tmp_filenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.otamin18, os.path.basename(f)),
                f
            ))
            os.remove(f)
        # =====================================================================

    def test_sacn_scal(self):
        """Test du fichier SACN reçu en novembre 2024"""
        # =====================================================================
        filename = 'H_I50210SC_GRP_SMN_RNA_PDT_00J01H00M_HOR_00J06H00M_'\
            'Scal_1d00_PP_P0P0.TXT'
        grp_filename = os.path.join(self.grp20, filename)
        grp_version = 'GRPv2020'
        otamin_dirname = self.grp4otamin
        otamin_version = 'OTAMINv2018'
        tmp_filenames = run_grp4otamin(
            grp_filename, grp_version, otamin_dirname, otamin_version,
            self.pom_model, self.leadtimes18, grp_encoding='iso-8859-1',
            grp_scal=True)
        for f in tmp_filenames:
            self.assertTrue(filecmp.cmp(
                os.path.join(self.otamin18, os.path.basename(f)),
                f
            ))
            os.remove(f)
        # =====================================================================

    def test_split_grpbasename(self):
        """
        Test de la fonction décomposant le nom du fichier GRP
        """
        # =====================================================================
        filename = 'H_K0403010_GRP_SMN_TAN_003_PP_P0P0.TXT'
        version = 'GRPv2016'
        run = split_grpbasename(filename, version)
        self.assertEqual(run.loc, 'K0403010')
        self.assertEqual(run.model, 'GRP')
        self.assertEqual(run.snow, 'SMN')
        self.assertEqual(run.error, 'TAN')
        self.assertEqual(run.leadtime, td(hours=3))
        self.assertEqual(run.timedelta, td(hours=1))
        self.assertEqual(run.rainfall, 'PP')
        self.assertEqual(run.periods, 'P0P0')
        # =====================================================================
        filename = \
            'H_U2345020_GRP_AMN_RNA_HOR_00J06H00M_PDT_00J01H00M_PP_P0P0.TXT'
        version = 'GRPv2018'
        run = split_grpbasename(filename, version)
        self.assertEqual(run.loc, 'U2345020')
        self.assertEqual(run.model, 'GRP')
        self.assertEqual(run.snow, 'AMN')
        self.assertEqual(run.error, 'RNA')
        self.assertEqual(run.leadtime, td(hours=6))
        self.assertEqual(run.timedelta, td(hours=1))
        self.assertEqual(run.rainfall, 'PP')
        self.assertEqual(run.periods, 'P0P0')
        # =====================================================================
        filename = \
            'H_RH10585x_GRP_SMN_TAN_PDT_00J01H00M_HOR_00J03H00M'\
            '_Scal_5d00_PP_P0P0.TXT'
        version = 'GRPv2020'
        run = split_grpbasename(filename, version)
        self.assertEqual(run.loc, 'RH10585x')
        self.assertEqual(run.model, 'GRP')
        self.assertEqual(run.snow, 'SMN')
        self.assertEqual(run.error, 'TAN')
        self.assertEqual(run.leadtime, td(hours=3))
        self.assertEqual(run.timedelta, td(hours=1))
        self.assertEqual(run.calthreshold, 5.0)
        self.assertEqual(run.rainfall, 'PP')
        self.assertEqual(run.periods, 'P0P0')
        # =====================================================================

    def test_str2td(self):
        """
        Test de la fonction convertissant une chaine en timedelta
        """
        # =====================================================================
        valid = {
            '00J00H05M': td(minutes=5),
            '00J00H30M': td(minutes=30),
            '00J01H00M': td(hours=1),
            '00J06H00M': td(hours=6),
            '01J00H00M': td(days=1),
        }
        for k, v in valid.items():
            self.assertEqual(str2td_grp(k), v)
        # =====================================================================

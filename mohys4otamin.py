#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyOTAMIN>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Script de préparation *en masse* des données produites par MOHYS Calage pour
OTAMIN, outil d'estimation de l'incertitude prédictive "modèle"
"""
import argparse
import collections
from datetime import datetime as dt, timedelta as td
import glob
import matplotlib.pyplot as plt
import numpy as np
import os.path
import pandas as pnd
import sys

# =========================================================================
COLORMAP = plt.cm.rainbow
DATASET_DTFMT = '%d/%m/%Y %H:%M'
# ERROR_DEPTHS = [0]
# ERROR_DEPTHS.append(-1)
# ERROR_DEPTHS.append(24)
ERROR_DEPTHS = [-1, 0, 1]
ERROR_DEPTHS.extend(range(3, 15, 3))  # t0 + 3 à t0 +12
ERROR_DEPTHS.extend(range(18, 78, 6))  # t0 +18 à t0 +72
ERROR_DEPTHS.append(999)
LEADTIMES = [1]
LEADTIMES.extend(range(3, 15, 3))  # t0 + 3 à t0 +12
LEADTIMES.extend(range(18, 78, 6))  # t0 +18 à t0 +72
MODEL_POM = '45hMOHd'
MODELS_POM = ['{0}{1:03d}'.format(MODEL_POM, e) for e in ERROR_DEPTHS]
OTAMIN_DTFMT = '%d-%m-%Y %H:%M'
UNITS = {
    'H': '$m$',
    'Q': '$m^3/s$'
}
FLOAT_FORMATS = {
    'H': {'dframe': '%.3f', 'scores': '{0:.3f}'},
    'Q': {'dframe': '%.1f', 'scores': '{0:.1f}'}
}


# =========================================================================
def argparser():
    """
    """
    parser = argparse.ArgumentParser(description='MOHYS pour OTAMIN')
    parser.add_argument("-d", "--dataset-name",
                        action="store",
                        dest="dataset_name",
                        required=True,
                        help='Nom du jeu de données')
    parser.add_argument("-n", "--variable-name",
                        action="store",
                        dest="varname",
                        required=True,
                        choices=['H', 'Q'],
                        help='Nom de la grandeur')
    parser.add_argument("-b", "--build-forecast",
                        action="store_true",
                        dest="model",
                        default=False,
                        help="Construire prévision à partir échantillon "
                             "sans lacune et selon prise en compte de "
                             "l'erreur à t0")
    parser.add_argument("-f", "--fill-dataset",
                        action="store_true",
                        dest="fill",
                        default=False,
                        help='Remplir les lacunes par np.nan')
    parser.add_argument("-p", "--plot-rmse",
                        action="store_true",
                        dest="plot",
                        default=False,
                        help='Impression graphique du RMSE par échéance')
    parser.add_argument("-c", "--compute-rmse",
                        action="store_true",
                        dest="rmse",
                        default=False,
                        help='Calcul du RMSE par échéance')
#    parser.add_argument("-v", "--verbose",
#                        action="store_true",
#                        dest="verbose",
#                        default=False,
#                        help='Mode verbeux')
    args = parser.parse_args()
#    if args.verbose:
    print(u"Exécution du script "+sys.argv[0])
    print(u"-- Vérification des arguments")
    for k, v in parser.__dict__['_option_string_actions'].items():
        try:
            if k.startswith('--'):
                print("   {0:16s} : {1}".format(k, args.__dict__[v.dest]))
        except KeyError:
            pass
    return args


def compute_rmse(dataframe):
    """
    Calculer le RMSE entre la colonne OBS et la colonne PRV
    en retirant les lignes contenant au moins une valeur non finie
    i.e. parmi [np.inf, -np.inf, np.nan]
    """
    dataframe = dataframe.replace([np.inf, -np.inf], np.nan)
    dataframe = dataframe.dropna()
    obs = dataframe['OBS'].values
    prv = dataframe['PRV'].values
    diff = prv - obs  # the DIFFERENCEs.
    diff2 = diff ** 2  # the SQUAREs of ^
    mdiff2 = diff2.mean()  # the MEAN of ^
    rmse_val = np.sqrt(mdiff2)  # ROOT of ^
    rmse = float(rmse_val)  # CONVERT NDARRAY TO FLOAT
    return rmse


def dataset2dataframe(dataset, fill=True):
    """
    Conversion en pandas.DataFrame
    """
    dates = [x[0] for x in dataset]
    obs = np.array([x[1] for x in dataset], dtype='f')
    sim = np.array([x[2] for x in dataset], dtype='f')
    dataframe = pnd.DataFrame(
        data={'OBS': obs, 'SIM': sim},
        index=dates
    )
    if fill:
        dt_min = min(dataframe.index)
        dt_max = max(dataframe.index)
        dates = [dt_min + x*td(hours=1)
                 for x in range(int((dt_max - dt_min) / td(hours=1)) + 1)]
        dataframe = dataframe.reindex(dates)
    return dataframe


def per2prv(dataframe=None, error_depth=None, leadtimes=range(73)):
    """
    Création du pnd. DataFrame contenant les prévisions
        avec 1 colonne par échéance
    Prévision = Persistance
    """
    if dataframe is None:
        return None
    # Initialisation
    # --------------
    prv_dates = []
    prv_data = collections.OrderedDict()
    for ltime in leadtimes:
        prv_data["OBS{0:03d}".format(ltime)] = []
        prv_data["PRV{0:03d}".format(ltime)] = []
    # Boucle sur les instants de prévision
    # ------------------------------------
    for t0 in dataframe.index:
        # Traitement si OBS dispo ET SIM dispo
        # ------------------------------------
        if not np.isnan(dataframe.loc[t0, "OBS"]) and \
                not np.isnan(dataframe.loc[t0, "SIM"]):
            # Traitement t0
            # -------------
            prv_dates.append(t0)
            # Calcul de la persistance
            # ------------------------------------------------
            persistance = dataframe.loc[t0, "OBS"]
            # Boucle sur les échéances
            # ------------------------
            for ltime in leadtimes:
                # Traitement OBS
                # --------------
                prv_header = "OBS{0:03d}".format(ltime)
                c_ltime = t0 + ltime * td(hours=1)
                try:
                    c_value = dataframe.loc[c_ltime, "OBS"]
                except KeyError:
                    c_value = np.nan
                prv_data[prv_header].append(c_value)
                # Traitement PRV
                # --------------
                prv_header = "PRV{0:03d}".format(ltime)
                prv_data[prv_header].append(persistance)
        else:
            pass
    # Création du pnd.DataFrame
    prv_data = pnd.DataFrame(prv_data, index=prv_dates)
    return prv_data


def prv4otamin(prv_data=None, leadtimes=range(73),
               output_dir=".", quantity="Q",
               station="STATION", model="CODEPOM"):
    """
    Ecriture des fichiers de sortie OTAMIN à partir d'un pnd.DataFrame
    """
    if prv_data is not None:
        # Ouverture des fichiers de sortie
        # --------------------------------
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        output_prefix = "_".join([station, model])
        fout = [open(output_dir + "/" +
                     output_prefix + "_" +
                     "{0:03d}H".format(lt) +
                     ".csv", "w", encoding="utf-8", newline="\n")
                for lt in leadtimes]
        # Entête des fichiers de sortie
        # -----------------------------
        outheader = "Stations;{}\n".format(station)
        outheader += "Grandeurs;{}\n".format(quantity)
        outheader += "Modeles;{}\n".format(model)
        outheader += "# JJ-MM-AAAA HH:MM;OBS;PREV\n"
        for fo in fout:
            fo.write(outheader)
        # Traitement des données contenues dans le pnd.DataFrame
        # ------------------------------------------------------
        for ltime, fo in zip(leadtimes, fout):
            header_obs = "OBS{0:03d}".format(ltime)
            header_prv = "PRV{0:03d}".format(ltime)
            for idx in prv_data.index:
                c_date = idx + ltime * td(hours=1)
                c_date = c_date.strftime('%d-%m-%Y %H:%M')
                c_obs = prv_data.loc[idx, header_obs]
                c_prv = prv_data.loc[idx, header_prv]
                if quantity == "Q":
                    if c_obs == 0:
                        c_obs = 0.0001
                    elif c_obs < 0:
                        c_obs = np.nan
                    if c_prv == 0:
                        c_prv = 0.0001
                    elif c_prv < 0:
                        c_prv = np.nan
                if np.isnan(c_obs):
                    c_obs = "-999.999"  # -9.9000
                else:
                    c_obs = "{0:.4f}".format(c_obs)
                if np.isnan(c_prv):
                    c_prv = "-999.999"  # -9.9000
                else:
                    c_prv = "{0:.4f}".format(c_prv)
                line = ";".join([c_date,
                                 c_obs,
                                 c_prv
                                 ]) + "\n"
                fo.write(line)
            fo.flush()
            os.fsync(fo.fileno())
        # Fermeture des fichiers de sortie
        # --------------------------------
        otamin_filenames = []
        for fo in fout:
            otamin_filenames.append(fo.name)
            fo.close()
        return otamin_filenames
    return None


def read_dataset(filename):
    """
    Lecture du fichier csv à 3 colonnes (date, obs, sim)
    """
    dates = set()
    dataset = []
    with open(filename, 'r', encoding='utf-8', newline='\n') as f:
        f.readline()
        for line in f.readlines():
            info = line.replace('\n', '').split(';')
            date = dt.strptime(info[0], DATASET_DTFMT)
            try:
                obs = float(info[1])
            except ValueError:
                obs = np.nan
            try:
                sim = float(info[2])
            except ValueError:
                sim = np.nan
            if date not in dates:
                dates.add(date)
                dataset.append([date, obs, sim])
    return dataset


def read_otamin(filename):
    """
    Lecture du fichier OTAMIN-CAL avec
    - 4 lignes d'entête
    - 3 colonnes (date, obs, prv)
    """
    dataset = []
    with open(filename, 'r', encoding='utf-8', newline='\n') as f:
        f.readline()
        f.readline()
        f.readline()
        f.readline()
        for line in f.readlines():
            info = line.replace('\n', '').split(';')
            date = dt.strptime(info[0], OTAMIN_DTFMT)
            try:
                obs = float(info[1])
            except ValueError:
                obs = np.nan
            else:
                if info[1] in ['-9.9000', '-999.999']:
                    obs = np.nan
            try:
                prv = float(info[2])
            except ValueError:
                prv = np.nan
            else:
                if info[2] in ['-9.9000', '-999.999']:
                    prv = np.nan
            dataset.append([date, obs, prv])
    dates = [x[0] for x in dataset]
    obs = np.array([x[1] for x in dataset], dtype='f')
    prv = np.array([x[2] for x in dataset], dtype='f')
    dataframe = pnd.DataFrame(
        data={'OBS': obs, 'PRV': prv},
        index=dates
    )
    return dataframe


def sim2prv(dataframe=None, error_depth=None, leadtimes=range(73)):
    """
    Création du pnd. DataFrame contenant les prévisions
        avec 1 colonne par échéance
    Si error_depth n'est pas None:
        application de la correction de l'erreur calculée à t0
        selon décroissance linéaire sur <error_depth> pas de temps
    """
    if dataframe is None:
        return None
    # Initialisation
    # --------------
    prv_dates = []
    prv_data = collections.OrderedDict()
    for ltime in leadtimes:
        prv_data["OBS{0:03d}".format(ltime)] = []
        prv_data["PRV{0:03d}".format(ltime)] = []
    if error_depth is None or error_depth == 0:
        error_ratio = [0]*len(leadtimes)
    else:
        error_ratio = [1. - float(t)/float(error_depth)
                       if t <= error_depth else 0
                       for t in leadtimes]
    # Boucle sur les instants de prévision
    # ------------------------------------
#    debug_index = 0
    for t0 in dataframe.index:
        # Traitement si OBS dispo ET SIM dispo
        # ------------------------------------
        if not np.isnan(dataframe.loc[t0, "OBS"]) and \
                not np.isnan(dataframe.loc[t0, "SIM"]):
            # Traitement t0
            # -------------
            prv_dates.append(t0)
            # Calcul du correctif des erreurs selon l'échéance
            # ------------------------------------------------
            c_error = dataframe.loc[t0, "OBS"] - dataframe.loc[t0, "SIM"]
            c_errors = [c_error * r for r in error_ratio]
#            print(t0, c_error)
#            print(t0, c_errors)
            # Boucle sur les échéances
            # ------------------------
            for k, ltime in enumerate(leadtimes):
                # Traitement OBS
                # --------------
                prv_header = "OBS{0:03d}".format(ltime)
                c_ltime = t0 + ltime * td(hours=1)
                try:
                    c_value = dataframe.loc[c_ltime, "OBS"]
                except KeyError:
                    c_value = np.nan
                prv_data[prv_header].append(c_value)
                # Traitement PRV
                # --------------
                prv_header = "PRV{0:03d}".format(ltime)
                c_ltime = t0 + ltime * td(hours=1)
                try:
                    c_value = dataframe.loc[c_ltime, "SIM"] + c_errors[k]
                except KeyError:
                    c_value = np.nan
                prv_data[prv_header].append(c_value)
        else:
            pass
    # Création du pnd.DataFrame
    prv_data = pnd.DataFrame(prv_data, index=prv_dates)
    return prv_data


def plot_scores(filename, fig_filename='figure.png',
                dpi=150, cmap=plt.cm.rainbow, cmapsize=256,
                title='', xlabel='Echéance [h]', ylabel='Score',
                legend_loc=1, legend_ncol=1, legend_fontsize=8,
                toskip=None
                ):
    """
    """
    if toskip is None:
        toskip = []
    dataset = collections.OrderedDict()
    cmap_rgb = cmap(np.arange(cmapsize))  # _rgbA (a=alpha)
    with open(filename, 'r', encoding='utf-8', newline='\n') as f:
        header = f.readline().replace('\n', '').split(';')
        for h in header:
            dataset.setdefault(h, [])
        for line in f.readlines():
            info = line.replace('\n', '').split(';')
            for k, h in enumerate(header):
                try:
                    if k == 0:
                        value = int(info[k])
                    else:
                        value = float(info[k])
                except ValueError:
                    value = np.nan
                dataset[h].append(value)
    figure = plt.figure(dpi=dpi)
    axes = figure.add_axes((0.10, 0.15, 0.85, 0.75))

    colors = {
        MODEL_POM + '-01': [0.5, 0.5, 0.5],
        MODEL_POM + '000': [0.1, 0.1, 0.1]
    }
    header2 = [h for h in header[1:] if h not in colors]
    colors.update({h: cmap_rgb[int((k/(len(header2)))*cmapsize)]
                   for k, h in enumerate(header2)})
    header2 = header[1:]
    for h in [MODEL_POM + 'd-01', MODEL_POM + '000']:
        if h in header2:
            header2.remove(h)
            header2.append(h)
    xvalues = dataset[header[0]]
    for h in header2:
        if h in toskip:
            continue
        yvalues = dataset[h]
        axes.plot(xvalues, yvalues, label=h,
                  linestyle='-', linewidth=1,
                  marker='',
                  color=colors[h])
    axes.set_xlabel(xlabel)
    axes.set_ylabel(ylabel)
    axes.set_title(title)
    axes.set_xlim(0, 100)
    axes.legend(
        loc=legend_loc,
        ncol=legend_ncol,
        fontsize=legend_fontsize
    )
    figure.savefig(fig_filename, dpi=dpi)
    plt.close(figure)
    return fig_filename


def write_scores(scores, filename, models, float_format='{0:.1f}'):
    """
    """
    with open(filename, 'w', encoding='utf-8', newline='\n') as f:
        f.write('Echeance;')
        f.write(';'.join(models))
        f.write('\n')
        for ltime in scores:
            text = "{}".format(ltime)
            for m in models:
                value = scores[ltime].get(m, None)
                if isinstance(value, float):
                    text += ";"
                    text += float_format.format(value)
                else:
                    text += ";"
            text += '\n'
            f.write(text)
    return filename


# =========================================================================
# =========================================================================
if __name__ == "__main__":
    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------
    print(dt.now().strftime("%d/%m/%Y %H:%M"))
    OPTIONS = argparser()
    # DATASET_NAME = 'K1180010_Digoin'
    # DATASET_VARNAME = 'Q'
    DATASET_NAME = OPTIONS.dataset_name
    DATASET_VARNAME = OPTIONS.varname
    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------
    DATASET_FILENAME = os.path.join(DATASET_NAME, DATASET_NAME + '.txt')
    DATASET_STATION = DATASET_NAME.split('_')[0]
    DATAFRAME_DIRNAME = os.path.join(DATASET_NAME, 'dataframe')
    DATAFRAME_FILENAME = os.path.join(DATAFRAME_DIRNAME, DATASET_NAME + '.txt')
    SCORES_DIRNAME = os.path.join(DATASET_NAME, 'scores')
    SCORES_FILENAME = os.path.join(SCORES_DIRNAME, DATASET_NAME + '.txt')
    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------
    if not os.path.exists(DATAFRAME_DIRNAME):
        os.makedirs(DATAFRAME_DIRNAME)
    for m in MODELS_POM:
        if not os.path.exists(os.path.join(DATASET_NAME, m)):
            os.makedirs(os.path.join(DATASET_NAME, m))
    if not os.path.exists(SCORES_DIRNAME):
        os.makedirs(SCORES_DIRNAME)
    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------
    print("-- Fichier SIMULATION d'origine    : {}".format(DATASET_FILENAME))
    # -------------------------------------------------------------------------
    # -------------------------------------------------------------------------
    if OPTIONS.fill:
        # ---------------------------------------------------------------------
        # PARTIE 1 : REMPLIR L'ECHANTILLON ORIGINALE PAR np.nan
        #
        # DATASET_FILENAME ===> DATAFRAME_FILENAME
        #
        # ---------------------------------------------------------------------
        dataset = read_dataset(DATASET_FILENAME)
        dataframe = dataset2dataframe(dataset)
        dataframe.to_csv(
            DATAFRAME_FILENAME,
            sep=';',
            float_format=FLOAT_FORMATS[DATASET_VARNAME]['dframe'],
            index_label='Date',
            date_format=DATASET_DTFMT
        )
        print("-- Fichier SIMULATION sans coupure : {}"
              "".format(DATAFRAME_FILENAME))

    if OPTIONS.model:
        # ---------------------------------------------------------------------
        # PARTIE 2 : CONTRUIRE PREVISION A PARTIR DE L'ECHANTILLON SANS LACUNE
        #
        # DATAFRAME_FILENAME ===> PREVISIONS DANS FICHIER POUR OTAMIN-CAL
        #
        # ---------------------------------------------------------------------
        dataset = read_dataset(DATAFRAME_FILENAME)
        dataframe = dataset2dataframe(dataset, fill=False)
        for e, m in zip(ERROR_DEPTHS, MODELS_POM):
            if e >= 0:
                print("-- Création des prévisions avec recalage sur {0:3d} "
                      "heures".format(e))
                fcst_dataframe = sim2prv(
                    dataframe=dataframe,
                    error_depth=e,
                    leadtimes=LEADTIMES
                )
            elif e == -1:
                print("-- Création des prévisions par persistance"
                      "".format(e))
                fcst_dataframe = per2prv(
                    dataframe=dataframe,
                    error_depth=e,
                    leadtimes=LEADTIMES
                )
            else:
                continue
            fcst_filenames = prv4otamin(
                prv_data=fcst_dataframe,
                leadtimes=LEADTIMES,
                output_dir=os.path.join(DATASET_NAME, m),
                quantity=DATASET_VARNAME,
                station=DATASET_STATION,
                model=m
            )
            print("   + Impression de {} fichiers OTAMIN-CAL"
                  "".format(len(fcst_filenames)))
#            for f in fcst_filenames:
#                print("   + Fichier OTAMIN-CAL : {}"
#                      "".format(os.path.basename(f)))
#            break

    if OPTIONS.rmse:
        # ---------------------------------------------------------------------
        # PARTIE 3 : CALCUL DU RMSE PAR ECHEANCE
        #
        # FICHIER POUR OTAMIN-CAL ===> FICHIER CSV AVEC LES SCORES
        #
        # ---------------------------------------------------------------------
        scores = collections.OrderedDict()
        for m in MODELS_POM:
            print("-- Calcul du RMSE pour le modèle {}".format(m))
            pattern = "{}_{}*.csv".format(DATASET_STATION, m)
            filenames = glob.glob(os.path.join(DATASET_NAME, m, pattern))
            for f in filenames:
                leadtime = int(float(os.path.basename(f).split('_')[-1][:3]))
                dataframe = read_otamin(f)
                score = compute_rmse(dataframe)
#                print("   + Fichier OTAMIN-CAL : {}"
#                      "".format(os.path.basename(f)))
#                print("     - ECHEANCE   : {0:5d} heures".format(leadtime))
#                print("     - SCORE RMSE : {0:7.1f}".format(score))
                scores.setdefault(leadtime, collections.OrderedDict())
                scores[leadtime].setdefault(m, score)
        #    break
        msg = write_scores(
            scores=scores,
            filename=SCORES_FILENAME,
            models=MODELS_POM,
            float_format=FLOAT_FORMATS[DATASET_VARNAME]['scores']
        )
        print("-- Export des scores RMSE : {}".format(msg))

    if OPTIONS.plot:
        # ---------------------------------------------------------------------
        # PARTIE 4 : CREATION DE LA FIGURE
        #
        # FICHIER CSV AVEC LES SCORES ===> FICHIER PNG
        #
        # ---------------------------------------------------------------------
        fig_filename = os.path.splitext(SCORES_FILENAME)[0] + '_all.png'
        msg = plot_scores(
            filename=SCORES_FILENAME,
            fig_filename=fig_filename,
            title=DATASET_NAME,
            ylabel='RMSE ({})'.format(UNITS[DATASET_VARNAME])
        )
        print("-- Figure des scores RMSE : {}".format(msg))
        toskip_models = [m for m in MODELS_POM if 'd-' in m]
        if toskip_models:
            fig_filename = os.path.splitext(SCORES_FILENAME)[0] + '.png'
            msg = plot_scores(
                filename=SCORES_FILENAME,
                fig_filename=fig_filename,
                title=DATASET_NAME,
                ylabel='RMSE ({})'.format(UNITS[DATASET_VARNAME]),
                toskip=toskip_models
            )
            print("-- Figure des scores RMSE : {}".format(msg))

    # =========================================================================
    print("-- Fin du script : {}".format(sys.argv[0]))
    print(dt.now().strftime("%d/%m/%Y %H:%M"))
    # =========================================================================

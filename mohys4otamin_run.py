#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyOTAMIN>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Script de lancement de mohys4otamin.py
"""
from datetime import datetime as dt
import glob
import os.path
import subprocess
import sys


print(dt.now().strftime("%d/%m/%Y %H:%M"))
HOME = '.'
dirnames = glob.glob(os.path.join(HOME, 'K*'))
for dirname in dirnames:
    dataset_name = os.path.basename(dirname)
    dataset_code = dataset_name.split('_')[0]
    dataset_varname = 'Q'
    if len(dataset_code) == 10:
        dataset_varname = 'H'
    filename = os.path.join(dirname, dataset_name + '.txt')
    if os.path.isdir(dirname) and os.path.exists(filename):
        print('-'*75)
        print(dataset_name, dataset_varname)
        logfilename = os.path.join(dirname, dataset_name + '.log')
        logfile = open(logfilename, 'w')
        processArgs = [
            'python',
            'mohys4otamin.py',
            '-d', dataset_name,
            '-n', dataset_varname,
            '-fbcp'
        ]
#        print(' '.join(processArgs))
        processRun = subprocess.Popen(
            processArgs, universal_newlines=True,
            shell=True, stdout=logfile, stderr=sys.stderr)  # nosec
        processRun.wait()
        logfile.close()
        print(dt.now().strftime("%d/%m/%Y %H:%M"))
print('-'*75)
print(dt.now().strftime("%d/%m/%Y %H:%M"))

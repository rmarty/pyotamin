#!/usr/bin/python3
# -*- coding: utf-8 -*-
########################################################################
#
# This file is part of python module <pyOTAMIN>.
# Copyright (C) 2013-2020  R. Marty
#   (renaud.marty@developpement-durable.gouv.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program (see COPYING.txt).
# If not, see <http://www.gnu.org/licenses/>.
#
########################################################################
"""
Convertir les simulations d'un modèle en fichier d'entrée pour OTAMIN Calage
Documentation: voir doc/html/_sim4otamin.html
"""
# ------------------------------------
# IMPORTS
# ------------------------------------
import glob
import os
import os.path
import sys
from datetime import datetime as dt, timedelta as td
import numpy as np
import pandas as pnd
import collections
# ------------------------------------
# PARAMETRES
# ------------------------------------
INPUT_DIRNAME = "D:\\2-gr4h\\sim"
INPUT_SUFFIX = "_sim.txt"
OUTPUT_DIRNAME = "D:\\2-gr4h\\otamin"
VARNAME = "Q"
ERROR_DEPTH = 999
LEADTIMES = range(73)
# ------------------------------------
# DEPENDANCES
# ------------------------------------


# *****************************************
#               FONCTIONS
# *****************************************
def read_simfile(filename, leadtimes=range(73)):
    """
    Lecture du fichier SIMULATION
    """
    with open(filename, 'r', encoding='utf-8') as f:
        # Entête
        # ------
        f.readline()
        # Donnéées
        # --------
        dates = []
        data_obs = []
        data_sim = []
        for line in f.readlines():
            raw_data = line.replace("\n", "").split(";")
            dates.append(dt.strptime(raw_data[0], "%Y%m%d%H"))
            if raw_data[1] != '':
                data_obs.append(float(raw_data[1]))
            else:
                data_obs.append(np.nan)
            if raw_data[2] != '':
                data_sim.append(float(raw_data[2]))
            else:
                data_sim.append(np.nan)
        # DataFrame
        # ---------
        data_obs = np.array(data_obs, dtype='f')
        data_sim = np.array(data_sim, dtype='f')
        data = pnd.DataFrame({'OBS': data_obs, 'SIM': data_sim}, index=dates)
        # Re-index pour introduire les pas de temps manquants
        # ---------------------------------------------------
        dt_min = min(data.index)
        dt_max = max(data.index) + leadtimes[-1]*td(hours=1)
        dates = [dt_min + x*td(hours=1)
                 for x in range(int((dt_max - dt_min) / td(hours=1)) + 1)]
        data = data.reindex(dates)
        return data


def sim2prv(sim_data=None, error_depth=None, leadtimes=range(73)):
    """
    Création du pnd. DataFrame contenant les prévisions
        avec 1 colonne par échéance
    Si error_depth n'est pas None:
        application de la correction de l'erreur calculée à t0
        selon décroissance linéaire sur <error_depth> pas de temps
    """
    if sim_data is not None:
        # Initialisation
        # --------------
        prv_dates = []
        prv_data = collections.OrderedDict()
        for ltime in leadtimes:
            prv_data["OBS{0:03d}".format(ltime)] = []
            prv_data["PRV{0:03d}".format(ltime)] = []
        if error_depth is None:
            error_ratio = [0]*len(leadtimes)
        else:
            error_ratio = [1. - float(t)/float(error_depth)
                           if t <= error_depth else 0
                           for t in leadtimes]
        # Boucle sur les instants de prévision
        # ------------------------------------
        for t0 in sim_data.index:
            # Traitement si OBS dispo ET SIM dispo
            # ------------------------------------
            if not np.isnan(sim_data.loc[t0, "OBS"]) and \
                    not np.isnan(sim_data.loc[t0, "SIM"]):
                # Traitement t0
                # -------------
                prv_dates.append(t0)
                # Calcul du correctif des erreurs selon l'échéance
                # ------------------------------------------------
                c_error = sim_data.loc[t0, "OBS"] - sim_data.loc[t0, "SIM"]
                c_errors = [c_error * r for r in error_ratio]
                # Boucle sur les échéances
                # ------------------------
                for k, ltime in enumerate(leadtimes):
                    # Traitement OBS
                    # --------------
                    prv_header = "OBS{0:03d}".format(ltime)
                    c_ltime = t0 + ltime * td(hours=1)
                    c_value = sim_data.loc[c_ltime, "OBS"]
                    prv_data[prv_header].append(c_value)
                    # Traitement PRV
                    # --------------
                    prv_header = "PRV{0:03d}".format(ltime)
                    c_ltime = t0 + ltime * td(hours=1)
                    c_value = sim_data.loc[c_ltime, "SIM"] + c_errors[k]
                    prv_data[prv_header].append(c_value)
            else:
                pass
        # Création du pnd.DataFrame
        prv_data = pnd.DataFrame(prv_data, index=prv_dates)
        return prv_data
    return None


def prv4otamin(prv_data=None, leadtimes=range(73),
               output_dir=".", quantity="Q",
               station="STATION", model="CODEPOM"):
    """
    Ecriture des fichiers de sortie OTAMIN à partir d'un pnd.DataFrame
    """
    if prv_data is not None:
        # Ouverture des fichiers de sortie
        # --------------------------------
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        output_prefix = "_".join([station, model])
        fout = [open(output_dir + "/" +
                     output_prefix + "_" +
                     "{0:03d}H".format(lt) +
                     ".csv", "w", encoding="utf-8", newline="\n")
                for lt in leadtimes]
        # Entête des fichiers de sortie
        # -----------------------------
        outheader = "Stations;{}\n".format(station)
        outheader += "Grandeurs;{}\n".format(quantity)
        outheader += "Modeles;{}\n".format(model)
        outheader += "# JJ-MM-AAAA HH:MM;OBS;PREV\n"
        for file in fout:
            file.write(outheader)
        # Traitement des données contenues dans le pnd.DataFrame
        # ------------------------------------------------------
        for ltime, file in enumerate(fout):
            header_obs = "OBS{0:03d}".format(ltime)
            header_prv = "PRV{0:03d}".format(ltime)
            for idx in prv_data.index:
                c_date = idx + ltime * td(hours=1)
                c_date = c_date.strftime('%d-%m-%Y %H:%M')
                c_obs = prv_data.loc[idx, header_obs]
                c_prv = prv_data.loc[idx, header_prv]
                if quantity == "Q":
                    if c_obs <= 0:
                        c_obs = 0.0001
                    if c_prv <= 0:
                        c_prv = 0.0001
                if np.isnan(c_obs):
                    c_obs = "-9.9000"
                else:
                    c_obs = "{0:.4f}".format(c_obs)
                if np.isnan(c_prv):
                    c_prv = "-9.9000"
                else:
                    c_prv = "{0:.4f}".format(c_prv)
                line = ";".join([c_date,
                                 c_obs,
                                 c_prv
                                 ]) + "\n"
                file.write(line)
            file.flush()
            os.fsync(file.fileno())
        # Fermeture des fichiers de sortie
        # --------------------------------
        otamin_filenames = []
        for file in fout:
            otamin_filenames.append(file.name)
            file.close()
        return otamin_filenames
    return None


# *****************************************
#               SCRIPT
# *****************************************
# ------------------------------------
# BOUCLE SUR LES FICHIERS A TRAITER
# ------------------------------------
for file in glob.glob(INPUT_DIRNAME + "/*" + INPUT_SUFFIX):
    # Nom du fichier SIMULATION
    # -------------------------
    filename = os.path.basename(file)
    station, model = filename.split("_")[0:2]
    print(dt.now().strftime("%d/%m/%Y %H:%M"))
    print("-- Fichier SIMULATION : {}".format(filename))
    print("   - Station : {}".format(station))
    print("   - Modèle : {}".format(model))
    # Lecture du fichier SIMULATION
    # -----------------------------
    print("   - Lecture des données de simulation")
    sim_data = read_simfile(file, leadtimes=LEADTIMES)
    if sim_data is not None:
        if ERROR_DEPTH is None:
            print("   - Conversion en prévision "
                  "SANS prise en compte de l'erreur à t0")
        else:
            print("   - Conversion en prévision "
                  "AVEC prise en compte de l'erreur à t0 "
                  "jusqu'à l'échéance +{} h".format(ERROR_DEPTH))
        prv_data = sim2prv(sim_data=sim_data,
                           error_depth=ERROR_DEPTH,
                           leadtimes=LEADTIMES)
        print(dt.now().strftime("%d/%m/%Y %H:%M"))
        if prv_data is not None:
            print("   - Conversion au format OTAMIN")
            msg_otamin = prv4otamin(prv_data=prv_data,
                                    leadtimes=LEADTIMES,
                                    output_dir=OUTPUT_DIRNAME,
                                    station=station,
                                    model=model,
                                    quantity=VARNAME)
            print(dt.now().strftime("%d/%m/%Y %H:%M"))
            if isinstance(msg_otamin, list):
                for msg in msg_otamin:
                    print("   - Ecriture du fichier OTAMIN : {}".format(msg))
            else:
                print(msg_otamin)
print("-- Fin du script : {}".format(sys.argv[0]))
print(dt.now().strftime("%d/%m/%Y %H:%M"))

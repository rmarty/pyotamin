
:orphan:


mohys4otamin.py
===============


.. role:: linkmeth

.. role:: linkclass

.. index:: mohys4otamin.py


Synopsis
--------

``mohys4otamin.py``

Description
-----------

Script de préparation *en masse* des données produites par MOHYS Calage pour OTAMIN, outil d'estimation de l'incertitude prédictive "modèle"

.. warning::
   Ce script ne fonctionne que dans un environnement Python 3

Notes de version
----------------

Version : 2019.04.09

Options
-------

``-d`` DATASET_NAME, ``--dataset-name`` DATASET_NAME
   Nom du jeu de données

``-n``  {H, Q}, ``--variable-name``  {H, Q}
   Nom de la grandeur

``-b``, ``--build-forecast``
   Construire prévision à partir échantillon sans lacune et selon prise en compte de l'erreur à t0

``-f`` , ``--fill-dataset``
   Remplir les lacunes par np.nan
   
.. note::
    
   np : bibliothèque numpy

``-c``, ``--compute-rmse``
   Calculer le RMSE par échéance

``-p``, ``--plot-rmse``
   Impression graphique du RMSE par échéance

``-h``, ``--help``
   show this help message and exit


Paramètres
----------

:linkclass:`COLORMAP` : Nom de la palette de couleur

:linkclass:`DATASET_DTFMT` : Format de la date dans les fichiers d'entrée

:linkclass:`ERROR_DEPTHS` : Liste des temps de recalage

- -1 : Persistance
- 0 : pas de recalage
- N : recalage avec prise en compte de l'erreur à t0. La correction appliquée décroit linéairement avec l'horizon de prévision jusqu'à l'horizon N
- 999 : équivalent à un recalage global

:linkclass:`LEADTIMES` : Liste des horizons de prévision considérés lors de la construction des prévisions

:linkclass:`MODEL_POM` : Préfixe du code modèle POM

:linkclass:`OTAMIN_DTFMT` : Format de date des fichiers OTAMIN

:linkclass:`UNITS` : Dictionnaire des unités des grandeurs (H, Q)

:linkclass:`FLOAT_FORMATS` : Dictionnaire des formats des flottants pour l'écriture du dataframe rempli (option *-f*) et des scores (option *-c*)

Structure des données d'entrée
------------------------------

Les fichiers MOHYS pour OTAMIN sont de type *csv*. Ils doivent respecter l'arborescence suivante:

    DATASET_NAME / DATASET_NAME.txt
    
avec

:linkclass:`DATASET_NAME` : Identifiant de la série : **CODESTATION**\_\ **NOMSTATION**
    
La première ligne doit contenir les noms des colonnes de données:

1. 'date'
2. 'obs'
3. 'sim'

Les lignes suivantes contiennt les données d'observation et de prévision valides à la date écrite en début de ligne. Si une des données n'existe pas, la "cellule" correspondante doit être vide.

**Exemple**:

    date;observé;simulé
    02/11/2008 12:00;156;114.7

Structure des données de sortie
-------------------------------

Remplir les lacunes par np.nan (option *-f*)
++++++++++++++++++++++++++++++++++++++++++++

Le fichier des données d'observations et simulations sans lacune est placé dans:

    DATASET_NAME / dataframe / DATASET_NAME.txt

Construire prévision à partir échantillon sans lacune et selon prise en compte de l'erreur à t0 (option *-b*)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Pour chaque temps de recalage, pour chaque horizon de prévision, un fichier *csv* au format **prv OTAMIN** est écrit:

    DATASET_NAME / 45hMOHdRRR / CODESTATION_CODEMODELEPOM_xxxH.csv

:linkclass:`RRR` : Temps de recalage

:linkclass:`HHH` : Horizon de prévision

Calculer le RMSE par échéance (option *-c*)
++++++++++++++++++++++++++++++++++++++++++++

Le fichier produit contenant le RMSE pour chaque série de prévision et pour chaque horizon de prévision est placé dans:

    DATASET_NAME / scores / DATASET_NAME.txt

Impression graphique du RMSE par échéance (option *-p*)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++

Les fichiers *image* sont placés dans:

    DATASET_NAME / scores / DATASET_NAME_all.png
    DATASET_NAME / scores / DATASET_NAME.png

Le premier fichier contient toutes les séries de prévision. Dans le second fichier, la série *persistance* est ignorée afin de faciliter la lecture des autres courbes.

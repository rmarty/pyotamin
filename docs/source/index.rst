Documentation du projet pyOTAMIN
================================

Introduction
------------

Ce projet rassemble les scripts dédiés au projet **Prévision 2015** du Schapi. 

.. note::
   Toutes les fonctionnalités sont disponibles en python 3

   - `GRP2prv_py3.py <GRP2prv_py3.html>`_
   - `grp4otamin.py <grp4otamin.html>`_
   - `mohys4otamin.py <mohys4otamin.html>`_
   - `mohys4otamin_finalfiles.py <mohys4otamin_finalfiles.html>`_
   - `mohys4otamin_run.py <mohys4otamin_run.html>`_
   - `PLA2prv_py3.py <PLA2prv_py3.html>`_

.. warning::
   Seulement certaines fonctionnalités sont disponibles en python 2

   - `GRP2prv.py <GRP2prv.html>`_
   - `PLA2prv.py <PLA2prv.html>`_

:Version: |version|
:Date: |today|

Licence
-------
Le module *pyOTAMIN* est placé sous la licence GPL (GNU Public License). Vous trouverez la description de cette licence dans le fichier `COPYING.txt <_static/COPYING.txt>`_. L'interface graphique intègre des icônes dont les licences sont décrites dans `COPYING_icons.txt <_static/COPYING_icons.txt>`_. 

Contacts
--------

.. index:: Contacts
.. index:: Dépôt Bitbucket

`Renaud Marty <renaud.marty@developpement-durable.gouv.fr>
<mailto:renaud.marty@developpement-durable.gouv.fr>`_

|bitbucket| |version_b| |python2| |python3| |issues| |readthedocs| |codefactor| |License| 


.. |bitbucket| image:: https://img.shields.io/badge/source-pyotamin-blueviolet
    :target: https://bitbucket.org/rmarty/pyotamin
.. |version_b| image:: https://img.shields.io/badge/version-2022.05.18-blue
   :target: https://bitbucket.org/rmarty/pyotamin/get/2022.05.18.zip
.. |python2| image:: https://img.shields.io/badge/python-2.7-red
   :target: https://www.python.org/
.. |python3| image:: https://img.shields.io/badge/python-3.7-blue
   :target: https://www.python.org/
.. |issues| image:: https://img.shields.io/bitbucket/issues/rmarty/pyotamin
   :target: https://bitbucket.org/rmarty/pyotamin/issues?status=new&status=open
.. |readthedocs| image:: https://readthedocs.org/projects/pyotamin/badge/?version=latest
   :target: https://pyotamin.readthedocs.io/fr/latest
.. |codefactor| image:: https://www.codefactor.io/repository/bitbucket/rmarty/pyotamin/badge
   :target: https://www.codefactor.io/repository/bitbucket/rmarty/pyotamin
.. |License| image:: https://img.shields.io/badge/license-GPLv3-blue
   :target: https://www.gnu.org/licenses/quick-guide-gplv3.fr.html


Sommaire
--------

.. toctree::
   :maxdepth: 2
   :caption: Table des matières:

   GRP2prv
   GRP2prv_py3
   grp4otamin
   mohys4otamin
   mohys4otamin_finalfiles
   mohys4otamin_run
   PLA2prv
   PLA2prv_py3


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

grp4otamin
==========

.. automodule:: grp4otamin
   
   .. rubric:: Functions

   .. autosummary::
   
      check_compatibility
      check_leadtimes
      check_version_grp
      check_version_otamin
      read_grp
      run_grp4otamin
      set_GrpRun
      split_colname
      split_grpbasename
      str2td_grp
      write_otamin


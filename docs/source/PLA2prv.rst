
:orphan:


PLA2prv.py
==========


.. role:: linkmeth

.. role:: linkclass

.. index:: PLA2prv.py


Synopsis
--------

``PLA2prv.py``

Description
-----------

Conversion des sorties de PLATHYNES (mode temps-réel ou mode différé) au format PRV Scores, compatible avec EAO/EXPRESSO et OTAMIN

.. warning::
   Ce script ne fonctionne que dans un environnement Python 2, car il est fourni avec EAO/EXPRESSO dans le cadre du projet Previ2015

Notes de version
----------------

Version : 2019.10.28

- Ajout option -f pour export dans un **répertoire autre qu'OTAMIN**
- Utilisé pour exporter des valeurs avant *DtDerObs*

Version : 2019.10.14

- Ajout option -g pour recalage avec décroissance linéaire

Version : 2019.10.02

- Première version du script

Options
-------

``-i`` FOLDER, ``--in-folder`` FOLDER
   Dossier des prévisions PLATHYNES

``-m``  {TR, TD}, ``--mode``  {TR, TD}
   Mode de fonctionnement,  temps réel (TR)/temps differe (TD)

``-p`` MODELE, ``--modele-pom`` MODELE
   Code du modele au sens de la POM

``-o`` FOLDER, ``--otamin-folder`` FOLDER
   Dossier de sortie des fichiers PRV pour **OTAMIN**

``-f`` FOLDER, ``--out-folder`` FOLDER
   Dossier d'export dans un **répertoire autre qu'OTAMIN**. 
   
.. note::
   Si non défini, ce répertoire est celui défini par `-o`. 

``-s`` SCEN_1,SCEN_2,..., ``--scenarios`` SCEN_1,SCEN_2,...
   [optionnel] Liste des codes scenarios (separateur: ','). 
   
.. note::
   Si cette option n'est pas renseignée, les codes des scénarios seront déterminés selon le nom des fichiers PLATHYNES lus.

``-0`` SCEN_0, ``--scen-P0`` SCEN_0
   Code du scenario de pluie nulle

``-x`` {num, mod}, ``--suffix`` {num, mod}
   Suffixe des fichiers prv, distinguant les fichiers issus du même instant de production (défaut: 'num'). Configurations possibles:

    - **num** : le suffixe est défini par incrémentation de 1 à ..., afin d'obtenir un nom de fichier qui n'existe pas encore ;
    - **mod** : le suffixe est défini par le code modèle (argument `-p`).

``-d``, ``--hmet-data``
   [optionnel] Exporter les donnees associees RR et TA ? (*Faux*, par défaut)

``-u``, ``--unique-forecast``
   [optionnel] Exporter des previsions uniques ? (*Faux*, par défaut).
   
.. warning::
   La valeur par défaut de cette option intégrée dans la version 2017.08.29 implique un traitement inverse de celui de la version 2016.10.03

``-g`` N POM [dScen], ``--post-processing`` N POM [dScen]
   [optionnel] Post-traitement des séries: recalage à t0 avec décroissance linéaire de l'erreur sur **N** pas de temps. Le code modèle de la série brute est remplacé par **POM** et, si besoin, le code scénario modifié en incrémentant par **dScen**. Cet argument peut être défini plusieurs fois.

    - Exemple 1: -g 999 45sPLAdARR 
    - Exemple 2: -g 999 45sPLAdARR 100 
    - Exemple 3: -g 999 45sPLAdARR 100 -g 30 45sPLAdPRO 200
   
.. seealso::
   Il est conseillé de définir l'option `-f` pour exporter les simulations avant *DtDerObs*.

``-v``, ``--verbose``
   [optionnel] Mode verbeux (*Faux*, par défaut)

``-h``, ``--help``
   show this help message and exit

Exemples d'utilisation
----------------------

:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de PLATHYNES (code modèle POM: *45sPLAd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *007*. L'option *-x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      PLA2prv.py -i input -o output -m TR -0 007 -p 45sPLAd000 -v

:linkmeth:`Convertir les sorties Temps-Différé (*TD*) de PLATHYNES (code modèle POM: *45sPLAd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *007*. L'option *-x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      PLA2prv.py -i input -o output -m TD -0 007 -p 45sPLAd000 -v

:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de PLATHYNES (code modèle POM: *45sPLAd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *007*. Seuls les scénarios *001*, *002*, *003*, *004*, *005* et *007* seront pris en compte, même s'il en existe d'autres. L'option *-x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      PLA2prv.py -i input -o output -m TR -s 001,002,003,004,005 -0 007 -p 45sPLAd000 -v

:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de PLATHYNES (code modèle POM: *45sPLAd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *007*. L'option *x* étant définie par *mod*, le suffixe des noms de fichiers sera le code modèle *45sPLAd000*`

.. container:: cmdimg

   .. container:: cmdline

      PLA2prv.py -i input -o output -m TR -0 007 -p 45sPLAd000 -x mod -v


:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de PLATHYNES (code modèle POM: *45sPLAd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *007*. L'option *-x* étant définie par *num*, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      PLA2prv.py -i input -o output -m TR -0 007 -p 45sPLAd000 -x num -v

:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de PLATHYNES (code modèle POM: *45sPLAd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *007*. L'option *-x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement. Ici, les données *RR* associées aux prévisions de débit sont exportées (*-d*).`

.. container:: cmdimg

   .. container:: cmdline

      PLA2prv.py -i input -o output -m TR -0 007 -p 45sPLAd000 -d -v

:linkmeth:`Convertir les sorties Temps-Différé (*TD*) de PLATHYNES (code modèle POM: *45sPLAd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement. Le script n'exporte que les scénarios uniques (i.e. distincts des scénarios déjà lus), par l'application de l'option (*-u*).`

.. container:: cmdimg

   .. container:: cmdline

      PLA2prv.py -i input -o output -m TD -0 007 -p 45sPLAd000 -u -v

:linkmeth:`Convertir les sorties Temps-Différé (*TD*) de PLATHYNES (code modèle POM: *45sPLAdBRU*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement. Le script n'exporte que les scénarios uniques (i.e. distincts des scénarios déjà lus), par l'application de l'option (*-u*). Ici, les données *RR* associées aux prévisions de débit sont exportées (*-d*). L'application détermine 2 séries recalées par série brute *unique*: (i) recalage sur une durée de *999* pas de temps, sous le code modèle *45sPLAdARR* et avec une incrémentation de 100 sur le code scénario, (ii) recalage sur une durée de *30* pas de temps, sous le code modèle *45sPLAdPRO* et avec une incrémentation de 0 (*valeur par défaut*) sur le code scénario`

.. container:: cmdimg

   .. container:: cmdline

      PLA2prv.py -i input -o output -m TD -0 007 -p 45sPLAdBRU -u -d -v -g 999 45sPLAdARR 100 -g 30 45sPLAdPRO


:linkmeth:`Convertir les sorties Temps-Différé (*TD*) de PLATHYNES (code modèle POM: *45sPLAd000*) situées dans le dossier *input* au format PRV. L'export pour OTAMIN, i.e. censuré par *DtDerObs*, est placé dans *output* et l'export sans censure dans *output2*. Le scénario de pluie nulle est identifié par le code *007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      PLA2prv.py -i input -o output -m TD -0 007 -p 45sPLAd000 -f output2 -v


:orphan:


grp4otamin.py
=============


.. role:: linkmeth

.. role:: linkclass

.. index:: grp4otamin.py.py


Synopsis
--------

``grp4otamin.py.py``


Description
-----------

Script de préparation *en masse* des données produites par GRP Calage pour OTAMIN, outil d'estimation de l'incertitude prédictive "modèle"

Notes de version
----------------

Version 2022.02.09
    - Compatibiltié avec GRPv2020

Version 2021.03.30
    - Compatibiltié avec GRPv2016 et GRPv2018
    - Compatibiltié avec OTAMINv2016 et OTAMINv2018
    - Ajout des paramètres INPUT_VERSION, OUTPUT_VERSION et POM_MODEL
    - Introduction de tests unitaires

Version 2019.04.09
    - adaptation à OTAMIN-2018 : nommage des horizons horaires 024 -> 024H

Paramètres
----------

:linkclass:`INPUT_DIRNAME` : Chemin du dossier contenant les données GRP Calage. Cette variable est imposée par la configuration de GRP.

:linkclass:`INPUT_SUFFIX` : Suffixe des noms de fichier à traiter. Cette variable est imposée par la convention suivie par GRP pour nommer les fichiers produits lors du **calage sur l'ensemble des données**, avant création de la base *Temps Réel*

.. note::
   INPUT_SUFFIX = '_PP_P0P0.TXT'

:linkclass:`INPUT_VERSION` : Version de GRP

.. note::
   INPUT_VERSION in ['GRPv2016', 'GRPv2018']

:linkclass:`LEADTIMES` : Horizons de prévision pour lesquels GRP stocke les prévisions. Cette variable est imposée par la convention suivie par GRP pour nommer les fichiers produits lors du **calage sur l'ensemble des données**, avant création de la base *Temps Réel*

.. note::
   LEADTIMES = [1, 3, 6, 9, 12, 18, 24, 30, 36, 42, 48, 54, 60, 66, 72]

:linkclass:`MODEL_PREFIX` : Code modèle au sens de la POM: 2 caractères pour le département (ex: **45**) du SPC suivi de **gGRPd** pour le déclarer dans la famille des modèles globaux (**g**) GRP en tant que modèle **d**\éterministe. Voir la nomenclature de codification de modèles *POM*.

.. note::
   POM_MODEL = '45gGRPd000'. Ce paramètre a changé de nom dans la version 2021.03.30

:linkclass:`OUTPUT_DIRNAME` : Chemin du dossier contenant les fichiers d'entrée d'OTAMIN. Cette variable est imposée par l'installation d'OTAMIN. Par exemple, si vous avez installé OTAMIN directement sous C:\

.. note::
   OUTPUT_DIRNAME = 'C:\OTAMIN\_01_ANA\_in'

:linkclass:`OUTPUT_VERSION` : Version d'OTAMIN

.. note::
   OUTPUT_VERSION in ['OTAMINv2016', 'OTAMINv2018']

Exécution
---------

.. container:: cmdimg

   .. container:: cmdline

      python grp4otamin.py


API Reference
-------------

.. autosummary::
    :toctree: api/
   
    grp4otamin


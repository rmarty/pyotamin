
:orphan:


mohys4otamin_finalfiles.py
==========================


.. role:: linkmeth

.. role:: linkclass

.. index:: mohys4otamin_finalfiles.py


Synopsis
--------

``mohys4otamin_finalfiles.py``


Description
-----------

Script créant les fichiers pour OTAMIN à partir des séries produites par `mohys4otamin.py <mohys4otamin.html>`_

.. warning::
   Ce script ne fonctionne que dans un environnement Python 3

Notes de version
----------------

Version 2019.04.09

Paramètres
----------

:linkclass:`INVENTORY_FILENAME` : Fichier csv de l'inventaire des temps de recalage

:linkclass:`RESULTS_DIRNAME` : Dossier parent des fichiers traités par `mohys4otamin.py <mohys4otamin.html>`_
   
:linkclass:`OTAMIN_DIRNAME` : Dossier *_in* d'OTAMIN *Calage*

:linkclass:`MODEL_POM` : Préfixe du code modèle POM

:linkclass:`TABLES` : Dictionnaire des noms des codes modèles POM des séries sans recalage (0) et avec un recalage global (999). Le code modèle de la série recalée sera **MODELEPOM**\RRR

.. note::
   
    **RRR** : Temps de recalage sur 3 caractères

Exécution
---------

.. container:: cmdimg

   .. container:: cmdline

      python mohys4otamin_finalfiles.py


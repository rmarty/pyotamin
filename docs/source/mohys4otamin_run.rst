
:orphan:


mohys4otamin_run.py
===================


.. role:: linkmeth

.. role:: linkclass

.. index:: mohys4otamin_run.py


Synopsis
--------

``mohys4otamin_run.py``


Description
-----------

Script de lancement de `mohys4otamin.py <mohys4otamin.html>`_

.. warning::
   Ce script ne fonctionne que dans un environnement Python 3

Notes de version
----------------

Version 2019.04.09

Paramètres
----------

:linkclass:`HOME` : Chemin de la racine contenant les fichiers pour OTAMIN

Exécution
---------

.. container:: cmdimg

   .. container:: cmdline

      python mohys4otamin_run.py


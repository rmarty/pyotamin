# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

import os
import sys

# -- LOCAL or ON READTHEDOCS setup -------------------------------------------
ON_RTD = os.environ.get('READTHEDOCS', None) == 'True'

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
if ON_RTD:
    sys.path.insert(
        0,
        os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..'))
    )
else:
    for p in [r'D:\Utilisateurs\renaud.marty\Documents\3-bitbucket\pyOTAMIN']:
        if os.path.exists(p):
            sys.path.insert(0, os.path.abspath(p))

# -- Project information -----------------------------------------------------
project = 'pyOTAMIN'
author = 'Renaud Marty'
date = '2022-05-18'
copyright = '{}, {}'.format(date.split('-', maxsplit=1)[0], author)
# The short X.Y version
version = date
# The full version, including alpha/beta/rc tags
release = version

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.imgmath',
    'sphinx.ext.viewcode',
    'sphinx.ext.napoleon',
    'sphinx_rtd_theme'
]
autosummary_generate = True
autodoc_default_options = {
    'members': None,  # pour forcer 'autodoc/autosummary' sur tous les membres
    'show-inheritance': True,  # pour afficher l'héritage
    # 'inherited-members': True,  # pour forcer 'autodoc/autosummary' sur les membres hérités,
    # 'undoc-members': True
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = 'fr'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'friendly' # None


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'  # 'nature'

def setup(app):
    app.add_stylesheet('pyotamin.css')  # may also be an URL

html_css_files = ['pyotamin.css']
html_title = 'Documentation pyotamin - ' + release
html_short_title = 'Documentation pyOTAMIN'
html_logo = '_static/pyotamin.png'
html_favicon = '_static/pyotamin.ico'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
html_theme_options = {
    'style_nav_header_background': '#004384',
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

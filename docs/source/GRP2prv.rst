
:orphan:


GRP2prv.py
==========


.. role:: linkmeth

.. role:: linkclass

.. index:: GRP2prv.py


Synopsis
--------

``GRP2prv.py``

Description
-----------

Conversion des sorties de GRP (mode temps-réel ou mode différé) au format PRV Scores, compatible avec EAO/EXPRESSO et OTAMIN

.. warning::
   Ce script ne fonctionne que dans un environnement Python 2, car il est fourni avec EAO/EXPRESSO dans le cadre du projet Previ2015

Notes de version
----------------

Version : 2022.05.18
    - Correction liée à la balise EXTRA de GRP v2020

Version 2022.02.09
    - Compatibiltié avec GRPv2020

Version 2021.08.30
    - Compatibiltié avec GRPv2016 et GRPv2018
    - Compatibiltié avec OTAMINv2016 et OTAMINv2018

Version : 2019.10.28
    - Ajout option -f pour export dans un **répertoire autre qu'OTAMIN**
    - Utilisé pour exporter des valeurs avant *DtDerObs*

Version : 2019.04.09
    - ajout arg -1 pour exporter 1 fichier par lieu
    - ajout arg -g pour exporter les simulations
        - sim interne avant DtDerObs
        - prévision sans assimilation après
        - en spécifiant le code modèle de la version sans assimilation de données
        
Version : 2017.08.29
    - ajout arg -u pour exporter toutes series, mm identiques

.. warning::
   La valeur par défaut de l'option (-u) intégrée depuis la version 2017.08.29 implique un traitement inverse de celui de la version 2016.10.03
   
Version : 2016.10.03
    - ajout arg -d pour exporter les pluies (RR), températures (TA) obs et prv

Options
-------

``-i`` FOLDER, ``--in-folder`` FOLDER
   Dossier des prévisions GRP

``-m``  {TR, TD}, ``--mode``  {TR, TD}
   Mode de fonctionnement,  temps réel (TR)/temps differe (TD)

``-p`` MODELE, ``--modele-pom`` MODELE
   Code du modele au sens de la POM

``-o`` FOLDER, ``--otamin-folder`` FOLDER
   Dossier de sortie des fichiers PRV pour **OTAMIN**

``-f`` FOLDER, ``--out-folder`` FOLDER
   Dossier d'export dans un **répertoire autre qu'OTAMIN**. 
   
.. note::
   Si non défini, ce répertoire est celui défini par `-o`. 
   
.. note::
   Cette option est inutile si l'option `-g` n'est pas appliquée.

``-G`` {GRPv2016,GRPv2018,GRPv2020}, ``--GRP-version`` {GRPv2016,GRPv2018,GRPv2020}
   Version de **GRP** spécifique à la configuration du SPC. Si non défini: *GRPv2016*, pour des raisons de rétro-compatibilité.
   
.. warning::
   Option ajoutée dans la version **2021.08.30**

``-O`` {OTAMINv2016,OTAMINv2018}, ``--OTAMIN-version`` {OTAMINv2016,OTAMINv2018}
   Version de **OTAMIN** spécifique à la configuration du SPC. Si non défini: *OTAMINv2016*, pour des raisons de rétro-compatibilité.
   
.. warning::
   Option ajoutée dans la version **2021.08.30**

``-s`` SCEN_1,SCEN_2,..., ``--scenarios`` SCEN_1,SCEN_2,...
   [optionnel] Liste des codes scenarios (separateur: ','). 
   
.. note::
   Si cette option n'est pas renseignée, les codes des scénarios seront déterminés selon le nom des fichiers GRP lus.

``-0`` SCEN_0, ``--scen-P0`` SCEN_0
   Code du scenario de pluie nulle

``-x`` {num, mod}, ``--suffix`` {num, mod}
   Suffixe des fichiers prv, distinguant les fichiers issus du même instant de production (défaut: 'num'). Configurations possibles:

    - **num** : le suffixe est défini par incrémentation de 1 à ..., afin d'obtenir un nom de fichier qui n'existe pas encore ;
    - **mod** : le suffixe est défini par le code modèle (argument `-p`).

``-d``, ``--hmet-data``
   [optionnel] Exporter les donnees associees RR et TA ? (*Faux*, par défaut)

``-u``, ``--unique-forecast``
   [optionnel] Exporter des previsions uniques ? (*Faux*, par défaut).
   
.. warning::
   La valeur par défaut de cette option intégrée dans la version 2017.08.29 implique un traitement inverse de celui de la version 2016.10.03

``-1``, ``--onefile-byloc``
   [optionnel] Exporter 1 fichier par lieu ? (*Faux*, par défaut)

``-g``, ``--grp-simu``
   [optionnel] Exporter les simulations en spécifiant le code modèle. 
   
.. seealso::
   Il est conseillé de définir l'option `-f` pour exporter les simulations avant *DtDerObs*.

``-v``, ``--verbose``
   [optionnel] Mode verbeux (*Faux*, par défaut)

``-h``, ``--help``
   show this help message and exit


Instant de départ de la prévision *DtDerObs*
--------------------------------------------

L'instant de départ de chaque série de prévision est déterminée automatiquement par ce script, selon les cas suivants :

- Si le fichier produit par GRP contenant les observations existe

    - Si ce fichier contient au moins une valeur de débit, distinct de la valeur de débit manquant (*-9.9999*), alors l'instant du débit le plus récent est considéré comme étant l'instant de départ de la prévision ;
    - Sinon l'instant de départ de la prévision est fixé en ôtant 1 heure au premier pas de temps de la série d'observation. Un message d'avertissement est signalé à l'utilisateur.

- Sinon, l'instant de départ de la prévision est fixé en ôtant 1 heure au premier pas de temps de la série de prévision. Un message d'avertissement est signalé à l'utilisateur.


Exemples d'utilisation
----------------------

:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TR -0 2007 -p 45gGRPd000 -v

:linkmeth:`Convertir les sorties Temps-Différé (*TD*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TD -0 2007 -p 45gGRPd000 -v

:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. Seuls les scénarios *2001*, *2002*, *2003*, *2004*, *2005* et *2007* seront pris en compte, même s'il en existe d'autres. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TR -s 2001,2002,2003,2004,2005 -0 2007 -p 45gGRPd000 -v

:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* étant définie par *mod*, le suffixe des noms de fichiers sera le code modèle *45gGRPd000*.`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TR -0 2007 -p 45gGRPd000 -x mod -v


:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* étant définie par *num*, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TR -0 2007 -p 45gGRPd000 -x num -v

:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* étant définie par *num*, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement. Ici, les données *RR* et *TA* associées aux prévisions de débit sont exportées (-d).`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TR -0 2007 -p 45gGRPd000 -x num -d -v

:linkmeth:`Convertir les sorties Temps-Différé (*TD*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement. Le script crée un fichier par lieu (*-1*).`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TD -0 2007 -p 45gGRPd000 -1 -v

:linkmeth:`Convertir les sorties Temps-Différé (*TD*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement. Le script exporte les simulations avant (**PQE_1A_D.DAT**) et après la date de dernière observation (**GRP_D_Simu_...**), par l'application de l'option (*-g*), en affectant le code modèle *45gGRPdBRU* à ces simulations.`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TD -0 2007 -p 45gGRPd000 -g 45gGRPdBRU -v


:linkmeth:`Convertir les sorties Temps-Différé (*TD*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV. L'export censuré par *DtDerObs* est placé dans le répertoire *output*. L'export non-censuré est placé dans *output2*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement. Le script exporte les simulations avant (**PQE_1A_D.DAT**) et après la date de dernière observation (**GRP_D_Simu_...**), par l'application de l'option (*-g*), en affectant le code modèle *45gGRPdBRU* à ces simulations.`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TD -0 2007 -p 45gGRPd000 -g 45gGRPdBRU -f output2 -v


:linkmeth:`Convertir les sorties Temps-Différé (*TD*) de GRP (code modèle POM: *45gGRPd000*) situées dans le dossier *input* au format PRV dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement. Le script n'exporte que les scénarios uniques (i.e. distincts des scénarios déjà lus), par l'application de l'option (*-u*).`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TD -0 2007 -p 45gGRPd000 -u -v



:linkmeth:`Convertir les sorties Temps-Réel (*TR*) de GRP (code modèle POM: *45gGRPd000*) dans sa version *GRPv2018* situées dans le dossier *input* au format PRV d'OTAMIN dans sa version *OTAMINv2018* dans le dossier *output*. Le scénario de pluie nulle est identifié par le code *2007*. L'option *x* n'étant pas définie, le suffixe des noms de fichiers créés par ce script sera incrémenté numériquement.`

.. container:: cmdimg

   .. container:: cmdline

      GRP2prv.py -i input -o output -m TR -0 2007 -p 45gGRPd000 -G GRPv2018 -O OTAMINv2018 -v



API Reference
-------------

.. autosummary::
    :toctree: api/
   
    GRP2prv

